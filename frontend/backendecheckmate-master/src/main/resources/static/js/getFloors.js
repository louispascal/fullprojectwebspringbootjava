if (document.getElementsByTagName("title")[0].innerHTML !== "Configuration du plan") {
    var button = document.getElementById("visualize");
    button.disabled = true;
}
let listBuilding = document.getElementById("buildings");
listBuilding.addEventListener('change', function () {
    let id_building = listBuilding.options[listBuilding.options.selectedIndex].value;
    console.log(id_building);
    document.getElementById("floors").innerHTML = "";
    $.ajax({
        url: 'http://172.31.249.232:8080/map/floor/' + id_building,
        type: 'GET',
        dataType: 'json',
        success: function (floors) {
            console.log(floors);
            for (let i = 0; i < floors.length; i++) {
                if (document.getElementsByTagName("title")[0].innerHTML == "Configuration du plan") {
                    document.getElementById("floors").innerHTML += '<option value="' + floors[i].id_floor + '">Etage ' + floors[i].floor_number + '</option>";'
                } else {
                    button.disabled = false;
                    document.getElementById("floors").innerHTML += '<option value="' + floors[i].id_floor + '">Etage ' + floors[i].floor_number + '</option>";'
                }
            }
        }
    });
});


