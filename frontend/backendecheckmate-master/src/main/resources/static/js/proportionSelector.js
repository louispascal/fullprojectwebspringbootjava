let proportion = document.getElementById("proportion");
let solarInput = document.getElementById("solarInput");
let hydraulicInput = document.getElementById("hydraulicInput");
let windInput = document.getElementById("windInput");
let submitProportion= document.getElementById("submitProportion");
let modalProportion = document.getElementById("modalProportion");
let closeProportion = document.getElementById("closeProportion");
let inputCompleted1 = false;
let inputCompleted2 = false;
let inputCompleted3 = false;
let jsonProportion = {
     "solarProportion":0,
     "hydraulicProportion":0,
     "windProportion":0
      };
let remainingValue1;
let remainingValue2;
let valueInput1;
solarInput.addEventListener('input', function () {
    console.log(solarInput.value);
    jsonProportion.solarProportion=solarInput.value;
        valueInput1 = solarInput.value;
        remainingValue1 = 100 - valueInput1;
        console.log(remainingValue1);
        inputCompleted1 = true;
        if(!inputCompleted2) {
        hydraulicInput.setAttribute("max", remainingValue1);
        }
        if(!inputCompleted3) {
        windInput.setAttribute("max", remainingValue1);
        }
    });
hydraulicInput.addEventListener('input', function () {
        console.log(hydraulicInput.value);
        inputCompleted2 = true;
        jsonProportion.hydraulicProportion=hydraulicInput.value;
        let valueInput2 = hydraulicInput.value;
                remainingValue2 = 100 - valueInput2 - valueInput1;
                console.log(remainingValue2);
                inputCompleted2 = true;
                if(!inputCompleted3) {
                windInput.setAttribute("max", remainingValue2);
                }
 });
windInput.addEventListener('input', function () {
        console.log(windInput.value);
        inputCompleted3 = true;
        jsonProportion.windProportion=windInput.value;
        console.log(jsonProportion);

});

proportion.addEventListener('change', function () {
   modalProportion.style.display = "block";
});

closeProportion.onclick = function () {
   modalProportion.style.display = "none";
}
submitProportion.addEventListener('click',function() {
    $.ajax({
      url: 'http://172.31.249.232:8080/power/proportion/'+jsonProportion.solarProportion+'/'+jsonProportion.hydraulicProportion+'/'+jsonProportion.windProportion,
      type: 'POST',
      async: true,
      data: JSON.stringify(jsonProportion),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (result) {
      console.log(result);
        }});
  });
function myFunction() {
    if(solarInput.value>100)
    {
    solarInput.value=solarInput.value.substring(0, solarInput.value.length-1);
        alert("la valeur dépasse 100%")
    }
}
function myFunction2() {
    if(hydraulicInput.value>remainingValue1)
    {
        alert("vous ne pouvez pas dépasser cette valeur : "+ remainingValue1)
        if(remainingValue1>0){
        hydraulicInput.value=remainingValue1;

        }else { hydraulicInput.value=0;}
    }
}
function myFunction3() {
    if(windInput.value > remainingValue2)
    {
        alert("vous ne pouvez pas dépasser cette valeur : "+ remainingValue2)
        if(remainingValue2>0){
        windInput.value=remainingValue2;
        }else { windInput.value=0;}
    }
}


