

let economic =document.getElementById("economic");
let environmental=document.getElementById("environmental");
let simulationInput =document.getElementById("simulationInput");
let submitButton= document.getElementById("submit");
let json = {
"logic":"null",
"value":0,
};
economic.addEventListener('input', function () {
    console.log(economic.value);
    json.logic=economic.value;
});
environmental.addEventListener('input', function () {
        console.log(environmental.value);
         json.logic=environmental.value;

});
simulationInput.addEventListener('input', function () {
        console.log(simulationInput.value);
         json.value=simulationInput.value;
        console.log(json);
});
function drawMultipleLineChart(formatteddata, title, xtext, ytext){
	Highcharts.chart('container', {

        title: {
            text: title
        },

        subtitle: {
            text: 'Source: pds.checkmate.com'
        },

        yAxis: {
            title: {
                text: ytext
            }
        },

        xAxis: {
            title: {
                 text: xtext
                        },
            accessibility: {
                rangeDescription: 'Range: 0 to 10000'
            }
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 0
            }
        },

        series: formatteddata,

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
}

function displayEconomicRecommendations(formatteddata){
     var divRecommendations = document.getElementById('recommendations');
     var content = " <h4>La recommendation de répartition de la production par source :</h4> <ol>   <li> la source "+ formatteddata.recommendation[0] +" :"+ formatteddata.recommendation[1]+" Kw </li> <br> <li>la source "+ formatteddata.recommendation[2] +" :"+ formatteddata.recommendation[3]+" Kw </li>  <br> <li>la source "+ formatteddata.recommendation[4] +" :"+ formatteddata.recommendation[5]+" Kw </li>  <br> <li> autres sources  :"+ formatteddata.recommendation[7]+" Kw </li> </ol> <h6>le coût total de la production des sources renouvelables est "+formatteddata.totalCost[0] +" €</h6> <h6> la capacité de production supplémentaire de chaque source : </h6> <ol>   <li> la source "+ formatteddata.currentCapacity[0] +" :"+ formatteddata.currentCapacity[1]+" Kw </li> <br> <li>la source "+ formatteddata.currentCapacity[2] +" :"+ formatteddata.currentCapacity[3]+" Kw </li>  <br> <li>la source "+ formatteddata.currentCapacity[4] +" :"+ formatteddata.currentCapacity[5]+" Kw </li>  </ol>"
     divRecommendations.innerHTML = content;
}

function displayRecommendations(formatteddata){
     var divRecommendations = document.getElementById('recommendations');
     var content = " <h4>Les recommendations des sources :</h4> <ol>   <li>" + formatteddata.recommendation[0] +" </li> <br> <li>"+ formatteddata.recommendation[1]+"</li>  <br> <li>"+formatteddata.recommendation[2]+"</li> </ol>  "
     divRecommendations.innerHTML = content;
}

function ajaxGraph(title, xtext, ytext){
 $.ajax({
               url: 'http://172.31.249.232:8080/power/multipleLinechart/'+json.logic,
               type: 'POST',
               data: JSON.stringify(json),
               async: true,
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: function (result) {
                   console.log(result);
                   var formatteddata = [];
                   for(var key in result){
                        var singleObject = {
                            name: '',
                            data: []
                        }

                   	singleObject.name = key.toUpperCase();
                   	for(var i = 0; i < result[key].length; i++){
                   		singleObject.data.push(result[key][i]);
                   	}
                   	formatteddata.push(singleObject);
                   	}
                   	console.log(formatteddata);
                   	drawMultipleLineChart(formatteddata, title, xtext, ytext);
               }});
}
function ajaxEconomicRecommendations(){
$.ajax({
                        url: 'http://172.31.249.232:8080/power/economicRecommendations/'+json.value,
                        type: 'POST',
                        async: true,
                        data: JSON.stringify(json),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            displayEconomicRecommendations(result);
                        }});
}
function ajaxEnvironmentalRecommendations(){
$.ajax({
                        url: 'http://172.31.249.232:8080/power/environmentalRecommendations/'+json.value,
                        type: 'POST',
                        async: true,
                        data: JSON.stringify(json),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (result) {
                            console.log(result);
                            displayRecommendations(result);
                        }});
}

submitButton.addEventListener('click', function () {
         if(json.logic!=null && json.value!=0){
             if(json.logic== "economic") {
               ajaxGraph("Prix de l énergie","Quantité en 1000 KW", "Prix d un KW en €" );
               ajaxEconomicRecommendations();
             }
            if(json.logic == "environmental" ){
              ajaxGraph("Empreinte carbone","Quantité en 1000 KW", "Empreinte carbone en gCO2" );
              ajaxEnvironmentalRecommendations();
            }
        }
});







