let id_buildingValue;
let corridorValue;
let id_floorValue;
let nb_open_spaceValue;
let nb_kitchenValue;
let nb_meeting_roomValue;
let nb_office_doubleValue;
let nb_officeValue;
let fillValue;
let json = {
    "id_building": 0,
    "corridor": "",
    "id_floor": 0,
    "nb_open_space": 0,
    "nb_kitchen": 0,
    "nb_meeting_room": 0,
    "nb_office_double": 0,
    "nb_office": 0,
    "fill": false
}
checkboxChange();
simpleOfficeChange();
onClickButton(json);
getValues();
listening(id_buildingValue);
listening(corridorValue);
listening(id_floorValue);
listening(nb_open_spaceValue);
listening(nb_kitchenValue);
listening(nb_meeting_roomValue);
listening(nb_office_doubleValue);
listening(nb_officeValue);
listening(fillValue);

function update(jsonToUpdate) {
    jsonToUpdate.id_building = id_buildingValue.value;
    jsonToUpdate.corridor = corridorValue.value;
    jsonToUpdate.id_floor = id_floorValue.value;
    jsonToUpdate.nb_open_space = nb_open_spaceValue.value;
    jsonToUpdate.nb_kitchen = nb_kitchenValue.value;
    jsonToUpdate.nb_meeting_room = nb_meeting_roomValue.value;
    jsonToUpdate.nb_office_double = nb_office_doubleValue.value;
    jsonToUpdate.nb_office = nb_officeValue.value;
    jsonToUpdate.fill = fillValue.value;
}

function getValues() {
    id_buildingValue = document.getElementById("buildings");
    corridorValue = document.getElementById("corridor");
    id_floorValue = document.getElementById("floors")
    nb_open_spaceValue = document.getElementById("op");
    nb_kitchenValue = document.getElementById("kitchen");
    nb_meeting_roomValue = document.getElementById("meeting");
    nb_office_doubleValue = document.getElementById("double");
    nb_officeValue = document.getElementById("simple office");
    fillValue = document.getElementById("fill");
}

function listening(listened) {
    listened.addEventListener('input', function () {
        getValues();
        console.log("New value : " + listened.value);
        update(json);
    });
}

function checkboxChange() {
    let checkbox = document.querySelector("input[name=checkbox]");
    checkbox.addEventListener('change', function () {
        if (this.checked) {
            console.log("Checkbox is checked..");
            document.getElementById("fill").setAttribute("value", "on");
            json.fill = true;
        } else {
            console.log("Checkbox is not checked..");
            document.getElementById("fill").setAttribute("value", "off");
            json.fill = false;
        }
    });
}


function simpleOfficeChange() {
    document.getElementById("simple").style.display = "none";
    let corridor = document.getElementById("corridor");
    corridor.addEventListener('change', function () {
        let value = corridor.value;
        console.log(value);
        if (value === "2 couloirs" || value == "2") {
            document.getElementById("simple").style.display = "block";
        } else {
            document.getElementById("simple").style.display = "none";
        }
    });
}


function onClickButton(json) {
    let button = document.getElementById("button");
    button.addEventListener('click', function () {
        sendForm(json);
    });
}

function sendForm(json) {
    let alertMessage;
    let icon;
    if (json.fill === "on" || json.fill === "off") {
        json.fill = false;
    }
    let url = "/showPreview/" + json.id_building + "/" + json.id_floor;
    $.ajax({
        url: 'http://172.31.249.232:8080/map/config/submit',
        type: 'POST',
        data: JSON.stringify(json),
        contentType: "application/json; charset=utf-8",
        dataType: "text",
        success: function (answer) {
            console.log(answer);
            alertMessage = 'La configuration est fonctionnelle.';
            icon = 'success';
            console.log(alertMessage);
            console.log(icon);
            Swal.fire({
                position: 'center',
                icon: icon,
                title: alertMessage,
                showConfirmButton: true,
                confirmButtonText:
                    '<a id="buttonOK" style="text-decoration:none; color: white"> Visualiser la configuration </a>'
            })
            document.getElementById("buttonOK").setAttribute("href", url);
        },
        error: function (answer) {
            console.log(answer.responseText);
            switch (answer.responseText) {
                case 'Need more' :
                    alertMessage = 'La configuration ne prend pas assez de place.';
                    icon = 'error';
                    break;
                case 'Too many requested' :
                    alertMessage = 'La configuration prend trop de place.';
                    icon = 'error';
                    break;
                default:
                    alertMessage = 'None';
                    icon = 'error';
                    break;
            }
            Swal.fire({
                position: 'center',
                icon: icon,
                title: alertMessage,
                showConfirmButton: true
            })
        }
    });
}



