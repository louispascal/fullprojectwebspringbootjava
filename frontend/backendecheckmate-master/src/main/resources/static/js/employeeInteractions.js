let interval = setInterval('', 99999999);
let currSender;
let currReceiver;
let messageContainer = document.getElementById("messageContainer");
let textInput = document.getElementById("textInput");
textInput.addEventListener("keyup", function (event) {
    if (event.key === 'Enter') {
        sendMessage(this.value, currSender, currReceiver);
        this.value = "";
        this.select();
    }
});

function displayMessage(sender, receiver) {
    currSender = sender;
    currReceiver = receiver;
    clearInterval(interval);
    $.ajax({
        type: "GET",
        url: "http://172.31.249.232:8080/user/getMessages/" + sender + "," + receiver,
        success: function (messages) {
            messageWindow(messages, sender, receiver);
        }
    });
    interval = setInterval(function () {
        $.ajax({
            type: "GET",
            url: "http://172.31.249.232:8080/user/getMessages/" + sender + "," + receiver,
            success: function (messages) {
                messageWindow(messages, sender, receiver);
            }
        });
    }, 5000);
}

function messageWindow(messages, sender, receiver) {
    textInput.style.visibility = "visible";
    messageContainer.style.visibility = "visible";
    messageContainer.innerHTML = "";
    for (i = 0; i < messages.length; i++) {
        mContainer = document.createElement("div");
        contentText = document.createElement("div");
        contentText.innerHTML = messages[i].content;
        contentText.setAttribute("style","width: 300px;");
        mTime = document.createElement("span");
        mTime.innerHTML = messages[i].time;
        mContainer.appendChild(contentText);
        mContainer.appendChild(mTime);
        if (messages[i].sender == sender) {
            mContainer.setAttribute("class", "containerMessage darker");
            mTime.setAttribute("class", "time-left");
        } else {
            mContainer.setAttribute("class", "containerMessage");
            mTime.setAttribute("class", "time-right");
        }
        messageContainer.appendChild(mContainer);
    }
    textInput.removeEventListener("keyup");
    textInput.addEventListener("keyup", function (event) {
        if (event.key === 'Enter') {
            sendMessage(this.value, sender, receiver);
            this.value = "";
            this.select();
        }
    });
    messageContainer.scrollTop = messageContainer.scrollHeight;
}

function sendMessage(t, s, r) {
    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date + ' ' + time;
    var info = {
        sender: s,
        receiver: r,
        content: t,
        time: dateTime,
    };
    $.ajax({
        type: "POST",
        url: "http://172.31.249.232:8080/user/sendMessage",
        dataType: 'json',
        data: JSON.stringify(info),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            console.log("SUCCESS: ", data);
        }
    });
    mContainer = document.createElement("div");
    contentText = document.createElement("p");
    contentText.innerHTML = info.content;
    mTime = document.createElement("span");
    mTime.innerHTML = info.time;
    mContainer.appendChild(contentText);
    mContainer.appendChild(mTime);
    mContainer.setAttribute("class", "containerMessage darker");
    mTime.setAttribute("class", "time-left");
    messageContainer.appendChild(mContainer);
    messageContainer.scrollTop = messageContainer.scrollHeight;
}


function filter() {
    var input, filter, employees, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    employees = document.getElementsByClassName("employee");
    for (i = 0; i < employees.length; i++) {
        name = employees[i].getElementsByClassName("name")[0].innerHTML;
        firstName = employees[i].getElementsByClassName("firstName")[0].innerHTML;
        if (name.toUpperCase().indexOf(filter) > -1 || firstName.toUpperCase().indexOf(filter) > -1) {
            employees[i].style.display = "";
        } else {
            employees[i].style.display = "none";
        }
    }
}

function show(id,name,firstname){
    document.getElementById("invitedName").innerHTML="Saisissez la réunion dans laquelle vous souhaitez inviter "+name+" "+firstname+":";
    document.getElementById("popup").style.display = "block";
    meetings = new Array();
    $.ajax({
        type: "GET",
        url: "http://172.31.249.232:8080/reservation/ownMeetings/"+document.getElementById("ownId").innerHTML,
        success: function (r) {
            meetings = r;
            ml = document.getElementById("meetingList");
            ml.innerHTML="";
            for (i = 0; i < meetings.length; i++) {
                if(new Date() < new Date(meetings[i].end_date.replace(/-/g, "/"))) {
                    li = document.createElement("li");
                    li.innerHTML = "Salle " + meetings[i].id_spacepk + " le " + meetings[i].date;
                    console.log(li.innerHTML);
                    li.setAttribute("onclick", "invite(" + meetings[i].id_reservation + "," + id + ")");
                    ml.appendChild(li);
                }
            }
        }
    });
}
function hide() {
    document.getElementById("popup").style.display = "none";

}

function invite(id_reservation,id){
    $.ajax({
        type: "POST",
        url: "http://172.31.249.232:8080/reservation/invite/"+id_reservation+"/"+id,
        success: function (data) {
            console.log("SUCCESS: ", data);
        }
    });
    hide();
}