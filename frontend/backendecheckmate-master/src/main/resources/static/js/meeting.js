var currentDateTime = new Date();
var year = currentDateTime.getFullYear();
var month = (currentDateTime.getMonth() + 1);
var date = (currentDateTime.getDate() + 1);

if(date < 10) {
    date = '0' + date;
}
if(month < 10) {
    month = '0' + month;
}

var dateTomorrow = year + "-" + month + "-" + date;
var checkinElem = document.querySelector("#checkin-date");
var checkoutElem = document.querySelector("#checkout-date");

checkinElem.setAttribute("min", dateTomorrow);
checkoutElem.setAttribute("min", dateTomorrow);

checkinElem.onchange = function () {
    checkoutElem.setAttribute("min", this.value);
}

checkoutElem.onchange = function () {
    checkinElem.setAttribute("max", this.value);
}

buildingSelect = document.getElementById("building-selection");
company = document.getElementById("company").innerText;
$.ajax({
    type: "GET",
    url: "http://172.31.249.232:8080/map/buildings/" + company,
    success: function (buildings) {
        fillOptionsBuilding(buildings,buildingSelect);
    }
});


roomSelect = document.getElementById("room-selection");
$.ajax({
    type: "GET",
    url: "http://172.31.249.232:8080/map/availableRooms/" + company,
    success: function (rooms) {
        fillOptionsRooms(rooms,roomSelect);
    }
});

function fillOptionsBuilding(buildings,select){
    for(i=0;i<buildings.length;i++){
        opt = document.createElement("option");
        opt.value = buildings[i].id_building;
        opt.innerHTML = buildings[i].building_name;
        select.appendChild(opt);
    }
}

function fillOptionsRooms(rooms,select){
    select.options.length=0;
    for(i=0;i<rooms.length;i++){
        opt = document.createElement("option");
        opt.value = rooms[i].id_space;
        opt.innerHTML = rooms[i].type + " n°" + rooms[i].space_number;
        select.appendChild(opt);
    }
}

floorSelect = document.getElementById("floor-selection");
floorSelect.addEventListener("change", function(){
    $.ajax({
        type: "GET",
        url: "http://172.31.249.232:8080/map/availableRoomsByFloor/" + floorSelect.value,
        success: function (rooms) {
            fillOptionsRooms(rooms,roomSelect);
            checkAvailability();
        }
    });
});

buildingSelect.addEventListener("change",function (){
    $.ajax({
        type: "GET",
        url: "http://172.31.249.232:8080/map/floor/" + buildingSelect.value,
        success: function (floors) {
            fillOptionsFloors(floors,floorSelect);
        }
    });
    $.ajax({
        type: "GET",
        url: "http://172.31.249.232:8080/map/availableRoomsByBuilding/" + buildingSelect.value,
        success: function (rooms) {
            fillOptionsRooms(rooms,roomSelect);
            checkAvailability();
        }
    });
});


function fillOptionsFloors(floors,select){
    select.options.length=0;
    for(i=0;i<floors.length;i++){
        opt = document.createElement("option");
        opt.value = floors[i].id_floor;
        opt.innerHTML = floors[i].floor_number;
        select.appendChild(opt);
    }
}

reservations = new Array();
$.ajax({
    type: "GET",
    url: "http://172.31.249.232:8080/reservation/allReservations",
    success: function (r) {
        reservations = r;
    }
});

openspaces = new Array();
$.ajax({
    type: "GET",
    url: "http://172.31.249.232:8080/map/openspaces/"+company,
    success: function (r) {
        openspaces = r;
    }
});

beginDate = document.getElementById("checkin-date");
beginHour = document.getElementById("begin");
var date = document.getElementById("rDate");
beginDate.addEventListener('input', function () {
    date.value = beginDate.value + " " + beginHour.value+":00";
    checkAvailability();
});
beginHour.addEventListener('input', function () {
    date.value = beginDate.value + " " + beginHour.value+":00";
    checkAvailability();
});

endDate = document.getElementById("checkout-date");
endHour = document.getElementById("end");
var end = document.getElementById("rEnd");
endDate.addEventListener('input', function () {
    end.value = endDate.value + " " + endHour.value+":00";
    checkAvailability();
});
endHour.addEventListener('input', function () {
    end.value = endDate.value + " " + endHour.value+":00";
    checkAvailability();
});

function checkAvailability(){
    var beginTime = new Date(date.value.replace(/-/g,"/"));
    var endTime = new Date(end.value.replace(/-/g,"/"));
    for(i=0;i<roomSelect.options.length;i++){
        var openspacecount = 0;
        for(j=0;j<reservations.length;j++){
            if(roomSelect.options[i].value == reservations[j].id_spacepk){
                for(k=0;k<openspaces.length;k++){
                    if(openspaces[k].id_space == reservations[j].id_spacepk){
                        if((beginTime<=new Date(reservations[j].end_date.replace(/-/g,"/")) && beginTime>=new Date(reservations[j].date.replace(/-/g,"/"))) || (endTime>=new Date(reservations[j].date.replace(/-/g,"/")) && endTime<=new Date(reservations[j].end_date.replace(/-/g,"/")))){
                            openspacecount++;
                            if(openspacecount==30){
                                roomSelect.options[i].remove();
                            }
                        }
                    }
                }
                if((beginTime<=new Date(reservations[j].end_date.replace(/-/g,"/")) && beginTime>=new Date(reservations[j].date.replace(/-/g,"/"))) || (endTime>=new Date(reservations[j].date.replace(/-/g,"/")) && endTime<=new Date(reservations[j].end_date.replace(/-/g,"/")))){
                    roomSelect.options[i].remove();
                }
            }
        }
    }
}