    $(document).ready(function() {
        setInterval('refreshPage()', 30000);
    });
    function refreshPage() {
        location.reload();
    }
     function drawMultipleLineChart(formatteddata, title, xtext, ytext){
        	Highcharts.chart('containerBar', {

            chart: {
                   type: 'column'
               },
               title: {
                   text: 'Production moyenne par mois'
               },
               subtitle: {
                   text: ''
               },
               xAxis: {
                   categories: [
                       'Jan',
                       'Feb',
                       'Mar',
                       'Apr',
                       'May',
                       'Jun',
                       'Jul',
                       'Aug',
                       'Sep',
                       'Oct',
                       'Nov',
                       'Dec'
                   ],
                   crosshair: true
               },
               yAxis: {
                   min: 0,
                   title: {
                       text: ' en KW'
                   }
               },
               tooltip: {
                   headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                   pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                       '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                   footerFormat: '</table>',
                   shared: true,
                   useHTML: true
               },
               plotOptions: {
                   column: {
                       pointPadding: 0.2,
                       borderWidth: 0
                   }
               },

                series: formatteddata,

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }

            });
        }
        function ajaxBarGraph(title, xtext, ytext){
         $.ajax({
                       url: 'http://172.31.249.232:8080/power/GraphBarData',
                       type: 'GET',
                       data: JSON.stringify(),
                       async: true,
                       contentType: "application/json; charset=utf-8",
                       dataType: "json",
                       success: function (result) {
                           console.log(result);
                           var formatteddata = [];
                           for(var key in result){
                                var singleObject = {
                                    name: '',
                                    data: []
                                }

                           	singleObject.name = key.toUpperCase();
                           	for(var i = 0; i < result[key].length; i++){
                           		singleObject.data.push(result[key][i]);
                           	}
                           	formatteddata.push(singleObject);
                           	}
                           	console.log(formatteddata);
                           	drawMultipleLineChart(formatteddata, title, xtext, ytext);
                       }});
        }


    ajaxBarGraph("Production moyenne par mois"," KW", "mois" )


















