package episen.pds.checkmate.repository.prototype;

import episen.pds.checkmate.model.prototype.Client;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class ClientProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public Iterable<Client> getClients() {
        ResponseEntity<Iterable<Client>> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/read",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Clients call " + response.getStatusCode());
        return response.getBody();
    }

    public Client getClient(int id) {
        ResponseEntity<Client> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/readbyid/" + id,
                HttpMethod.GET,
                null,
                Client.class
        );
        log.debug("Get Client call " + response.getStatusCode());
        return response.getBody();
    }

    public Client createClient(Client e) {
        ResponseEntity<Client> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/create",
                HttpMethod.POST,
                new HttpEntity<>(e),
                Client.class
        );
        log.debug("Create Client call " + response.getStatusCode());
        return response.getBody();
    }

    public Client updateClient(Client e) {
        ResponseEntity<Client> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/update/",
                HttpMethod.PUT,
                new HttpEntity<>(e),
                Client.class
        );
        log.debug("Update Client call " + response.getStatusCode());
        return response.getBody();
    }

    public void deleteClient(int id) {
        ResponseEntity<Void> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/deletebyid/" + id,
                HttpMethod.DELETE,
                null,
                Void.class);
        log.debug("Delete Client call " + response.getStatusCode());
    }
}


