package episen.pds.checkmate.service.alerting;

import episen.pds.checkmate.model.alerting.RseEmployee;
import episen.pds.checkmate.repository.alerting.RseEmployeeProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Data
public class RseEmployeeService {

    @Autowired
    private RseEmployeeProxy proxy;


    public RseEmployee login(String email) {
        return proxy.login(email);
    }
}
