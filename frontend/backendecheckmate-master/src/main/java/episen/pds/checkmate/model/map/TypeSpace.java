package episen.pds.checkmate.model.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TypeSpace {

    private Integer id_type_space;
    private String type;
    private Float size_percentage;
    private Float size_percentage2;

}
