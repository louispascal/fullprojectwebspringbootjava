package episen.pds.checkmate;

import lombok.Data;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Data
@SpringBootApplication
public class FrontendCheckmateApplication {


    public static void main(String[] args) {
        SpringApplication.run(FrontendCheckmateApplication.class, args);
    }

}
