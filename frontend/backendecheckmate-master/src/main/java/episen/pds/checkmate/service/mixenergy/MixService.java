package episen.pds.checkmate.service.mixenergy;

import episen.pds.checkmate.model.mixenergy.MixEnergy;
import episen.pds.checkmate.repository.mixenergy.MixProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Data
@Service
public class MixService {
    @Autowired
    private final MixProxy mixProxy;

    public MixEnergy getEnergy() {
        return mixProxy.getMix();
    }






}

