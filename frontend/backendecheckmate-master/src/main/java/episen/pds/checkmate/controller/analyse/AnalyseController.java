package episen.pds.checkmate.controller.analyse;

import episen.pds.checkmate.model.analyse.DashboardDto;

import episen.pds.checkmate.service.analyse.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.*;
import java.util.stream.Collectors;

@Controller()
@RequestMapping(path = "analyse")
public class AnalyseController {
    @Autowired
    private DashboardService dashboardService;


    @GetMapping("/dashboard")
    public String dashboard(Model model) {


        model.addAttribute("statistics", dashboardService.getStatistics().stream().collect(Collectors.toList()));
        model.addAttribute("productionAndConsumptionPerDistrict", dashboardService.getProductionAndConsumptionPerDistrict());
        return "analyse/dashboard.html";
    }

    @GetMapping("/districtDetails/{district}")
    public String districtDetails(@PathVariable("district") String district, Model model) {

        model.addAttribute("details", dashboardService.getProductionAndConsumptionPerBuilding(district));
        return "analyse/districtCompDetails.html";
    }

    @GetMapping("/districtHistory/{district}")
    public String districtHistory(@PathVariable("district") String district, Model model) {

        model.addAttribute("details", dashboardService.getDistrictHistory(district));
        return "analyse/districtCompDetails.html";
    }

    @GetMapping("/buildingDetails/{building}")
    public String buildingDetails(@PathVariable("building") String building, Model model) {

        model.addAttribute("details", dashboardService.getProductionAndConsumptionPerHome(building));
        return "analyse/buildingCompDetails.html";
    }

    @GetMapping("/buildingHistory/{building}")
    public String buildingHistory(@PathVariable("building") String building, Model model) {

        model.addAttribute("details", dashboardService.getBuildingHistory(building));
        return "analyse/buildingCompDetails.html";
    }

    @GetMapping("/homeDetails/{home}")
    public String homeDetails(@PathVariable("home") String home, Model model) {

        model.addAttribute("details", dashboardService.getProductionAndConsumptionPerEquipment(home));
        return "analyse/homeCompDetails.html";
    }

    @GetMapping("/homeHistory/{home}")
    public String homeHistory(@PathVariable("home") String home, Model model) {

        model.addAttribute("details", dashboardService.getHomeHistory(home));
        return "analyse/homeCompDetails.html";
    }

    @GetMapping("/equipmentHistory/{home}")
    public String equipmentHistory(@PathVariable("home") String home, Model model) {

        model.addAttribute("details", dashboardService.getEquipmentHistory(home));
        return "analyse/homeCompDetails.html";
    }

    
/*    @GetMapping("/")
    public String viewSolaireH(Model model) {
        List<InstallationSolar> equipements = equipementService.getEquipementsSolar();
        model.addAttribute("equipements", equipements);
        return "analyse/solaireH.html";
    }

    @GetMapping("/analyse/windH")
    public String viewWindH(Model model) {
        List<InstallationWind> equipements = equipementService.getEquipementsWind();
        model.addAttribute("equipements", equipements);
        return "analyse/windH.html";
    }
    //sbaron@etu.u-pec.fr


    @GetMapping("/analyse/chartSolaireH")
    public String viewChartSolaireHome(Model model) {
        List<InstallationSolar> equipements = equipementService.getEquipementsSolar();
        ArrayList<Integer> ids = new ArrayList<>();
        for (InstallationSolar equipement : equipements) {
            if (!ids.contains(equipement.getId_solar_panel())) {
                ids.add(equipement.getId_solar_panel());
            }
        }

        model.addAttribute("ids", ids);

        return "analyse/chartSolaireHome.html";
    }

    @GetMapping("/analyse/chartSolaireH/{id}")
    public String viewChartSolaireH(Model model, @PathVariable("id") final int idSolarP) {
        List<InstallationSolar> equipements = equipementService.getEquipementsSolarChart(idSolarP);
        Map<String, Double> graphData = new TreeMap<>();
        String d = equipements.get(0).getSampling_date().substring(0, 10);
        double eff = 0;
        for (int i = 0; i < equipements.size(); i++) {
            if (equipements.get(i).getSampling_date().substring(0, 10).equals(d)) {
                eff += equipements.get(i).getEfficacity();
            } else {
                graphData.put(d, eff);
                d = equipements.get(i).getSampling_date().substring(0, 10);
                eff = 0;
            }
        }

        model.addAttribute("chartData", graphData);

        return "analyse/chartSolaireH.html";
    }

    @GetMapping("/analyse/chartWindH/{id}")
    public String viewChartWindH(Model model, @PathVariable("id") final int idWind) {
        List<InstallationWind> equipements = equipementService.getEquipementsWindChart(idWind);
        Map<String, Double> graphData = new TreeMap<>();
        String d = equipements.get(0).getSampling_date().substring(0, 10);
        double eff = 0;
        for (int i = 0; i < equipements.size(); i++) {
            if (equipements.get(i).getSampling_date().substring(0, 10).equals(d)) {
                eff += equipements.get(i).getEfficacity();
            } else {
                graphData.put(d, eff);
                d = equipements.get(i).getSampling_date().substring(0, 10);
                eff = 0;
            }
        }

        model.addAttribute("chartData", graphData);
        return "analyse/chartWindH.html";
    }

    @GetMapping("/analyse/chartWindH")
    public String viewChartWindHome(Model model) {
        List<InstallationWind> equipements = equipementService.getEquipementsWind();
        ArrayList<Integer> ids = new ArrayList<>();
        for (InstallationWind equipement : equipements) {
            if (!ids.contains(equipement.getId_wind_turbine())) {
                ids.add(equipement.getId_wind_turbine());
            }
        }

        model.addAttribute("ids", ids);

        return "analyse/chartWindHome.html";
    }*/
}
