package episen.pds.checkmate.service.scope8;

import episen.pds.checkmate.repository.scope8.ConsumptionDwProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsumptionDwService {
    @Autowired

    private ConsumptionDwProxy consumptionDwProxy;

    public List<String[]> getConsumptionDwById(Integer id) {
        return consumptionDwProxy.getConsumptionDw(id);
    }
}
