package episen.pds.checkmate.repository.workspace;

import episen.pds.checkmate.model.workspace.Employee;
import episen.pds.checkmate.model.workspace.Access;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class EmployeeProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public Iterable<Employee> getEmployees(int id_company) {
        ResponseEntity<Iterable<Employee>> response = restTemplate.exchange(
                getBackUrl() + props.getApiUserUrl() + "/readEmployee/" + id_company,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Employees call " + response.getStatusCode());
        return response.getBody();
    }

    public Employee getEmployee(int id) {
        ResponseEntity<Employee> response = restTemplate.exchange(
                getBackUrl() + props.getApiUserUrl() + "/readEmployeebyid/" + id,
                HttpMethod.GET,
                null,
                Employee.class
        );
        log.debug("Get Employee call " + response.getStatusCode());
        return response.getBody();
    }

    public Employee createEmployee(Employee e) {
        ResponseEntity<Employee> response = restTemplate.exchange(
                getBackUrl() + props.getApiUserUrl() + "/createEmployee",
                HttpMethod.POST,
                new HttpEntity<>(e),
                Employee.class
        );
        log.debug("Create Client call " + response.getStatusCode());
        return response.getBody();
    }

    public Employee updateEmployee(Employee e) {
        ResponseEntity<Employee> response = restTemplate.exchange(
                getBackUrl() + props.getApiUserUrl() + "/updateEmployee/",
                HttpMethod.PUT,
                new HttpEntity<>(e),
                Employee.class
        );
        log.debug("Update Client call " + response.getStatusCode());
        return response.getBody();
    }

    public void deleteEmployee(int id) {
        ResponseEntity<Void> response = restTemplate.exchange(
                getBackUrl() + props.getApiUserUrl() + "/deleteEmployeebyid/" + id,
                HttpMethod.DELETE,
                null,
                Void.class);
        log.debug("Delete Client call " + response.getStatusCode());
    }

    public Employee login(String email) {
        ResponseEntity<Employee> response = restTemplate.exchange(
                getBackUrl() + props.getApiUserUrl() + "/loginEmployee/" + email,
                HttpMethod.GET,
                null,
                Employee.class);
        log.debug("Login Client call " + response.getStatusCode());
        return response.getBody();
    }

    public Access getAccess(int id_access) {
        ResponseEntity<Access> response = restTemplate.exchange(
                getBackUrl() + props.getApiUserUrl() + "/getAccess/" + id_access,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Employees call " + response.getStatusCode());
        return response.getBody();
    }
}
