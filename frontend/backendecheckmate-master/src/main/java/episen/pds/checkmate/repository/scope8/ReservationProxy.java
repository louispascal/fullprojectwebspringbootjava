package episen.pds.checkmate.repository.scope8;

import episen.pds.checkmate.model.map.Space;
import episen.pds.checkmate.model.scope8.Reservation;
import episen.pds.checkmate.model.workspace.Employee;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Component
public class ReservationProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public List<String[]> getReservations() {
        ResponseEntity<List<String[]>> response = restTemplate.exchange(
                getBackUrl() + props.getApiReservationUrl() + "/allReservationsjoin",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Reservations call " + response.getStatusCode());
        return response.getBody();
    }

    public Reservation getReservation(int id) {
        ResponseEntity<Reservation> response = restTemplate.exchange(
                getBackUrl() + props.getApiReservationUrl() + "/readreservationbyid/" + id,
                HttpMethod.GET,
                null,
                Reservation.class
        );
        log.debug("Get Reservation call " + response.getStatusCode());
        return response.getBody();
    }

    public Reservation createReservation(Reservation e) {
        ResponseEntity<Reservation> response = restTemplate.exchange(
                getBackUrl() + props.getApiReservationUrl() +"/createReservation",
                HttpMethod.POST,
                new HttpEntity<>(e),
                Reservation.class
        );
        log.debug("Create Reservation call " + response.getStatusCode());
        return response.getBody();
    }

    public Iterable<Reservation> getReservationById(int id) {
        ResponseEntity<Iterable<Reservation>> response = restTemplate.exchange(
                getBackUrl() + props.getApiReservationUrl() + "/reservationById/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Reservations call " + response.getStatusCode());
        return response.getBody();
    }

    public Iterable<Reservation> getMeetingsById(int id) {
        ResponseEntity<Iterable<Reservation>> response = restTemplate.exchange(
                getBackUrl() + props.getApiReservationUrl() + "/meetingsById/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Reservations call " + response.getStatusCode());
        return response.getBody();
    }

    public Iterable<Space> getReservedSpaces(int id) {
        ResponseEntity<Iterable<Space>> response = restTemplate.exchange(
                getBackUrl() + props.getApiMapUrl() + "/reservedSpaces/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Reservations call " + response.getStatusCode());
        return response.getBody();
    }

    public Reservation updateReservation(Reservation e) {
        ResponseEntity<Reservation> response = restTemplate.exchange(
                getBackUrl() + props.getApiReservationUrl() + "/updateReservation/",
                HttpMethod.PUT,
                new HttpEntity<>(e),
                Reservation.class
        );
        log.debug("Update Reservation call " + response.getStatusCode());
        return response.getBody();
    }

    public void deleteReservation(int id) {
        ResponseEntity<Void> response = restTemplate.exchange(
                getBackUrl() + props.getApiReservationUrl() + "/deleteReservationbyid/" + id,
                HttpMethod.DELETE,
                null,
                Void.class);
        log.debug("Delete Reservation call " + response.getStatusCode());
    }


}



