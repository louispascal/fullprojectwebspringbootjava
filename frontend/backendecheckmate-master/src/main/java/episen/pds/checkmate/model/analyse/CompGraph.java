package episen.pds.checkmate.model.analyse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompGraph {
    private String title;
    private Float value1;
    private Float value2;
    private Float ratio;

}
