package episen.pds.checkmate.model.equipment;

import lombok.Data;

@Data
public class Rooms {
    private Integer id_citizen;
    private String type;
    private Integer id_equipment;
    private String type_equipment;
    private Integer id_room;
}
