package episen.pds.checkmate.model.analyse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InstallationWind {

    private Integer id_installation_wind;
    private  Integer id_wind_turbine;
    private String name_building;
    private double production;
    private double consumption;
    private String sampling_date;

    private double efficacity;




}

