package episen.pds.checkmate.repository.map;

import episen.pds.checkmate.model.map.Floor;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class FloorProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public Floor getFloor(Integer id_floor) {
        ResponseEntity<Floor> response = restTemplate.exchange(
                getBackUrl() + props.getApiMapUrl() + "readbyid/" + id_floor,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Floor call " + response.getStatusCode());
        return response.getBody();
    }
}
