package episen.pds.checkmate.model.alerting;


import lombok.Data;

@Data
public class RseEmployee {

    private Integer id;
    private String name;
    private String firstname;
    private String email;
    private int age;
}
