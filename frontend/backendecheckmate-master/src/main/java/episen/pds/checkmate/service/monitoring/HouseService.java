package episen.pds.checkmate.service.monitoring;

import episen.pds.checkmate.model.monitoring.House;
import episen.pds.checkmate.repository.monitoring.HouseProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class HouseService {

    @Autowired
    private HouseProxy houseProxy;

    public House getHouse(final int id) {
        return houseProxy.getHouse(id);
    }

    public Iterable<House> getHouses() {
        return houseProxy.getHouses();
    }

    public void deleteHouse(final int id) {
        houseProxy.deleteHouse(id);
    }

    public House saveHouse(House house) {
        if (house.getId_house() == null) {
            return houseProxy.createHouse(house);
        }
        return houseProxy.updateHouse(house);
    }

}
