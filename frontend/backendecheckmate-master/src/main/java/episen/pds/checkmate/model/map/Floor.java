package episen.pds.checkmate.model.map;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Floor {

    private Integer id_floor;
    private Integer floor_number;
    private Integer id_building;
}
