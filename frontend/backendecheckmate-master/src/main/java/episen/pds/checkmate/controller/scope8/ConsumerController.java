package episen.pds.checkmate.controller.scope8;


import episen.pds.checkmate.service.scope8.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ConsumerController {

    @Autowired
    private ConsumerService consumerService;

    @GetMapping("/consumer/{id}")
    public String consumer(@PathVariable("id") final int id, Model model) {

        model.addAttribute("consumers", consumerService.getConsumer(id));
        return "scope8/ConsumerList";
    }
}
