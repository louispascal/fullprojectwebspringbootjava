package episen.pds.checkmate.service.alerting;

import episen.pds.checkmate.model.alerting.Alerte;
import episen.pds.checkmate.repository.alerting.AlerteProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class AlerteService {

    @Autowired
    private AlerteProxy alerteProxy;

    public Iterable<Alerte> getAlertes() {
        return alerteProxy.getAlertes();
    }

}
