package episen.pds.checkmate.controller.smartgrid;

import episen.pds.checkmate.model.smartgrid.District;
import episen.pds.checkmate.service.smartgrid.EnergyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Slf4j
@Controller
public class EnergyController {

    @Autowired
    private EnergyService energyService;

    @GetMapping("/homeEnergy") //Redirect to the main scope SmartGrid page
    public String home() {
        return "smartgrid/homeEnergy";
    }

    @GetMapping("/districtEnergy/{id}") //Redirect to the corresponding district page
    public String quarter(@PathVariable("id") final int id, Model model) {
        District d = energyService.getDistrict(id);
        model.addAttribute("district", d);
        return "smartgrid/districtEnergy";
    }

    @GetMapping("/prodsolar") //Redirect to the solar energy production page
    public String prodsolar() {
        return "smartgrid/prodsolar";
    }

    @GetMapping("/prodnuclear") //Redirect to the nuclear energy production page
    public String prodnuclear() {
        return "smartgrid/prodnuclear";
    }

    @GetMapping("/prodwind") //Redirect to the wind energy production page
    public String prodwind() {
        return "smartgrid/prodwind";
    }

    @GetMapping("/prodhydraulic") //Redirect to the hydraulic energy production page
    public String prodhydraulic() {
        return "smartgrid/prodydraulic";
    }


}
