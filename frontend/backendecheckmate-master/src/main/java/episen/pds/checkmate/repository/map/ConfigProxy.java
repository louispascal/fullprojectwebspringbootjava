package episen.pds.checkmate.repository.map;

import episen.pds.checkmate.model.map.MapRequest;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class ConfigProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public void sendConfig(MapRequest map) {
        ResponseEntity<MapRequest> response = restTemplate.exchange(
                getBackUrl() + props.getApiMapUrl() + "config/submit",
                HttpMethod.POST,
                new HttpEntity<>(map),
                MapRequest.class
        );
        log.debug("Send config " + response.getStatusCode());
    }
}
