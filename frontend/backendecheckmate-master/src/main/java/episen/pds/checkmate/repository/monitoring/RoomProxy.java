package episen.pds.checkmate.repository.monitoring;

import episen.pds.checkmate.model.monitoring.Room;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Component
public class RoomProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public Iterable<Room> getRooms() {
        ResponseEntity<Iterable<Room>> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/read",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Iterable<Room>>() {
                }
        );
        log.debug("Get Rooms call " + response.getStatusCode());
        return response.getBody();
    }

    public Room getRoom(int id) {
        ResponseEntity<Room> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/readbyid/" + id,
                HttpMethod.GET,
                null,
                Room.class
        );
        log.debug("Get Room call " + response.getStatusCode());
        return response.getBody();
    }

    public Room createRoom(Room e) {
        ResponseEntity<Room> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/create",
                HttpMethod.POST,
                new HttpEntity<>(e),
                Room.class
        );
        log.debug("Create Room call " + response.getStatusCode());
        return response.getBody();
    }

    public Room updateRoom(Room e) {
        ResponseEntity<Room> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/update/",
                HttpMethod.PUT,
                new HttpEntity<>(e),
                Room.class
        );
        log.debug("Update Room call " + response.getStatusCode());
        return response.getBody();
    }

    public void deleteRoom(int id) {
        ResponseEntity<Void> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/deletebyid/" + id,
                HttpMethod.DELETE,
                null,
                Void.class);
        log.debug("Delete Room call " + response.getStatusCode());
    }
    public List<Room> getRoomsByUser(Integer id_user) {
        ResponseEntity<List<Room>> response = restTemplate.exchange(
                getBackUrl() + props.getApiHeatingUrl() + "Rooms/" + id_user,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get all Rooms in User's house call" + response.getStatusCode());
        return response.getBody();

    }
}
