package episen.pds.checkmate.model.map;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Building {

    private Integer id_building;
    private String building_name;

}
