package episen.pds.checkmate.repository.analyse;

import episen.pds.checkmate.model.analyse.CompGraph;
import episen.pds.checkmate.model.analyse.DashboardDto;
import episen.pds.checkmate.model.analyse.GraphHistoryItem;

import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Component
public class DashboardProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public List<DashboardDto> getStatistics() {
        ResponseEntity<List<DashboardDto>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/statistics", HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Statistics call " + response.getStatusCode());
        return response.getBody();
    }

    public List<CompGraph> getProductionAndConsumptionPerDistrict() {
        ResponseEntity<List<CompGraph>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/getProductionAndConsumptionPerDistrict", HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get ProductionAndConsumptionPerDistrict call " + response.getStatusCode());
        return response.getBody();
    }

    public List<CompGraph> getProductionAndConsumptionPerBuilding(String district) {
        ResponseEntity<List<CompGraph>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/getProductionAndConsumptionPerBuilding/" + district, HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get ProductionAndConsumptionPerBuilding call " + response.getStatusCode());
        return response.getBody();
    }

    public List<CompGraph> getProductionAndConsumptionPerHome(String building) {
        ResponseEntity<List<CompGraph>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/getProductionAndConsumptionPerHome/" + building, HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get ProductionAndConsumptionPerHome call " + response.getStatusCode());
        return response.getBody();
    }

    public List<CompGraph> getProductionAndConsumptionPerEquipment(String home) {
        ResponseEntity<List<CompGraph>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/getProductionAndConsumptionPerEquipment/" + home, HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get ProductionAndConsumptionPerHome call " + response.getStatusCode());
        return response.getBody();
    }


    public List<CompGraph> getDistrictHistory(String name) {
        ResponseEntity<List<CompGraph>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/getDistrictHistory/" + name, HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get DistrictHistory call " + response.getStatusCode());
        return response.getBody();
    }

    public List<CompGraph> getBuildingHistory(String name) {
        ResponseEntity<List<CompGraph>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/getBuildingHistory/" + name, HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get getBuildingHistory call " + response.getStatusCode());
        return response.getBody();
    }

    public List<CompGraph> getHomeHistory(String name) {
        ResponseEntity<List<CompGraph>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/getHomeHistory/" + name, HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get getHomeHistory call " + response.getStatusCode());
        return response.getBody();
    }

    public List<CompGraph> getEquipmentHistory(String name) {
        ResponseEntity<List<CompGraph>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/getEquipmentHistory/" + name, HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get getEquipmentHistory call " + response.getStatusCode());
        return response.getBody();
    }


 /*   public List<InstallationSolar> getEquipementsSolarChart(int idSolarP) {
        ResponseEntity<List<InstallationSolar>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/equipementsSolarChart/" + idSolarP,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Equipements call " + response.getStatusCode());
        return response.getBody();
    }

    public List<InstallationWind> getEquipementsWind() {
        ResponseEntity<List<InstallationWind>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/equipementsWind",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Equipements call " + response.getStatusCode());
        return response.getBody();
    }

    public List<InstallationWind> getEquipementsWindChart(int idWind) {
        ResponseEntity<List<InstallationWind>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAnalyseUrl() + "/equipementsWindChart/" + idWind,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Equipements call " + response.getStatusCode());
        return response.getBody();
    }*/
}
