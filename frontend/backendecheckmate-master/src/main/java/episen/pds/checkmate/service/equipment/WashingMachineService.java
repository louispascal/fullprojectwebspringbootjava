package episen.pds.checkmate.service.equipment;

import episen.pds.checkmate.model.equipment.WashingMachine;
import episen.pds.checkmate.model.equipment.WashingMachine;
import episen.pds.checkmate.repository.equipment.WashingMachineProxy;
import episen.pds.checkmate.repository.equipment.WashingMachineProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WashingMachineService {
    @Autowired
    private WashingMachineProxy WashingMachineProxy;

    public WashingMachine setWashingMachine(WashingMachine washingwachine) {
        return WashingMachineProxy.setWashingMachineInfos(washingwachine);
    }

    public WashingMachine getWashingMachine(Integer id) {
        return WashingMachineProxy.getWashingMachine(id);
    }
}
