package episen.pds.checkmate.model.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class Area {

    private Integer id_area;
    private String area_localisation;
    private Integer id_floor;
    private Float capacity;
}
