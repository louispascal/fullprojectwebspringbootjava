package episen.pds.checkmate.controller.prototype;

import episen.pds.checkmate.model.prototype.Client;
import episen.pds.checkmate.service.prototype.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class ClientController {

    @Autowired
    private ClientService service;

    @GetMapping("/home")
    public String home(Model model) {
        model.addAttribute("clients", service.getClients());
        return "prototype/home";
    }


    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("client", new Client());
        return "prototype/formNewClient";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") final int id, Model model) {
        model.addAttribute("client", service.getClient(id));
        return "prototype/formUpClient";
    }

    @GetMapping("/deletebyid/{id}")
    public String delete(@PathVariable("id") final int id, Model model) {
        service.deleteClient(id);
        model.addAttribute("clients", service.getClients());
        return "prototype/home";
    }

    @PostMapping("/saveClient")
    public String saveClient(@ModelAttribute Client client, Model model) {
        service.saveClient(client);
        model.addAttribute("clients", service.getClients());
        return "prototype/home";
    }
}