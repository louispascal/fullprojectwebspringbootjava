package episen.pds.checkmate.repository.alerting;

import episen.pds.checkmate.model.alerting.Alerte;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class AlerteProxy {

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public Iterable<Alerte> getAlertes() {
        ResponseEntity<Iterable<Alerte>> response = restTemplate.exchange(
                getBackUrl() + props.getApiAlerteUrl() + "/read",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Alertes " + response.getStatusCode());
        return response.getBody();
    }

}
