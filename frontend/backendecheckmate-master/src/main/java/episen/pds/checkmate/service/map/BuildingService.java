package episen.pds.checkmate.service.map;

import episen.pds.checkmate.model.map.Building;
import episen.pds.checkmate.repository.map.BuildingProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class BuildingService {
    @Autowired
    private BuildingProxy buildingProxy;

    public Iterable<Building> getBuildings(Integer company) {
        return buildingProxy.getBuildings(company);
    }


}
