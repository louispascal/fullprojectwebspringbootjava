package episen.pds.checkmate.model.equipment;

import lombok.Data;

@Data
public class WashingMachine {
    private Integer id_washing_machine;
    private String mode;
    private Integer id_equipment;

}
