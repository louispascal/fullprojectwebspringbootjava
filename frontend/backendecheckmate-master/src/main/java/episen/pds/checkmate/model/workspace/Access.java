package episen.pds.checkmate.model.workspace;


import lombok.Data;

@Data
public class Access {

    private Integer id_access;
    private boolean has_equipments;
    private boolean has_monitoring_meeting_room;
    private boolean has_smart_city_activity;
    private boolean has_message;
    private boolean has_meeting;
    private boolean has_reservation;
    private boolean has_configuration_plan;
    private boolean has_plan;
    private boolean has_consumption;


}