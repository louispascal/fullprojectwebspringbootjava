package episen.pds.checkmate.model.workspace;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

    private Integer id_employee;
    private String name;
    private String firstName;
    private String job;
    private String email;
    private Integer id_company;
    private Integer id_access;
    private Integer id_status;


}