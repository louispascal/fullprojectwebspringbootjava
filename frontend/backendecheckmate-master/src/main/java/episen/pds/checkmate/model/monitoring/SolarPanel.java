/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package episen.pds.checkmate.model.monitoring;

import episen.pds.checkmate.model.smartgrid.Building;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Ines
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SolarPanel implements Serializable {

    private Integer idSolarPanel;
    private Building building;
    private Double production;
}
