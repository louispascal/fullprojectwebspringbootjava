package episen.pds.checkmate.model.monitoring;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EquipementRoom {
    private Integer id;
    private String code;
    private String libelle;
    private String typeEquipement;
    private Date dateInstallation;
    private String value;
    private Room room;
}