package episen.pds.checkmate.controller.mixenergy;

import episen.pds.checkmate.service.mixenergy.MixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class MixController {

    @Autowired
    private MixService mixService;

    @GetMapping("/totalEnergy")
    public String create(Model model) {
        model.addAttribute("mixEnergy", mixService.getEnergy());
        return "MixEnergy/MixEnergy";
    }

    @GetMapping("/smartgrid")
    public String getAlgo(){
        return "MixEnergy/LogicalSelector";
}


}