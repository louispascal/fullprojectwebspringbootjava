package episen.pds.checkmate.controller.equipment;

import episen.pds.checkmate.service.equipment.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {
    @Autowired
    private HomeService homeService;
    @GetMapping("/myHome/{id}")
    public String Home(@PathVariable Integer id ,Model model){
        System.out.println(2);
        model.addAttribute("Rooms", homeService.getRoomsByUser(id));
        return "equipment/House1";
    }
}
