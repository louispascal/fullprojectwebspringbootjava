package episen.pds.checkmate.model.analyse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DashboardDto {

    private Integer id;
    private String title;
    private Float value;
    private Boolean autoDisplay;
}

