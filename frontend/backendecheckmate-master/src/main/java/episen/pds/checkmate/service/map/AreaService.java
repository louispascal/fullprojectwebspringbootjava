package episen.pds.checkmate.service.map;

import episen.pds.checkmate.model.map.Area;
import episen.pds.checkmate.repository.map.AreaProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class AreaService {
    @Autowired
    private AreaProxy areaProxy;

    public Iterable<Area> getAreas(String building_name, Integer floor_number, String area) {
        return areaProxy.getAreas(building_name, floor_number, area);
    }


}
