package episen.pds.checkmate.controller.map;

import episen.pds.checkmate.model.map.MapRequest;
import episen.pds.checkmate.service.map.BuildingService;
import episen.pds.checkmate.service.map.MapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpSession;

@Slf4j
@Controller()
public class ConfigurationMapController {

    @Autowired
    private BuildingService buildingService;
    @Autowired
    private MapService mapService;


    @GetMapping("/config")
    public String viewConfigMap(Model model, HttpSession session) {
        Integer company = (Integer) session.getAttribute("company");
        model.addAttribute("buildings", buildingService.getBuildings(company));
        model.addAttribute("map", new MapRequest());
        return "map/configuration/mapconfig.html";
    }

    @GetMapping("/showPreview/{building}/{floor}")
    public String showMapPreview(@PathVariable(value = "building") Integer building, @PathVariable(value = "floor") String floor, Model model, HttpSession session) {
        Integer company = (Integer) session.getAttribute("company");
        model = mapService.buildMap(String.valueOf(building), floor, model);
        model.addAttribute("buildings", buildingService.getBuildings(company));
        return "map/configuration/mapPreview.html";
    }

    @GetMapping("/cancelConfig/{id_floor}")
    public String cancelConfig(@PathVariable("id_floor") int id_floor, Model model, HttpSession session) {
        mapService.deleteSpacesAndCorridors(id_floor);
        Integer company = (Integer) session.getAttribute("company");
        model.addAttribute("buildings", buildingService.getBuildings(company));
        model.addAttribute("map", new MapRequest());
        return "map/configuration/mapconfig.html";
    }

}