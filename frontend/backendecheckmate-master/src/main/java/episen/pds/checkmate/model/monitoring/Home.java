package episen.pds.checkmate.model.monitoring;

import episen.pds.checkmate.model.map.Building;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@AllArgsConstructor
@NoArgsConstructor
public class Home {
    private Integer idHome;
    private String type;
    private Building building;
}