package episen.pds.checkmate.controller.alerting;


import episen.pds.checkmate.service.alerting.AlerteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AlerteController {

    @Autowired
    private AlerteService alerteService;

    @GetMapping("/AlertesSection")
    public String home(Model model) {
        model.addAttribute("alertes", alerteService.getAlertes());
        return "alerting/AlertesList";
    }


}
