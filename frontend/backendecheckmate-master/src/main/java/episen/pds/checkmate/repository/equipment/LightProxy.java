package episen.pds.checkmate.repository.equipment;

import episen.pds.checkmate.model.equipment.Light;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class LightProxy {
    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }


    public Light setLightInfos(Light light) {
        ResponseEntity<Light> response = restTemplate.exchange(
                getBackUrl() + props.getApiLightUrl() + "setLight/",
                HttpMethod.POST,
                new HttpEntity<>(light),
                Light.class
        );
        log.debug("Get Light call " + response.getStatusCode());
        return response.getBody();
    }

    public Light getLight(Integer id) {
        ResponseEntity<Light> response = restTemplate.exchange(
                getBackUrl() + props.getApiLightUrl() + "Light/" + id.toString(),
                HttpMethod.GET,
                null,
                Light.class
        );
        log.debug("Get Light call " + response.getStatusCode());
        return response.getBody();
    }
}
