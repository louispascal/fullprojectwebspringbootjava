package episen.pds.checkmate.model.prototype;

import lombok.Data;

@Data
public class Client {

    private Integer id;
    private String name;
    private String firstName;
    private int age;
}