package episen.pds.checkmate.repository.scope8;

import episen.pds.checkmate.model.scope8.Consumer;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Slf4j
@Component
public class ConsumerProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public Consumer getConsumer(int id) {
        ResponseEntity<Consumer> response = restTemplate.exchange(
                getBackUrl() + props.getApiConsumerUrl() + "/consumer/" + id,
                HttpMethod.GET,
                null,
                Consumer.class
        );
        log.debug("Get Consumer call " + response.getStatusCode());
        return response.getBody();
    }
}