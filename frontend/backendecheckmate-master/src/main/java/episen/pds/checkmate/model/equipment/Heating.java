package episen.pds.checkmate.model.equipment;

import lombok.Data;

@Data
public class Heating {
    private Integer id_heating;
    private Integer value;
    private String mode;
    private Integer id_equipment;
}
