package episen.pds.checkmate.service.map;

import episen.pds.checkmate.model.map.Area;
import episen.pds.checkmate.model.map.Corridor;
import episen.pds.checkmate.model.map.Space;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class MapService {
    @Autowired
    private AreaService areaService;
    @Autowired
    private CorridorService corridorService;
    @Autowired
    private SpaceService spaceService;
    @Autowired
    private FloorService floorService;

    public Model buildMap(String building, String floor, Model model) {
        int id_floor = Integer.parseInt(floor);
        int id_building = Integer.parseInt(building);
        Iterable<Area> listAreaN = areaService.getAreas(building, id_floor, "N");
        Iterable<Area> listAreaS = areaService.getAreas(building, id_floor, "S");
        Iterable<Area> AreaNW = areaService.getAreas(building, id_floor, "NW");
        Iterable<Area> AreaNE = areaService.getAreas(building, id_floor, "NE");
        Iterable<Area> AreaSW = areaService.getAreas(building, id_floor, "SW");
        Iterable<Area> AreaSE = areaService.getAreas(building, id_floor, "SE");
        Iterable<Corridor> corridors = corridorService.getCorridors(building, id_floor);
        List<Space> spacesN = new ArrayList<>();
        List<Space> spacesS = new ArrayList<>();
        for (Area area : listAreaN) {
            spacesN = Stream.concat(spacesN.stream(), spaceService.getSpaces(building, id_floor, area.getId_area()).stream()).collect(Collectors.toList());
        }
        for (Area area : listAreaS) {
            spacesS = Stream.concat(spacesS.stream(), spaceService.getSpaces(building, id_floor, area.getId_area()).stream()).collect(Collectors.toList());
        }
        model.addAttribute("floor_number", floorService.getFloorNumber(id_floor));
        model.addAttribute("id_building", id_building);
        model.addAttribute("id_floor", id_floor);
        model.addAttribute("areaN", listAreaN);
        model.addAttribute("areaS", listAreaS);
        model.addAttribute("AreaNW", AreaNW);
        model.addAttribute("AreaNE", AreaNE);
        model.addAttribute("AreaSW", AreaSW);
        model.addAttribute("AreaSE", AreaSE);
        model.addAttribute("corridors", corridors);
        model.addAttribute("spacesN", spacesN);
        model.addAttribute("spacesS", spacesS);
        return model;
    }

    public void deleteSpacesAndCorridors(Integer id_floor) {
        spaceService.deleteSpacesAndCorridors(id_floor);
    }
}
