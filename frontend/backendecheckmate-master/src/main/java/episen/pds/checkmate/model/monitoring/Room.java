package episen.pds.checkmate.model.monitoring;

import episen.pds.checkmate.model.equipment.EquipmentEnergy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Room {
    private Integer idRoom;
    private String type;
    private String name;
    private Home home;
    private BigDecimal sommeConsumption;
    private Iterable<EquipmentEnergy> equipments;

}