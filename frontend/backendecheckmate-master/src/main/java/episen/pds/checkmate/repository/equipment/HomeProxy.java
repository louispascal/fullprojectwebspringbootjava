package episen.pds.checkmate.repository.equipment;

import episen.pds.checkmate.model.equipment.Rooms;
import episen.pds.checkmate.model.monitoring.Room;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.AsyncRestOperations;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Component
public class HomeProxy {


    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }


    public List<Rooms> getRoomsByUser(Integer id_user) {
        ResponseEntity<List<Rooms>> response = restTemplate.exchange(
                getBackUrl() + props.getApiEquipmentUrl() + "/myHome/" + id_user,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get all Rooms in User's house call" + response.getStatusCode());
        return response.getBody();
    }
}
