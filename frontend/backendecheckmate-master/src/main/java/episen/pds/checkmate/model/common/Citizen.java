package episen.pds.checkmate.model.common;


import lombok.Data;

@Data
public class Citizen {

    private Integer idCitizen;
    private String name;
    private String firstname;
    private String mail;
    private Integer id_role;
    private Integer idHome;

}
