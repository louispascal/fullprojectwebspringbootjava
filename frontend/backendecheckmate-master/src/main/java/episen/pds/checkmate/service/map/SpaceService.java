package episen.pds.checkmate.service.map;

import episen.pds.checkmate.model.map.Space;
import episen.pds.checkmate.repository.map.SpaceProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Data
@Service
public class SpaceService {
    @Autowired
    private SpaceProxy spaceProxy;

    public List<Space> getSpaces(String building_name, Integer id_floor, Integer id_area) {
        return spaceProxy.getSpaces(building_name, id_floor, id_area);
    }

    public void deleteSpacesAndCorridors(Integer id_floor) {
        spaceProxy.deleteSpacesAndCorridors(id_floor);
    }
}
