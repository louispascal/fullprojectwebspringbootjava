/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package episen.pds.checkmate.model.monitoring;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Ines
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductionWindTurbine implements Serializable {
    
    private Integer idProductionWindTurbine;
    private Double instantaneousPower;
    private Date samplingDate;
    private WindTurbine windTurbine;

}
