package episen.pds.checkmate.model.common;


import lombok.Data;

@Data
public class Role {

    private Integer idRole;
    private boolean configureEquipment;
    private boolean monitoringEquipment;
    private boolean smartGrid;
    private boolean alertingCitizen;
    private boolean alertingRse;
    private boolean mixEnergy;
    private boolean smartCity;

}
