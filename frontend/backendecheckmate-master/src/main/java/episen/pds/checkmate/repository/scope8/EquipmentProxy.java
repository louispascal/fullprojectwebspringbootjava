package episen.pds.checkmate.repository.scope8;

import episen.pds.checkmate.model.scope8.Equipment;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Component
public class EquipmentProxy {
    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }


    public List<Equipment> getEquipment(int idconsumer) {
        ResponseEntity<List<Equipment>> response = restTemplate.exchange(
                getBackUrl() + props.getApiEquipmentUrl() + "/equipmentcons/" + idconsumer,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Clients call " + response.getStatusCode());

        return response.getBody();
    }

    public Iterable<Equipment> getEquipments() {
        ResponseEntity<Iterable<Equipment>> response = restTemplate.exchange(
                getBackUrl() + props.getApiEquipmentUrl() + "/allEquipments",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Clients call " + response.getStatusCode());
        return response.getBody();
    }


}