package episen.pds.checkmate.model.monitoring;

import episen.pds.checkmate.model.equipment.EquipmentEnergy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Consumption {
    private Integer idConsumption;
    private Double instantaneousPower;
    private Date samplingDate;
    private EquipmentEnergy equipment;
}