package episen.pds.checkmate.repository.monitoring;

import episen.pds.checkmate.model.equipment.EquipmentEnergy;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class EquipmentEnergyProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public Iterable<EquipmentEnergy> getEquipmentEnergys() {
        ResponseEntity<Iterable<EquipmentEnergy>> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/read",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Iterable<EquipmentEnergy>>() {
                }
        );
        log.debug("Get Equipement Rooms call " + response.getStatusCode());
        return response.getBody();
    }

    public EquipmentEnergy getEquipmentEnergy(int id) {
        ResponseEntity<EquipmentEnergy> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/readbyid/" + id,
                HttpMethod.GET,
                null,
                EquipmentEnergy.class
        );
        log.debug("Get Equipement Room call " + response.getStatusCode());
        return response.getBody();
    }

    public EquipmentEnergy createEquipmentEnergy(EquipmentEnergy e) {
        ResponseEntity<EquipmentEnergy> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/create",
                HttpMethod.POST,
                new HttpEntity<>(e),
                EquipmentEnergy.class
        );
        log.debug("Create Equipement Room call " + response.getStatusCode());
        return response.getBody();
    }

    public EquipmentEnergy updateEquipmentEnergy(EquipmentEnergy e) {
        ResponseEntity<EquipmentEnergy> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/update/",
                HttpMethod.PUT,
                new HttpEntity<>(e),
                EquipmentEnergy.class
        );
        log.debug("Update Equipement Room call " + response.getStatusCode());
        return response.getBody();
    }

    public void deleteEquipmentEnergy(int id) {
        ResponseEntity<Void> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/deletebyid/" + id,
                HttpMethod.DELETE,
                null,
                Void.class);
        log.debug("Delete Equipement Room call " + response.getStatusCode());
    }
}
