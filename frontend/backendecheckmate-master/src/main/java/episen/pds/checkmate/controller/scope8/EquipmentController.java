package episen.pds.checkmate.controller.scope8;

import episen.pds.checkmate.service.scope8.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@Controller
public class EquipmentController {

    @Autowired
    private EquipmentService equipmentService;

    @GetMapping("/equipmentcons/{id}")
    public String Equipment(@PathVariable("id") final int id, Model model) {

        model.addAttribute("equipments", equipmentService.getEquipment(id));
        return "scope8/equipmentListCons";
    }

    @GetMapping("/allEquipments")
    public String EquipmentsAll(Model model) {
        model.addAttribute("equipmentsall", equipmentService.getEquipments());
        return "scope8/ShowEquipments";
    }


}
