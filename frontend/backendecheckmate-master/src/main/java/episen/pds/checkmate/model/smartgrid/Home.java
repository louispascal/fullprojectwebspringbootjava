package episen.pds.checkmate.model.smartgrid;
import lombok.Data;

@Data
public class Home {
    /* FROM TABLE HOME */
    private Integer id_home;
    private String type;
    private Integer id_building;
    private Integer id_citizen;

}
