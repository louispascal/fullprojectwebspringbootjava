package episen.pds.checkmate.service.analyse;

import episen.pds.checkmate.model.analyse.CompGraph;
import episen.pds.checkmate.model.analyse.DashboardDto;
import episen.pds.checkmate.model.analyse.GraphHistoryItem;
import episen.pds.checkmate.repository.analyse.DashboardProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Data
@Service
public class DashboardService {
    @Autowired
    private DashboardProxy dashboardProxy;

    public List<DashboardDto> getStatistics() {
        return dashboardProxy.getStatistics();
    }

    public List<CompGraph> getProductionAndConsumptionPerDistrict() {
        return dashboardProxy.getProductionAndConsumptionPerDistrict();
    }

    public List<CompGraph> getProductionAndConsumptionPerBuilding(String district) {
        return dashboardProxy.getProductionAndConsumptionPerBuilding(district);
    }

    public List<CompGraph> getProductionAndConsumptionPerHome(String building) {
        return dashboardProxy.getProductionAndConsumptionPerHome(building);
    }

    public List<CompGraph> getProductionAndConsumptionPerEquipment(String home) {
        return dashboardProxy.getProductionAndConsumptionPerEquipment(home);
    }

    public List<CompGraph> getDistrictHistory(String name) {
        return dashboardProxy.getDistrictHistory(name);
    }

    public List<CompGraph> getBuildingHistory(String name) {
        return dashboardProxy.getBuildingHistory(name);
    }

    public List<CompGraph> getHomeHistory(String name) {
        return dashboardProxy.getHomeHistory(name);
    }

    public List<CompGraph> getEquipmentHistory(String name) {
        return dashboardProxy.getEquipmentHistory(name);
    }

/*
    public List<InstallationSolar> getEquipementsSolarChart(int idSolarP) {
        return equipementProxy.getEquipementsSolarChart(idSolarP);
    }


    public List<InstallationWind> getEquipementsWind() {
        return equipementProxy.getEquipementsWind();
    }
    public List<InstallationWind> getEquipementsWindChart(int idWind) {
        return equipementProxy.getEquipementsWindChart(idWind);
    }
*/


}
