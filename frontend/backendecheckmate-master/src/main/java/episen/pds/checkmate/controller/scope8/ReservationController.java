package episen.pds.checkmate.controller.scope8;

import episen.pds.checkmate.model.scope8.Reservation;
import episen.pds.checkmate.service.scope8.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;


@Controller
public class ReservationController {

    @Autowired
    private ReservationService service;

    @GetMapping("/allReservationsjoin")
    public String home(Model model) {
        List<String[]> ok = service.getReservations();

        model.addAttribute("Reservations", service.getReservations());
        return "scope8/AllReservations";
    }


    @PostMapping("/createReservation/{id}")
    public String create(@ModelAttribute("reservation") Reservation reservation,@PathVariable Integer id,Model model) {
        reservation.setId_employee(id);
        service.addReservation(reservation);
        model.addAttribute("reservations", service.getReservationById(id));
        model.addAttribute("reservedSpaces", service.getReservedSpaces(id));
        return "WorkspaceServices/MyReservation";
    }

    @GetMapping("/myReservation/{id}")
    public String reservationById(@PathVariable Integer id,Model model) {
        model.addAttribute("reservations", service.getReservationById(id));
        model.addAttribute("reservedSpaces", service.getReservedSpaces(id));
        return "WorkspaceServices/MyReservation";
    }

    @GetMapping("/myMeetings/{id}")
    public String meetingsById(@PathVariable Integer id,Model model) {
        model.addAttribute("meetings", service.getMeetingsById(id));
        return "WorkspaceServices/MyMeetings";
    }
    /*
    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") final int id, Model model) {
        model.addAttribute("Reservation", service.getReservation(id));
        return "formUpReservation";
    }

    @GetMapping("/deletebyid/{id}")
    public String delete(@PathVariable("id") final int id, Model model) {
        service.deleteReservation(id);
        model.addAttribute("Reservations", service.getReservations());
        return "home";
    }

   /* @PostMapping("/saveReservation")
    public String saveReservation(@ModelAttribute Reservation Reservation, Model model) {
        service.saveReservation(Reservation);
        model.addAttribute("Reservations", service.getReservations());
        return "home";
    }*/

}
