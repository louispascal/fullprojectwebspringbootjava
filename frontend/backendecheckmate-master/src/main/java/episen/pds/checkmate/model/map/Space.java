package episen.pds.checkmate.model.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Space {

    private Integer id_space;
    private Integer space_number;
    private Integer position_in_area;
    private Integer id_area;
    private Integer id_type_space;
    private String type;
}
