package episen.pds.checkmate.controller.equipment;

import episen.pds.checkmate.model.equipment.WashingMachine;
import episen.pds.checkmate.service.equipment.WashingMachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class WashingMachineController {
    @Autowired
    private WashingMachineService washingmachineService;

    @GetMapping("myHome/WashingMachine/{id}")
    public String washingmachine(@PathVariable String id, Model model) {
        model.addAttribute("washingmachine", washingmachineService.getWashingMachine(Integer.parseInt(id)));
        return "equipment/WashingMachine";
    }

    @PostMapping("myHome/setWashingMachine")
    public String updateWashingMachine(@ModelAttribute("washingmachine") WashingMachine washingmachine, Model model) {
        washingmachineService.setWashingMachine(washingmachine);
        return "equipment/WashingMachine";
    }
}