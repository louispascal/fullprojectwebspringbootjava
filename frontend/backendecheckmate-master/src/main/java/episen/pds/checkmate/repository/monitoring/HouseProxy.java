package episen.pds.checkmate.repository.monitoring;

import episen.pds.checkmate.model.monitoring.House;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class HouseProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public Iterable<House> getHouses() {
        ResponseEntity<Iterable<House>> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/read",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Iterable<House>>() {
                }
        );
        log.debug("Get Houses call " + response.getStatusCode());
        return response.getBody();
    }

    public House getHouse(int id) {
        ResponseEntity<House> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/readbyid/" + id,
                HttpMethod.GET,
                null,
                House.class
        );
        log.debug("Get House call " + response.getStatusCode());
        return response.getBody();
    }

    public House createHouse(House e) {
        ResponseEntity<House> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/create",
                HttpMethod.POST,
                new HttpEntity<>(e),
                House.class
        );
        log.debug("Create House call " + response.getStatusCode());
        return response.getBody();
    }

    public House updateHouse(House e) {
        ResponseEntity<House> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/update/",
                HttpMethod.PUT,
                new HttpEntity<>(e),
                House.class
        );
        log.debug("Update House call " + response.getStatusCode());
        return response.getBody();
    }

    public void deleteHouse(int id) {
        ResponseEntity<Void> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/deletebyid/" + id,
                HttpMethod.DELETE,
                null,
                Void.class);
        log.debug("Delete House call " + response.getStatusCode());
    }
}
