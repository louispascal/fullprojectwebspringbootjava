package episen.pds.checkmate.model.scope8;

import lombok.Data;

@Data
public class Consumer {

    private Integer id;
    private String name;

}
