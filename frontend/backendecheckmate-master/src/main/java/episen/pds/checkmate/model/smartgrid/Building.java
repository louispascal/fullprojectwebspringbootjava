package episen.pds.checkmate.model.smartgrid;
import lombok.Data;

@Data
public class Building {
    /* FROM TABLE BUILDING */
    private Integer id_building;
    private Integer id_wind_turbine;
    private String address;
    private Integer id_district;
    private String typeBuilding;
    
}
