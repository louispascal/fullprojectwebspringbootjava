package episen.pds.checkmate.controller.workspace;

import episen.pds.checkmate.model.common.Citizen;
import episen.pds.checkmate.model.scope8.Reservation;
import episen.pds.checkmate.model.workspace.Employee;
import episen.pds.checkmate.service.workspace.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;


@Controller
public class UserController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employees/{id}")
    public String employees(@PathVariable Integer id, Model model) {
        model.addAttribute("employees", employeeService.getEmployees(id));
        return "WorkspaceServices/EmployeeList";
    }

    @GetMapping("/roomReservation/")
    public String meeting(Model model) {
        model.addAttribute("reservation", new Reservation());
        return "WorkspaceServices/RoomReservation";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute("employee") Employee employee, Model model, HttpSession session) {
        Employee loggedAs = employeeService.login(employee.getEmail());
        if (loggedAs == null) {
            model.addAttribute("citizen", new Citizen());
            model.addAttribute("employee", new Employee());
            model.addAttribute("errorDWP", true);
            model.addAttribute("error", false);
            return "home/homepage.html";
        }
        session.setMaxInactiveInterval(600);
        session.setAttribute("loggedAs", loggedAs);
        model.addAttribute("company", loggedAs.getId_company());
        session.setAttribute("company", loggedAs.getId_company());
        session.setAttribute("roles", employeeService.getAccess(loggedAs.getId_access()));
        return "home/homeDWP.html";
    }
}