package episen.pds.checkmate.repository.scope8;

import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Component
public class ConsumptionDwProxy {
    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }


    public List<String[]> getConsumptionDw(int idconsumer) {
        ResponseEntity<List<String[]>> response = restTemplate.exchange(
                getBackUrl() + props.getApiConsumptionDwUrl() + "/consumption/" + idconsumer,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Clients call " + response.getStatusCode());

        return response.getBody();
    }
}
