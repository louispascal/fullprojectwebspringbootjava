package episen.pds.checkmate.service.monitoring;

import episen.pds.checkmate.model.monitoring.Home;
import episen.pds.checkmate.model.monitoring.Consumption;
import episen.pds.checkmate.model.equipment.EquipmentEnergy;
import episen.pds.checkmate.model.monitoring.Room;
import episen.pds.checkmate.model.monitoring.SolarPanel;
import episen.pds.checkmate.model.monitoring.WindTurbine;
import episen.pds.checkmate.repository.monitoring.MonitoringProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import episen.pds.checkmate.model.monitoring.ProductionSolarPanel;
import episen.pds.checkmate.model.monitoring.ProductionWindTurbine;

@Data
@Service
public class MonitoringService {

    @Autowired
    private MonitoringProxy monitoringProxy;

    public Iterable<Home> getHomes() {
        return monitoringProxy.getHomes();
    }

    public Home getHomeById(int id) {
        return monitoringProxy.getHomeById(id);
    }

    public Room getRoomById(int id) {
        return monitoringProxy.getRoomById(id);
    }

    public EquipmentEnergy getEquipmentById(int id) {
        return monitoringProxy.getEquipmentById(id);
    }

    public Iterable<Room> getRoomsByIdHome(int idHome) {
        return monitoringProxy.getRoomByIdHome(idHome);
    }
    
    public Iterable<WindTurbine> getWindTurbineByIdBuilding(int idBuilding) {
        return monitoringProxy.getWindTurbineByIdBuilding(idBuilding);
    }
    
    public Iterable<SolarPanel> getSolarPanelByIdBuilding(int idBuilding) {
        return monitoringProxy.getSolarPanelByIdBuilding(idBuilding);
    }

    public Iterable<EquipmentEnergy> getEquipmentsByIdHome(int idHome) {
        return monitoringProxy.getEquipmentByIdHome(idHome);
    }

    public Iterable<EquipmentEnergy> getEquipmentsByIdRoom(int idRoom) {
        return monitoringProxy.getEquipmentByIdRoom(idRoom);
    }
    
    public Iterable<Consumption> getConsumptionsByIdEquipment(int idEquipment) {
        return monitoringProxy.getConsumptionByIdEquipment(idEquipment);
    }


}
