//package episen.pds.checkmate.controller.alerting;
//
//
//import episen.pds.checkmate.model.RseEmployee;
//import episen.pds.checkmate.model.workspace.Employee;
//import episen.pds.checkmate.service.AlerteService;
//import episen.pds.checkmate.service.RseEmployeeService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PostMapping;
//
//import javax.servlet.http.HttpSession;
//
//
//@Controller
//public class RseEmployeeController {
//
//    @Autowired
//    private RseEmployeeService rseEmployeeService;
//    @Autowired
//    private AlerteService alerteService;
//
//    @GetMapping("/alert")
//    public String viewAlerts(Model model) {
//        model.addAttribute("alertes", alerteService.getAlertes());
//        return "alerting/AlertesList";
//    }
//
//
//    @PostMapping("/rse/checklogin")
//    public String login(@ModelAttribute("rseemployee") RseEmployee rseEmployee, Model model, HttpSession session) {
//        RseEmployee loggedAs = rseEmployeeService.login(rseEmployee.getEmail());
//        if (loggedAs == null) {
//            model.addAttribute("rseemployee", new RseEmployee());
//            model.addAttribute("employee", new Employee());
//            model.addAttribute("error", true);
//            model.addAttribute("errorDWP", false);
//            return "home/homepage.html";
//        } else {
//            session.setAttribute("loggedAs", loggedAs);
//            return "redirect:/homeSmartGrid";
//        }
//    }
//
//
//}
