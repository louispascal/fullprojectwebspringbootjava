package episen.pds.checkmate.controller.equipment;

import episen.pds.checkmate.model.equipment.Heating;
import episen.pds.checkmate.service.equipment.HeatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.websocket.server.PathParam;

@Controller
public class HeatingController {
    @Autowired
    private HeatingService heatingService;

    @GetMapping("myHome/Heating/{id}")
    public String heating (@PathVariable String id, Model model){
        model.addAttribute("heating",heatingService.getHeating(Integer.parseInt(id)));
        return "equipment/Heating";
    }
    @PostMapping("/setHeating")
    public String updateHeating(@ModelAttribute("heating") Heating heating, Model model) {
        System.out.println(heating.getId_equipment());
        System.out.println(heating.getValue());
        System.out.println(heating.getMode());
        heatingService.setHeating(heating);
        return "equipment/Heating";
    }
    @PostMapping("myHome/AutoHeating")
    public String autoHeating( Model model) {
        System.out.println("auto");
        heatingService.autoHeating();
        return  "equipment/Heating";
    }
}
