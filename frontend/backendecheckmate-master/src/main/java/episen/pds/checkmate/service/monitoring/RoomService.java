package episen.pds.checkmate.service.monitoring;

import episen.pds.checkmate.model.monitoring.Room;
import episen.pds.checkmate.repository.monitoring.RoomProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Data
@Service
public class RoomService {

    @Autowired

    private RoomProxy roomProxy;

    public Room getRoom(final int id) {
        return roomProxy.getRoom(id);
    }

    public Iterable<Room> getRooms() {
        return roomProxy.getRooms();
    }

    public void deleteRoom(final int id) {
        roomProxy.deleteRoom(id);
    }

    public Room saveRoom(Room room) {
        if (room.getIdRoom() == null) {
            return roomProxy.createRoom(room);
        }
        return roomProxy.updateRoom(room);
    }

}
