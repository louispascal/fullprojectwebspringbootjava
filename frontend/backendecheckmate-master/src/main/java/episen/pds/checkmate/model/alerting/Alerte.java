package episen.pds.checkmate.model.alerting;


import lombok.Data;

@Data
public class Alerte {

    private Integer alerteid;
    private String message;

}
