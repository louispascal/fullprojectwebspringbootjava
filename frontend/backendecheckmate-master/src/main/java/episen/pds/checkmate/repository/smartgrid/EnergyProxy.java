package episen.pds.checkmate.repository.smartgrid;

import episen.pds.checkmate.model.smartgrid.District;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class EnergyProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }
    public District getDistrictInfos(int id) {
        ResponseEntity<District> response = restTemplate.exchange(
                getBackUrl() + props.getApiDistrictUrl() + "/readbydistrict/" + id,
                HttpMethod.GET,
                null,
                District.class
        );
        log.debug("Get District call " + response.getStatusCode());
        return response.getBody();
    }

}


