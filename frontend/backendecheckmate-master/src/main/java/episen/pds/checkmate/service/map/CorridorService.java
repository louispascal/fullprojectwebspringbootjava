package episen.pds.checkmate.service.map;

import episen.pds.checkmate.model.map.Corridor;
import episen.pds.checkmate.repository.map.CorridorProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class CorridorService {
    @Autowired
    private CorridorProxy corridorProxy;

    public Iterable<Corridor> getCorridors(String building_name, Integer floor_number) {
        return corridorProxy.getCorridors(building_name, floor_number);
    }


}
