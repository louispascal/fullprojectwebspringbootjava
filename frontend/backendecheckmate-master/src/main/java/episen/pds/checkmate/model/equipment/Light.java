package episen.pds.checkmate.model.equipment;

import lombok.Data;

@Data
public class Light {
    private Integer id_light;
    private Integer value;
    private Integer id_equipment;

}
