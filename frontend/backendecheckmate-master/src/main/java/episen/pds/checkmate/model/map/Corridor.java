package episen.pds.checkmate.model.map;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Corridor {

    private Integer id_corridor;
    private Integer id_floor;
    private String orientation;

}
