package episen.pds.checkmate.model.smartgrid;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class District {
    /* FROM TABLE DISTRICT */
    private Integer id_district;
    /* FROM TABLE CONSUMPTION_HOME AND PRODUCTION_HOME */
    private Integer consumption;
    private Integer production;
    private String sampling_date;
}
