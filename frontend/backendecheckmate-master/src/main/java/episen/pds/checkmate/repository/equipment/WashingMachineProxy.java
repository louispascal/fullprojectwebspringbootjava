package episen.pds.checkmate.repository.equipment;

import episen.pds.checkmate.model.equipment.WashingMachine;
import episen.pds.checkmate.model.equipment.WashingMachine;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class WashingMachineProxy {
    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }


    public WashingMachine setWashingMachineInfos(WashingMachine washingmachine) {
        ResponseEntity<WashingMachine> response = restTemplate.exchange(
                getBackUrl() + props.getApiWashingMachineUrl() + "setWashingMachine/",
                HttpMethod.POST,
                new HttpEntity<>(washingmachine),
                WashingMachine.class
        );
        log.debug("Get WashingMachine call " + response.getStatusCode());
        return response.getBody();
    }

    public WashingMachine getWashingMachine(Integer id) {
        ResponseEntity<WashingMachine> response = restTemplate.exchange(
                getBackUrl() + props.getApiWashingMachineUrl() + "WashingMachine/" + id.toString(),
                HttpMethod.GET,
                null,
                WashingMachine.class
        );
        log.debug("Get WashingMachine call " + response.getStatusCode());
        return response.getBody();
    }
}

