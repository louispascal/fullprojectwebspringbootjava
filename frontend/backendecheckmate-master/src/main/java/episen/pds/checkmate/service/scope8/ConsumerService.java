package episen.pds.checkmate.service.scope8;

import episen.pds.checkmate.model.scope8.Consumer;
import episen.pds.checkmate.repository.scope8.ConsumerProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Data
@Service
public class ConsumerService {

    @Autowired
    private ConsumerProxy consumerProxy;

    public Consumer getConsumer(final int id) {
        return consumerProxy.getConsumer(id);
    }
}
