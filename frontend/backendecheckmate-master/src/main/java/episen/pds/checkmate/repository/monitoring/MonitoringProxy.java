package episen.pds.checkmate.repository.monitoring;

import episen.pds.checkmate.model.monitoring.Home;
import episen.pds.checkmate.model.monitoring.Consumption;
import episen.pds.checkmate.model.equipment.EquipmentEnergy;
import episen.pds.checkmate.model.monitoring.Room;
import episen.pds.checkmate.model.monitoring.SolarPanel;
import episen.pds.checkmate.model.monitoring.WindTurbine;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class MonitoringProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public Iterable<Home> getHomes() {
        ResponseEntity<Iterable<Home>> response = restTemplate.exchange(getBackUrl() + props.getApiMonitoringUrl() + "/homes",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Iterable<Home>>() {
                }
        );
        log.debug("Get homes call " + response.getStatusCode());
        return response.getBody();
    }

    public Home getHomeById(int id) {
        ResponseEntity<Home> response = restTemplate.exchange(getBackUrl() + props.getApiMonitoringUrl() + "/home/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Home>() {
                }
        );
        log.debug("Get homes call " + response.getStatusCode());
        return response.getBody();
    }

    public Iterable<Room> getRoomByIdHome(int id) {
        ResponseEntity<Iterable<Room>> response = restTemplate.exchange(
                getBackUrl() + props.getApiMonitoringUrl() + "/home/room/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Iterable<Room>>() {
                }
        );
        log.debug("Get rooms by id_home " + response.getStatusCode());
        return response.getBody();
    }

    public Room getRoomById(int id) {
        ResponseEntity<Room> response = restTemplate.exchange(
                getBackUrl() + props.getApiMonitoringUrl() + "/room/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Room>() {
                }
        );
        log.debug("Get room by id " + response.getStatusCode());
        return response.getBody();
    }

    public EquipmentEnergy getEquipmentById(int id) {
        ResponseEntity<EquipmentEnergy> response = restTemplate.exchange(getBackUrl() + props.getApiMonitoringUrl() + "/equipment/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<EquipmentEnergy>() {
                }
        );
        log.debug("Get Equipment by id " + response.getStatusCode());
        return response.getBody();
    }

    public Iterable<EquipmentEnergy> getEquipmentByIdHome(int id) {
        ResponseEntity<Iterable<EquipmentEnergy>> response = restTemplate.exchange(getBackUrl() + props.getApiMonitoringUrl() + "/home/equipment/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Iterable<EquipmentEnergy>>() {
                }
        );
        log.debug("Get equipment by id_home " + response.getStatusCode());
        return response.getBody();
    }

    public Iterable<EquipmentEnergy> getEquipmentByIdRoom(int id) {
        ResponseEntity<Iterable<EquipmentEnergy>> response = restTemplate.exchange(getBackUrl() + props.getApiMonitoringUrl() + "/room/equipment/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Iterable<EquipmentEnergy>>() {
                }
        );
        log.debug("Get equipment by id_room " + response.getStatusCode());
        return response.getBody();
    }

    public Iterable<Consumption> getConsumptionByIdEquipment(int id) {
        ResponseEntity<Iterable<Consumption>> response = restTemplate.exchange(
                getBackUrl() + props.getApiMonitoringUrl() + "/equipment/consumption/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Iterable<Consumption>>() {
                }
        );
        log.debug("Get equipment by idEquipment " + response.getStatusCode());
        return response.getBody();
    }
        
    public Iterable<WindTurbine> getWindTurbineByIdBuilding(int id) {
        ResponseEntity<Iterable<WindTurbine>> response = restTemplate.exchange(
                getBackUrl() + props.getApiMonitoringUrl() + "/building/windTurbine/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Iterable<WindTurbine>>() {
                }
        );
        log.debug("Get wind_turbines by id_building " + response.getStatusCode());
        return response.getBody();
    }
        
    public Iterable<SolarPanel> getSolarPanelByIdBuilding(int id) {
        ResponseEntity<Iterable<SolarPanel>> response = restTemplate.exchange(
                getBackUrl() + props.getApiMonitoringUrl() + "/building/solarPanel/" + id,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Iterable<SolarPanel>>() {
                }
        );
        log.debug("Get solar_panel by id_building " + response.getStatusCode());
        return response.getBody();
    }
    
    
}
