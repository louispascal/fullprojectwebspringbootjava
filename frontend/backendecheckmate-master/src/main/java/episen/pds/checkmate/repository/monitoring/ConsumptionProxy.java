package episen.pds.checkmate.repository.monitoring;

import episen.pds.checkmate.model.monitoring.Consumption;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class ConsumptionProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public Iterable<Consumption> getConsumptions() {
        ResponseEntity<Iterable<Consumption>> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/read",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Iterable<Consumption>>() {
                }
        );
        log.debug("Get Consumptions call " + response.getStatusCode());
        return response.getBody();
    }

    public Consumption getConsumption(int id) {
        ResponseEntity<Consumption> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/readbyid/" + id,
                HttpMethod.GET,
                null,
                Consumption.class
        );
        log.debug("Get Consumption call " + response.getStatusCode());
        return response.getBody();
    }

    public Consumption createConsumption(Consumption e) {
        ResponseEntity<Consumption> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/create",
                HttpMethod.POST,
                new HttpEntity<>(e),
                Consumption.class
        );
        log.debug("Create Consumption call " + response.getStatusCode());
        return response.getBody();
    }

    public Consumption updateConsumption(Consumption e) {
        ResponseEntity<Consumption> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/update/",
                HttpMethod.PUT,
                new HttpEntity<>(e),
                Consumption.class
        );
        log.debug("Update Consumption call " + response.getStatusCode());
        return response.getBody();
    }

    public void deleteConsumption(int id) {
        ResponseEntity<Void> response = restTemplate.exchange(
                getBackUrl() + props.getApiClientUrl() + "/deletebyid/" + id,
                HttpMethod.DELETE,
                null,
                Void.class);
        log.debug("Delete Consumption call " + response.getStatusCode());
    }
}
