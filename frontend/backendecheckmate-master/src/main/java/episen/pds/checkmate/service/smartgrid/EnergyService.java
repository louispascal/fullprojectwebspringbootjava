package episen.pds.checkmate.service.smartgrid;

import episen.pds.checkmate.model.smartgrid.District;
import episen.pds.checkmate.repository.smartgrid.EnergyProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class EnergyService {

    @Autowired
    private EnergyProxy energyProxy;

    public District getDistrict(Integer id) {
        return energyProxy.getDistrictInfos(id);
    }
}
