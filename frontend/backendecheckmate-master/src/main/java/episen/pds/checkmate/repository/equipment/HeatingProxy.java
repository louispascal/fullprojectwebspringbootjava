package episen.pds.checkmate.repository.equipment;

import episen.pds.checkmate.model.equipment.Heating;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class HeatingProxy {
    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }


    public Heating setHeatingInfos(Heating heating) {
        System.out.println(heating.toString());
        ResponseEntity<Heating> response = restTemplate.exchange(
                getBackUrl() + props.getApiHeatingUrl() + "setHeating/",
                HttpMethod.POST,
                new HttpEntity<>(heating),
                Heating.class
        );
        log.debug("Get Heating call " + response.getStatusCode());
        return response.getBody();
    }

    public Heating getHeating(Integer id) {
        ResponseEntity<Heating> response = restTemplate.exchange(
                getBackUrl() + props.getApiHeatingUrl() + "Heating/" + id.toString(),
                HttpMethod.GET,
                null,
                Heating.class
        );
        log.debug("Get Heating call " + response.getStatusCode());
        return response.getBody();
    }

    public Heating getAutoHeating() {
        ResponseEntity<Heating> response = restTemplate.exchange(
                getBackUrl() + props.getApiHeatingUrl() + "AutoMode/",
                HttpMethod.GET,
                null,
                Heating.class
        );
        log.debug("Get Heating auto Mode call " + response.getStatusCode());
        return response.getBody();
    }
}
