package episen.pds.checkmate.repository;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "episen.pds.checkmate")
@Data
public class UrlProperties {

    private final String apiRseUrl = "/rseuser";
    private final String apiAlerteUrl = "/alerte";
    private final String apiClientUrl = "/prototype/client";
    private final String apiUserUrl = "/user";
    private final String apiMixUrl = "/power";
    private final String apiConsumerUrl = "/cons";
    private final String apiEquipmentUrl = "/equipment";
    private final String apiReservationUrl = "/reservation";
    private final String apiConsumptionDwUrl = "/prototype/ConsumptionDw";
    private final String apiLocalUrl = "/user/";
    private final String apiMapUrl = "/map/";
    private final String apiLightUrl = "/equipment/";
    private final String apiDistrictUrl = "/district";
    private final String apiMonitoringUrl = "/monitoring";
    private final String apiHeatingUrl = "/equipment/";
    private final String apiWashingMachineUrl = "/equipment/";
    private final String apiAnalyseUrl="/analyse";
    private final String apiCitizenUrl="/citizen";
    private final String apiHomeUrl= "/equipment/";

}



