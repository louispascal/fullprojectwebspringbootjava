package episen.pds.checkmate.service.equipment;


import episen.pds.checkmate.model.equipment.Rooms;
import episen.pds.checkmate.repository.equipment.HomeProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Data
@Service
public class HomeService {

    @Autowired
    private HomeProxy homeProxy;


    public List<Rooms> getRoomsByUser(final Integer id){
        System.out.println(1);
        return homeProxy.getRoomsByUser(id);}
}
