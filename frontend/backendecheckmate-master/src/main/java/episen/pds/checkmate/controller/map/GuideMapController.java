package episen.pds.checkmate.controller.map;

import episen.pds.checkmate.service.map.BuildingService;
import episen.pds.checkmate.service.map.MapService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Slf4j
@Controller()
public class GuideMapController {
    @Autowired
    private BuildingService buildingService;
    @Autowired
    private MapService mapService;

    @GetMapping("/guide")
    public String viewGuide(Model model, HttpSession session) {
        Integer company = Integer.parseInt(session.getAttribute("company").toString());
        model.addAttribute("buildings", buildingService.getBuildings(company));
        return "map/guide/guidemap.html";
    }

    @GetMapping("/guidemap")
    public String viewMap(@RequestParam(value = "building") String building, @RequestParam(value = "floor") String floor, Model model, HttpSession session) {
        Integer company = (Integer) session.getAttribute("company");
        model = mapService.buildMap(building, floor, model);
        model.addAttribute("buildings", buildingService.getBuildings(company));
        return "map/guide/map.html";
    }
}