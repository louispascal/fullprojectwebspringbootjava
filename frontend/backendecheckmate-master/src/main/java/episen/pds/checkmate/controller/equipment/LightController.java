package episen.pds.checkmate.controller.equipment;

import episen.pds.checkmate.model.equipment.Light;
import episen.pds.checkmate.service.equipment.LightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class LightController {
    @Autowired
    private LightService lightService;
    @GetMapping("myHome/Light/{id}")
    public String light(@PathVariable Integer id,Model model){
        model.addAttribute("light",lightService.getLight(id));
        return "equipment/Light";
    }
    @PostMapping("/setLight")
    public String updateLight(@ModelAttribute("light") Light light, Model model) {
        System.out.println(light.getValue());
        lightService.setLight(light);
        return "equipment/Light";
    }
}
