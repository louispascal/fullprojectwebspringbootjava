package episen.pds.checkmate.service.equipment;

import episen.pds.checkmate.model.equipment.Heating;
import episen.pds.checkmate.repository.equipment.HeatingProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HeatingService {
    @Autowired
    private HeatingProxy HeatingProxy;

    public Heating setHeating(Heating heating) {
        System.out.println(heating.toString());
        return HeatingProxy.setHeatingInfos(heating);
    }

    public Heating getHeating(Integer id) {
        return HeatingProxy.getHeating(id);
    }

    public Heating autoHeating() {
        return HeatingProxy.getAutoHeating();
    }
}
