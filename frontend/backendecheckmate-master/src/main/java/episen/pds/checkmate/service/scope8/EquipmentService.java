package episen.pds.checkmate.service.scope8;

import episen.pds.checkmate.model.scope8.Equipment;
import episen.pds.checkmate.repository.scope8.EquipmentProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class EquipmentService {
    @Autowired
    private EquipmentProxy equipmentProxy;

    public Iterable<Equipment> getEquipment(final int id) {
        return equipmentProxy.getEquipment(id);
    }

    public Iterable<Equipment> getEquipments() {
        return equipmentProxy.getEquipments();
    }


}
