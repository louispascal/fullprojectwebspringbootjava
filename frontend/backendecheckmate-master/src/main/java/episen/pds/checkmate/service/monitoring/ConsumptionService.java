package episen.pds.checkmate.service.monitoring;

import episen.pds.checkmate.model.monitoring.Consumption;
import episen.pds.checkmate.repository.monitoring.ConsumptionProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class ConsumptionService {

    @Autowired

    private ConsumptionProxy consumptionProxy;

    public Consumption getConsumption(final int id) {
        return consumptionProxy.getConsumption(id);
    }

    public Iterable<Consumption> getConsumptions() {
        return consumptionProxy.getConsumptions();
    }

    public void deleteConsumption(final int id) {
        consumptionProxy.deleteConsumption(id);
    }

   public Consumption saveConsumption(Consumption consumption) {
        if (consumption.getIdConsumption() == null) {
            return consumptionProxy.createConsumption(consumption);
        }
        return consumptionProxy.updateConsumption(consumption);
    }

}
