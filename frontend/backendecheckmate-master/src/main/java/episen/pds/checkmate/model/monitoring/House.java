package episen.pds.checkmate.model.monitoring;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class House {
    private Integer id_house;
    private String owner;
    private String address;
    private String typeofhouse;
    private Double consumption;
    private Boolean bepos;
    private Double energy_production;
    private Integer timeOfConsumption;
    private Integer timeOfProduction;
}