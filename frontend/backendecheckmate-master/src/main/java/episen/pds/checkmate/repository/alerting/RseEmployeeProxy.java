package episen.pds.checkmate.repository.alerting;

import episen.pds.checkmate.model.alerting.RseEmployee;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class RseEmployeeProxy {


    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public RseEmployee login(String email) {
        ResponseEntity<RseEmployee> response = restTemplate.exchange(
                getBackUrl() + props.getApiRseUrl() + "/loginRseEmployee/" + email,
                HttpMethod.GET,
                null,
                RseEmployee.class);
        log.debug("Login RseEmployee call " + response.getStatusCode());
        System.out.println("this is login proxy response" + response.getBody());
        return response.getBody();
    }
}
