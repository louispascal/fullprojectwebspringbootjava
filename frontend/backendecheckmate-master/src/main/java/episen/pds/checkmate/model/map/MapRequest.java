package episen.pds.checkmate.model.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MapRequest {
    private Integer id_building;
    private Integer id_floor;
    private String corridor;
    private Integer nb_open_space;
    private Integer nb_office;
    private Integer nb_kitchen;
    private Integer nb_meeting_room;
    private Integer nb_office_double;
    private Boolean fill;

}
