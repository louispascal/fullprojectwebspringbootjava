package episen.pds.checkmate.controller.scope8;

import episen.pds.checkmate.service.scope8.ConsumptionDwService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class ConsumptionDwController {

    @Autowired
    private ConsumptionDwService service;

    @GetMapping("/consumption/{id}")
    public String cons(@PathVariable("id") final int id, Model model) {
        List<String[]> ok = service.getConsumptionDwById(id);


        model.addAttribute("consumptions", service.getConsumptionDwById(id));
        return "scope8/Consumptions";
    }
}
