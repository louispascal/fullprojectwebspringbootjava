package episen.pds.checkmate.model.scope8;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reservation {
    private Integer id_reservation;
    private Integer id_spacepk;
    private Integer id_employee;
    private String date;
    private String end_date;

}
