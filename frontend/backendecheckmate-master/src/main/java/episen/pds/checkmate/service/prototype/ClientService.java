package episen.pds.checkmate.service.prototype;

import episen.pds.checkmate.model.prototype.Client;
import episen.pds.checkmate.repository.prototype.ClientProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class ClientService {


    @Autowired

    private ClientProxy clientProxy;

    public Client getClient(final int id) {
        return clientProxy.getClient(id);
    }

    public Iterable<Client> getClients() {
        return clientProxy.getClients();
    }

    public void deleteClient(final int id) {
        clientProxy.deleteClient(id);
    }

    public Client saveClient(Client client) {
        // Last name must be in UpperCase
        client.setName(client.getName().toUpperCase());

        if (client.getId() == null) return clientProxy.createClient(client);
        return clientProxy.updateClient(client);
    }

}
