package episen.pds.checkmate.repository.mixenergy;

import episen.pds.checkmate.model.mixenergy.MixEnergy;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Slf4j
@Component
public class MixProxy {
    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public MixProxy(UrlProperties props) {
        this.props = props;
    }

    public MixEnergy getMix() {

        ResponseEntity<MixEnergy> response = restTemplate.exchange(
                getBackUrl() + props.getApiMixUrl() + "/totalenergy/" ,

                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        return response.getBody();

    }


    }

