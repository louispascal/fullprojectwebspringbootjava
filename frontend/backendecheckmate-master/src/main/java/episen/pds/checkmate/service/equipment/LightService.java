package episen.pds.checkmate.service.equipment;

import episen.pds.checkmate.model.equipment.Light;
import episen.pds.checkmate.repository.equipment.LightProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LightService {
    @Autowired
    private LightProxy LightProxy;

    public Light setLight(Light light) {
        return LightProxy.setLightInfos(light);
    }

    public Light getLight(Integer id) {
        return LightProxy.getLight(id);
    }
}
