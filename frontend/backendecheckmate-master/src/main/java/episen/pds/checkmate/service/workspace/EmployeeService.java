package episen.pds.checkmate.service.workspace;

import episen.pds.checkmate.model.workspace.Employee;
import episen.pds.checkmate.model.workspace.Access;
import episen.pds.checkmate.repository.workspace.EmployeeProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class EmployeeService {


    @Autowired

    private EmployeeProxy employeeProxy;

    public Employee getEmployee(final int id) {
        return employeeProxy.getEmployee(id);
    }

    public Iterable<Employee> getEmployees(final int company_id) {
        return employeeProxy.getEmployees(company_id);
    }

    public void deleteEmployee(final int id) {
        employeeProxy.deleteEmployee(id);
    }

    public Employee saveEmployee(Employee employee) {
        // Last name must be in UpperCase
        employee.setName(employee.getName().toUpperCase());

        if (employee.getId_employee() == null) return employeeProxy.createEmployee(employee);
        return employeeProxy.updateEmployee(employee);
    }

    public Employee login(final String email) {
        return employeeProxy.login(email);
    }

    public Access getAccess(final int id_access) {
        return employeeProxy.getAccess(id_access);
    }

}
