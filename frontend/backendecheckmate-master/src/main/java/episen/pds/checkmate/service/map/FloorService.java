package episen.pds.checkmate.service.map;

import episen.pds.checkmate.repository.map.FloorProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class FloorService {
    @Autowired
    private FloorProxy floorProxy;

    public Integer getFloorNumber(Integer id_floor) {
        return floorProxy.getFloor(id_floor).getFloor_number();
    }


}
