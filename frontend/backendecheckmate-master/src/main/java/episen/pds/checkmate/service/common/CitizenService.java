package episen.pds.checkmate.service.common;

import episen.pds.checkmate.model.common.Citizen;
import episen.pds.checkmate.model.common.Role;
import episen.pds.checkmate.repository.common.CitizenProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class CitizenService {
    @Autowired
    private final CitizenProxy citizenProxy;

    public Citizen login(String email) {
        return citizenProxy.login(email);
    }

    public Role getRole(Integer id_role) {
        return citizenProxy.getRole(id_role);
    }
}
