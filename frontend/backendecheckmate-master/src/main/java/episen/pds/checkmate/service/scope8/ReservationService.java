package episen.pds.checkmate.service.scope8;

import episen.pds.checkmate.model.map.Space;
import episen.pds.checkmate.model.scope8.Reservation;
import episen.pds.checkmate.repository.scope8.ReservationProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Data
@Service
public class ReservationService {


    @Autowired

    private ReservationProxy ReservationProxy;

    public Reservation addReservation(Reservation reservation){
        return ReservationProxy.createReservation(reservation);
    }

    public Iterable<Reservation> getReservationById(Integer id){
        return ReservationProxy.getReservationById(id);
    }

    public Iterable<Space> getReservedSpaces(Integer id){
        return ReservationProxy.getReservedSpaces(id);
    }

    public Iterable<Reservation> getMeetingsById(Integer id){
        return ReservationProxy.getMeetingsById(id);
    }

    public Reservation getReservation(final int id) {
        return ReservationProxy.getReservation(id);
    }

    public List<String[]> getReservations() {

       /* String current = ReservationProxy.getReservations().get(0)[3].toString();


        Long currentInt =  Long.parseLong(current);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
        System.out.println(dateFormat.format(currentInt));
        System.out.println(currentInt);

       ReservationProxy.getReservations().set(0, new String[] {dateFormat.format(currentInt)});
        System.out.println(ReservationProxy.getReservations()); */
        return ReservationProxy.getReservations();
    }

    public void deleteReservation(final int id) {
        ReservationProxy.deleteReservation(id);
    }


       /* public  Client saveClient(Client client) {
            // Last name must be in UpperCase
            client.setName(client.getName().toUpperCase());

            if(client.getId() == null) return clientProxy.createClient(client);
            return clientProxy.updateClient(client);
        }
*/

}
