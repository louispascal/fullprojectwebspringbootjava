package episen.pds.checkmate.model.equipment;

import episen.pds.checkmate.model.monitoring.Room;
import java.util.Date;
import lombok.Data;


@Data
public class EquipmentEnergy {
    private Integer idEquipment;
    private String category;
    private String typeEquipment;
    private Double nominalPower;
    private Room room;
    private Double value;    
    private Date samplingDate;
}
