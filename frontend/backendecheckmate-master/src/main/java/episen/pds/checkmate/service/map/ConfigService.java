package episen.pds.checkmate.service.map;

import episen.pds.checkmate.model.map.MapRequest;
import episen.pds.checkmate.repository.map.ConfigProxy;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Data
@Service
public class ConfigService {
    @Autowired
    private ConfigProxy configProxy;

    public void sendConfig(MapRequest map) {
        configProxy.sendConfig(map);
    }


}