package episen.pds.checkmate.controller.common;


import episen.pds.checkmate.model.common.Citizen;
import episen.pds.checkmate.model.workspace.Employee;
import episen.pds.checkmate.service.common.CitizenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller()
public class CommonController implements ErrorController {

    private final CitizenService citizenService;

    @Autowired
    public CommonController(CitizenService citizenService) {
        this.citizenService = citizenService;
    }

    @GetMapping("/")
    public String viewHome(Model model) {
        model.addAttribute("citizen", new Citizen());
        model.addAttribute("employee", new Employee());
        model.addAttribute("error", false);
        model.addAttribute("errorDWP", false);
        return "home/homepage.html";
    }

    @RequestMapping("/error")
    public String handleError() {
        return "home/error.html";
    }

    @PostMapping("/energy/login")
    public String login(@ModelAttribute("citizen") Citizen citizen, Model model, HttpSession session) {
        Citizen loggedAs = citizenService.login(citizen.getMail());
        if (loggedAs == null) {
            model.addAttribute("citizen", new Citizen());
            model.addAttribute("employee", new Employee());
            model.addAttribute("error", true);
            model.addAttribute("errorDWP", false);
            return "home/homepage.html";
        } else {
            session.setAttribute("loggedAs", loggedAs);
            session.setAttribute("energy_roles", citizenService.getRole(loggedAs.getId_role()));
            return "redirect:/homeSmartGrid";
        }
    }

    @GetMapping("/logout")
    public String Logout(Model model, HttpSession session) {
        session.invalidate();
        model.addAttribute("citizen", new Citizen());
        model.addAttribute("employee", new Employee());
        model.addAttribute("error", false);
        model.addAttribute("errorDWP", false);
        return "home/homepage.html";
    }

    @GetMapping("/homeSmartGrid")
    public String viewHomeEnergy() {
        return "home/homeEnergy.html";
    }

    @GetMapping("/homeDWP")
    public String viewHomeDWP() {
        return "home/homeDWP.html";
    }

}