package episen.pds.checkmate.controller.monitoring;

import episen.pds.checkmate.model.monitoring.Home;
import episen.pds.checkmate.model.monitoring.Consumption;
import episen.pds.checkmate.model.equipment.EquipmentEnergy;
import episen.pds.checkmate.model.monitoring.Room;
import episen.pds.checkmate.model.monitoring.WindTurbine;
import episen.pds.checkmate.model.monitoring.SolarPanel;
import episen.pds.checkmate.service.monitoring.MonitoringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

@Controller
public class MonitoringController {

    @Autowired
    private MonitoringService service;

    @GetMapping("/monitoring/home_type/{id}")
    public String homeType(Model model, @PathVariable("id") final int idHome) {

        Home home = service.getHomeById(idHome);
        model.addAttribute("home", home);

        Iterable<Room> rooms = service.getRoomsByIdHome(idHome);
        rooms.forEach(room -> {
            room.setEquipments(service.getEquipmentsByIdRoom(room.getIdRoom()));
        });
        model.addAttribute("rooms", rooms);
        
        Iterable<WindTurbine> windTurbines = service.getWindTurbineByIdBuilding(home.getBuilding().getId_building());
        model.addAttribute("windTurbines", windTurbines);
        
        Iterable<SolarPanel> solarPanels = service.getSolarPanelByIdBuilding(home.getBuilding().getId_building());
        model.addAttribute("solarPanels", solarPanels);

        return "monitoring/home_type.html";
    }

//    @GetMapping("/monitoring/home/{id}")
//    public String home(Model model, @PathVariable("id") final int idHome) {
//        Iterable<Home> homes = service.getHomes();
//        model.addAttribute("homes", homes);
//        Home home = service.getHomeById(idHome);
//        model.addAttribute("home", home);
//        Iterable<Room> rooms = service.getRoomsByIdHome(idHome);
//        model.addAttribute("rooms", rooms);
//        Map<String, BigDecimal> graphData = new TreeMap<>();
//        for (Room roomLocal : rooms) {
//            if (roomLocal.getSommeConsumption() != null) {
//                System.out.println(roomLocal.getName() + "---" + roomLocal.getSommeConsumption());
//                graphData.put(roomLocal.getName(), roomLocal.getSommeConsumption());
//            }
//        }
//        model.addAttribute("chartData", graphData);
//
//        return "monitoring/home.html";
//    }
    @GetMapping("/monitoring")
    public String home(Model model) {
        Iterable<Home> homes = service.getHomes();
        model.addAttribute("homes", homes);
        model.addAttribute("home", new Home());
        model.addAttribute("rooms", new ArrayList<>());
        model.addAttribute("chartData", new TreeMap<>());

        return "monitoring/home.html";
    }

    @GetMapping("/monitoring/home")
    public String monitoring(Model model) {
        Iterable<Home> homes = service.getHomes();
        model.addAttribute("homes", homes);
        model.addAttribute("home", new Home());
        model.addAttribute("rooms", new ArrayList<>());
        model.addAttribute("chartData", new TreeMap<>());

        return "monitoring/home.html";
    }

    @GetMapping("/monitoring/room/{id}")
    public String room(Model model, @PathVariable("id") final int idRoom) {
        Room room = service.getRoomById(idRoom);
        Iterable<Room> rooms = new ArrayList<>();
        if (room != null && room.getHome() != null && room.getHome().getIdHome() != null) {
            rooms = service.getRoomsByIdHome(room.getHome().getIdHome());
        }
        model.addAttribute("rooms", rooms);
        model.addAttribute("room", room);
        model.addAttribute("idHome", room.getHome().getIdHome());
        Iterable<EquipmentEnergy> equipments = service.getEquipmentsByIdRoom(idRoom);
        model.addAttribute("equipments", equipments);

        return "monitoring/room.html";
    }

    @GetMapping("/monitoring/consumption/{id}")
    public String equipement(@PathVariable("id") final int idEquipment, Model model) {
        Iterable<Consumption> consumptions = service.getConsumptionsByIdEquipment(idEquipment);
        EquipmentEnergy equipment = service.getEquipmentById(idEquipment);
        model.addAttribute("idRoom", equipment.getRoom().getIdRoom());
        model.addAttribute("equipment", equipment);
        model.addAttribute("consumptions", consumptions);

        return "monitoring/consumption.html";
    }

    @PostMapping("/monitoring/home_type")
    public String roomByHome(Model model, @ModelAttribute Home home) {
        return "redirect:/monitoring/home_type/" + home.getIdHome();
    }

    @PostMapping("/monitoring/room")
    public String equipementByRoom(Model model, @ModelAttribute Room room) {
        return "redirect:/monitoring/room/" + room.getIdRoom();
    }

    @GetMapping("/monitoring/chart/{id}")
    public String getPieChart(@PathVariable("id") final int idEquipment, Model model) {
        Iterable<Consumption> consumptions = service.getConsumptionsByIdEquipment(idEquipment);
        EquipmentEnergy equipement = service.getEquipmentById(idEquipment);
        Map<String, Double> graphData = new TreeMap<>();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        for (Consumption consumption : consumptions) {
            graphData.put(dateFormat.format(consumption.getSamplingDate()), consumption.getInstantaneousPower());
        }

        model.addAttribute("chartData", graphData);
        model.addAttribute("idRoom", equipement.getRoom().getIdRoom());
        model.addAttribute("equipement", equipement);
        return "monitoring/chart.html";
    }

}
