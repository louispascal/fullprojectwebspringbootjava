package episen.pds.checkmate.model.scope8;

import lombok.Data;


@Data

public class ConsumptionDw {


    private Integer id_consumption;
    private Integer idreservation_consumption;
    private Integer idcons_equipment;
    private String unity;
    private Integer value_consumption;
    private String date_prelevment;


}

