package episen.pds.checkmate.repository.map;

import episen.pds.checkmate.model.map.Space;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Slf4j
@Component
public class SpaceProxy {

    private RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public List<Space> getSpaces(String id_building, Integer id_floor, Integer id_area) {
        ResponseEntity<List<Space>> response = restTemplate.exchange(
                getBackUrl() + props.getApiMapUrl() + "spaces/" + id_building + "&" + id_floor + "&" + id_area,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Space call " + response.getStatusCode());
        log.debug("SPACE" + response.getBody());
        return response.getBody();
    }

    public void deleteSpacesAndCorridors(Integer id_floor) {
        ResponseEntity<Void> response = restTemplate.exchange(
                getBackUrl() + props.getApiMapUrl() + "/deleteSpaces/" + id_floor,
                HttpMethod.DELETE,
                null,
                Void.class);
        log.debug("Delete Spaces and Corridors call " + response.getStatusCode());
    }
}
