package episen.pds.checkmate.repository.common;


import episen.pds.checkmate.model.common.Citizen;
import episen.pds.checkmate.model.common.Role;
import episen.pds.checkmate.repository.UrlProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
public class CitizenProxy {

    private final RestTemplate restTemplate = new RestTemplate();
    @Autowired
    private UrlProperties props;

    @Value("${backend.url}")
    String Url;

    public String getBackUrl() {
        return Url;
    }

    public Citizen login(String email) {
        ResponseEntity<Citizen> response = restTemplate.exchange(
                getBackUrl() + props.getApiCitizenUrl() + "/login/" + email,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Citizen call " + response.getStatusCode());
        return response.getBody();
    }

    public Role getRole(Integer id_role) {
        ResponseEntity<Role> response = restTemplate.exchange(
                getBackUrl() + props.getApiCitizenUrl() + "/role/" + id_role,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        log.debug("Get Role call " + response.getStatusCode());
        return response.getBody();
    }
}