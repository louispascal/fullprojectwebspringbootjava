package episen.pds.checkmate.model.analyse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GraphHistoryItem {
    private LocalDate date;
    private Float value;
    private Boolean autoDisplay = true;


}
