Salut, c'est un projet que j'ai fait pendant ma deuxième année en école d'ingénieur. 
C'est un projet web utilisant Java, Spring Boot, une base de donnée sql, html, css et javascript.
L'objectif de ce projet était de mettre en place pour les services généraux d'une ville du suivi technique sur la consommation d'éléctricité, la production et deux trois autres petites choses interessantes. Dans un second temps ce projet permet aussi à chaque personne habitant dans la ville d'avoir accès à ses biens et de monitorer ses équipements, de faire des réservations dans des espaces de travail.

On va faire un set up rapidement, vous allez avoir besoin de plusieurs chose pour pouvoir mettre en place et utiliser ce projet. 

Pour ce que vous allez faire pour nous vous aurez besoin de :

    - mettre en place une base de donnée postgres SQL avec un modèle relationnel :
    https://www.youtube.com/watch?v=e1MwsT5FJRQ&ab_channel=ProgrammingKnowledge

    - télécharger et set up la version 17 de java :
    https://www.youtube.com/watch?v=cL4GcZ6GJV8&ab_channel=AmitThinks

    - télécharger et set up intellij ultimate edition :
    https://www.youtube.com/watch?v=upQu_brz7OI&ab_channel=Codearchery

    - télécharger et installer git bash :
    https://www.educative.io/answers/how-to-install-git-bash-in-windows

Ce projet va vous servir de template. Il y a dans ce projet presque tout ce dont vous avez besoin pour réussir le votre.

Ce que vous devrez faire est :

    - Créer un autre projet springboot

    - réaliser des CRUD (Create, Read, Update, Delete) pour ajouter des formateurs

    - Le publier sur github ou gitlab 

    - nous ajouter sur le projet  