package episen.pds.checkmate.backendcheckmate.model.map;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "dwp", name = "corridor")
public class Corridor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_corridor;
    private Integer id_floor;
    private String orientation;

    public Corridor(Integer id_floor, String orientation) {
        this.id_floor = id_floor;
        this.orientation = orientation;
    }
}
