package episen.pds.checkmate.backendcheckmate.model.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MapInformation {
    private Integer id_building;
    private Integer id_floor;
    private Integer id_nw;
    private Integer id_ne;
    private Integer id_sw;
    private Integer id_se;



}
