package episen.pds.checkmate.backendcheckmate.api.equipment;

import episen.pds.checkmate.backendcheckmate.model.equipment.Rooms;
import episen.pds.checkmate.backendcheckmate.service.equipment.RoomsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins ="*", allowedHeaders = "*")
@RequestMapping(path = "equipment")
@RestController
public class RoomsController {
    private final RoomsService roomsService;
    @Autowired
    public RoomsController(RoomsService roomsService){
        this.roomsService = roomsService;
    }
    @GetMapping("/myHome/{id}")
    public Iterable<Rooms> getRooms(@PathVariable Integer id) {return roomsService.selectRoomByCitizen(id);
    }
}
