package episen.pds.checkmate.backendcheckmate.dao.analyse;

import episen.pds.checkmate.backendcheckmate.model.analyse.EnergySource;
import episen.pds.checkmate.backendcheckmate.model.analyse.EnergySourceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IEnergySourceRepository extends JpaRepository<EnergySource, Integer> {
    Integer countByEnergySourceTypeName(String name);
}
