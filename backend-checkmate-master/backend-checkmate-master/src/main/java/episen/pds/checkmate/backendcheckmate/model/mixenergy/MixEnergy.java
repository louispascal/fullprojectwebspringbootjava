package episen.pds.checkmate.backendcheckmate.model.mixenergy;

import lombok.Data;

@Data
public class MixEnergy {
    private Integer solar_power;
    private Integer wind_power;
    private Integer hydraulic_power;
    private String sampling_date;

}
