package episen.pds.checkmate.backendcheckmate.dao.smartgrid;

import episen.pds.checkmate.backendcheckmate.model.smartgrid.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface EnergyRepository extends JpaRepository<District, Integer> {

    @Query(value = "SELECT sum(consumption) from energy.house where id_district = :id)", nativeQuery = true)
    Integer getSumConsumptionDistrictById(Integer id);

    @Query(value = "SELECT sum(production) from energy.house where id_district = :id)", nativeQuery = true)
    Integer getSumProductionDistrictById(Integer id);

    //Used
    @Query(nativeQuery = true)
    Optional<District> getDistrictInfosById(Integer id);

    @Query(value = "SELECT * from energy.district", nativeQuery = true)
    List<Integer> findAllDistrict();

    @Transactional
    @Modifying
    @Query(value = "UPDATE energy.affectation set instantaneous_power =?2 WHERE id_district =?1", nativeQuery = true)
    void postEnergyToProduced(Integer id_district,Integer instantaneous_power);

    @Query(value = "SELECT sum(instantaneous_power) from energy.affectation", nativeQuery = true)
    Integer getSumInstantaneous_powerToProduced();

    @Query(value = "SELECT distinct(id_district) from energy.building", nativeQuery = true)
    List<Integer> getIdsDistrict();
}
