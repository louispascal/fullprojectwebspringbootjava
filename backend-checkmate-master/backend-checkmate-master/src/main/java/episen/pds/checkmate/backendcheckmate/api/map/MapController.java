package episen.pds.checkmate.backendcheckmate.api.map;

import episen.pds.checkmate.backendcheckmate.model.map.*;
import episen.pds.checkmate.backendcheckmate.service.map.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "map")
@RestController
public class MapController {
    private final FloorService floorService;
    private final BuildingService buildingService;
    private final SpaceService spaceService;
    private final AreaService areaService;
    private final CorridorService corridorService;
    private final MapConfigurationService mapConfigurationService;
    private final PathFindingService pathFindingService;


    @Autowired
    public MapController(FloorService floorService, BuildingService buildingService, SpaceService spaceService, AreaService areaService, CorridorService corridorService, MapConfigurationService mapConfigurationService, PathFindingService pathFindingService) {
        this.floorService = floorService;
        this.buildingService = buildingService;
        this.spaceService = spaceService;
        this.areaService = areaService;
        this.corridorService = corridorService;
        this.mapConfigurationService = mapConfigurationService;
        this.pathFindingService = pathFindingService;
    }

    //This endpoint returns a list of spaces on the requested building.
    @GetMapping("/floor/{id_building}")
    public List<Floor> getFloorsByBuilding(@PathVariable String id_building) {
        return floorService.getFloorsByBuilding(Integer.parseInt(id_building));
    }

    //This endpoint returns a floor object according to its ID.
    @GetMapping(path = "/readbyid/{id}")
    public Optional<Floor> getFloor(@PathVariable Integer id) {
        return floorService.getFloorById(id);
    }

    //This endpoint returns a list of spaces on the requested area.
    @GetMapping(path = "spaces/{id_building}&{id_floor}&{id_area}")
    public List<Space> getSpacesByArea(@PathVariable String id_building, @PathVariable String id_floor, @PathVariable String id_area) {
        return spaceService.getSpaceByArea(Integer.parseInt(id_building), Integer.parseInt(id_floor), Integer.parseInt(id_area));
    }

    //This endpoint returns a list of areas on the requested floor.
    @GetMapping(path = "areas/{id_building}&{id_floor}&{area}")
    public List<Area> getAreasByFloor(@PathVariable String id_building, @PathVariable String id_floor, @PathVariable String area) {
        return areaService.getAreasFromFloor(Integer.parseInt(id_building), Integer.parseInt(id_floor), area);
    }

    //This endpoint returns a list of the requested company's buildings.
    @GetMapping("/buildings/{id_company}")
    public List<Building> getBuildings(@PathVariable String id_company) {
        return buildingService.getBuildings(Integer.valueOf(id_company));
    }

    //This endpoint returns a list of corridors on the requested floor.
    @GetMapping(path = "corridor/{id_building}&{id_floor}")
    public List<Corridor> getCorridorsByFloor(@PathVariable String id_building, @PathVariable String id_floor) {
        return corridorService.getCorridorsFromFloor(Integer.parseInt(id_building), Integer.parseInt(id_floor));
    }

    //This endpoint is used to delete the content of a floor.
    @DeleteMapping("/deleteSpaces/{id_floor}")
    public void deleteSpacesAndCorridors(@PathVariable Integer id_floor) {
        spaceService.deleteCorridors(id_floor);
        spaceService.deleteSpaces(id_floor);
    }

    //This endpoint is used to configure a floor.
    @PostMapping(path = "config/submit")
    public ResponseEntity<String> submitConfig(@RequestBody MapRequest mapRequest) {
        String answer = mapConfigurationService.launchConfiguration(mapRequest);
        HttpStatus status;
        if ("Size match".equals(answer)) {
            status = HttpStatus.OK;
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return ResponseEntity.status(status).body(answer);
    }

    //This endpoint is used to get the path from one space to another
    @PostMapping(path = "guide/submit/{goId}/{toId}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ArrayList<Coordinate> getPath(MapInformation mapInformation, @PathVariable Integer goId, @PathVariable Integer toId) {
        return pathFindingService.retrievePath(goId, toId, mapInformation);
    }


    @GetMapping("/availableRooms/{id_company}")
    public List<Space> getAvailableRooms(@PathVariable String id_company) {
        return spaceService.getAvailableRooms(Integer.valueOf(id_company));
    }

    @GetMapping("/availableRoomsByBuilding/{id_building}")
    public List<Space> getAvailableRoomsByBuilding(@PathVariable String id_building) {
        return spaceService.getAvailableRoomsByBuilding(Integer.valueOf(id_building));
    }

    @GetMapping("/availableRoomsByFloor/{id_floor}")
    public List<Space> getAvailableRoomsByFloor(@PathVariable String id_floor) {
        return spaceService.getAvailableRoomsByFloor(Integer.valueOf(id_floor));
    }

    @GetMapping("/openspaces/{id_company}")
    public List<Space> getOpenSpaces(@PathVariable String id_company) {
        return spaceService.getOpenSpaces(Integer.valueOf(id_company));
    }

    @GetMapping("/reservedSpaces/{id}")
    public List<Space> getReservedSpaces(@PathVariable Integer id) {
        return spaceService.getReservedSpaces(id);
    }
}
