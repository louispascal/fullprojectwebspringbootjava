package episen.pds.checkmate.backendcheckmate.dao.smartgrid;

import episen.pds.checkmate.backendcheckmate.model.smartgrid.Autumn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

@Repository
public interface AutumnRepository extends JpaRepository<Autumn, Integer> {


    Autumn getAutumnByScheduleContains(String schedule);
}
