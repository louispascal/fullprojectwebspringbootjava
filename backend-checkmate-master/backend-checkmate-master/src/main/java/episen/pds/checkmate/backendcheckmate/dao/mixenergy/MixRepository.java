package episen.pds.checkmate.backendcheckmate.dao.mixenergy;
import episen.pds.checkmate.backendcheckmate.model.mixenergy.Production_of_site;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;



public interface MixRepository extends JpaRepository<Production_of_site, Integer> {
    @Query(value ="select sum (instantaneous_power) * 100 / sum(max_capacity)  from energy.production_of_site p inner join energy.site_of_production s on s.id_site=p.id_site  and s.site_type='solaire' where p.sampling_date in (select sampling_date from energy.production_of_site order by sampling_date desc limit 1)"
            , nativeQuery = true)
    Integer findSolarEnergy();
    @Query(value = "select sum (instantaneous_power) * 100 / sum(max_capacity)  from energy.production_of_site p inner join energy.site_of_production s on s.id_site=p.id_site  and s.site_type='éolienne' where p.sampling_date in (select sampling_date from energy.production_of_site order by sampling_date desc limit 1)"
            , nativeQuery = true)
    Integer findWindEnergy();
    @Query(value ="select sum (instantaneous_power) * 100 / sum(max_capacity)  from energy.production_of_site p inner join energy.site_of_production s on s.id_site=p.id_site  and s.site_type='hydraulique' where p.sampling_date in (select sampling_date from energy.production_of_site order by sampling_date desc limit 1)"
            , nativeQuery = true)
    Integer findHydraulicEnergy();
    @Query(value="select max(sampling_date) from energy.production_of_site"
           ,nativeQuery = true)
    String findTimeProd();
    @Query(value="select sum(current_capacity) from energy.production_of_site p inner join energy.site_of_production s on s.id_site=p.id_site and p.site_status ='true' and s.site_type=? where p.sampling_date in (select sampling_date from energy.production_of_site order by sampling_date desc limit 1)"
            ,nativeQuery = true)
    Integer findCurrentCapacity(String energyType);
    @Query(value="select sum(instantaneous_power) from energy.production_of_site p inner join energy.site_of_production s on s.id_site=p.id_site and p.site_status ='true' and s.site_type=? where p.sampling_date in (select sampling_date from energy.production_of_site order by sampling_date desc limit 1)"
            ,nativeQuery = true)
    Integer findInstantaneousPower(String energyType);




}
