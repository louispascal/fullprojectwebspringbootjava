package episen.pds.checkmate.backendcheckmate.dao.analyse;

import episen.pds.checkmate.backendcheckmate.model.analyse.ConsumptionAnalyze;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IConsumptionAnalyzeRepository extends JpaRepository<ConsumptionAnalyze, Integer> {
    @Query("select sum(ca.value) from ConsumptionAnalyze ca")
    Integer sumOfConsumption();


}
