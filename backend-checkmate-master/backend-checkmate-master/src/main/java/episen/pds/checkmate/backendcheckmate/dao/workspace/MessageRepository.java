package episen.pds.checkmate.backendcheckmate.dao.workspace;

import episen.pds.checkmate.backendcheckmate.model.workspace.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

    @Query(value = "SELECT * FROM dwp.message WHERE (sender = ?1 and receiver = ?2) or (sender = ?2 and receiver = ?1) ORDER BY time", nativeQuery = true)
    List<Message> getMessages(Integer sender, Integer receiver);

    @Modifying
    @Transactional
    @Query(value = "Insert into dwp.message (sender,receiver,content,time) values(?1,?2,?3,?4)", nativeQuery = true)
    void sendMessage(Integer sender, Integer receiver, String content, Timestamp time);
}
