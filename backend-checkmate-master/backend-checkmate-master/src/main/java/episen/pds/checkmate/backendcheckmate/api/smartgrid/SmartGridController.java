package episen.pds.checkmate.backendcheckmate.api.smartgrid;

import episen.pds.checkmate.backendcheckmate.model.smartgrid.District;
import episen.pds.checkmate.backendcheckmate.service.smartgrid.EnergyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@EnableScheduling
public class SmartGridController {

    @Autowired
    private EnergyService energyService;

    @Scheduled(cron = "*/10 * * * * *")
    /*public void auto() {
        System.out.println("ça marche");
        List<Integer> idDistricts = energyService.findAllDistrict();
        List<District> districts = new LinkedList<>();
        for(Integer id: idDistricts) {
            Optional<District> district = energyService.getDistrictInfosById(id);
            if(district.isPresent()) {
                districts.add(energyService.getDistrictInfosById(id).get());
            }
        }

        for(District district: districts)
            System.out.println(district);
    }*/

    @GetMapping(path = "/districts")
    public List<District> getDistrictInfos() {
        List<Integer> idDistricts = energyService.getAllDistricts();
        List<District> districts = new LinkedList<>();
        for(Integer id: idDistricts) {
            Optional<District> district = energyService.getDistrictInfosById(id);
            if(district.isPresent()) {
                districts.add(energyService.getDistrictInfosById(id).get());
            }
        }
        return districts;
    }

}
