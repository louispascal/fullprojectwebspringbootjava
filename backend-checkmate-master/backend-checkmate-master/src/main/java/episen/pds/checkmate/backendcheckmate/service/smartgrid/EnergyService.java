package episen.pds.checkmate.backendcheckmate.service.smartgrid;

import episen.pds.checkmate.backendcheckmate.dao.smartgrid.EnergyRepository;
import episen.pds.checkmate.backendcheckmate.dao.smartgrid.SiteRepository;
import episen.pds.checkmate.backendcheckmate.model.mixenergy.Site_of_Production;
import episen.pds.checkmate.backendcheckmate.model.smartgrid.District;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EnergyService {
    private final EnergyRepository energyRepository;
    private final SiteRepository siteRepository;

    @Autowired
    public EnergyService(EnergyRepository energyRepository, SiteRepository siteRepository) {
        this.energyRepository = energyRepository;
        this.siteRepository = siteRepository;
    }
    public Optional<District> getDistrictInfosById(Integer id){
        return energyRepository.getDistrictInfosById(id);
    }
    public List<Integer> getAllDistricts(){
        return energyRepository.getIdsDistrict();
    }
    public void postEnergyToProduced(Integer id, Integer value){
        energyRepository.postEnergyToProduced(id, value);
    }
    public Integer getSumInstantaneous_powerToProduced(){
        return energyRepository.getSumInstantaneous_powerToProduced();
    }
    public List<Site_of_Production> getSite_of_Production(){
        return siteRepository.findAll();
    }
}