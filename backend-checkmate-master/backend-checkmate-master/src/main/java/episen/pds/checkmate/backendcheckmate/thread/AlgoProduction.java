package episen.pds.checkmate.backendcheckmate.thread;
import episen.pds.checkmate.backendcheckmate.dao.mixenergy.ChoiceRepository;
import episen.pds.checkmate.backendcheckmate.dao.smartgrid.*;
import episen.pds.checkmate.backendcheckmate.model.mixenergy.Choice;
import episen.pds.checkmate.backendcheckmate.service.smartgrid.EnergyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
@Slf4j
@Component
public class AlgoProduction implements Runnable{

    private final EnergyService energyService;
    @Autowired
    private Current_Season_Repository current_season_repository;
    @Autowired
    private WinterRepository winterRepository;
    @Autowired
    private SummerRepository summerRepository;
    @Autowired
    private AutumnRepository autumnRepository;
    @Autowired
    private SpringRepository springRepository;
    @Autowired
    public AlgoProduction( EnergyService energyService) {this.energyService = energyService;}
    @Autowired
    public ChoiceRepository choiceRepository;
    @Autowired
    public Production_Of_Site_Repository production_of_site_repository;


    @Override
    public void run() {
        AlgoCapacity algoCapacity = new AlgoCapacity(energyService, current_season_repository, winterRepository, summerRepository, autumnRepository, springRepository);
        AlgoQuarter algoQuarter = new AlgoQuarter(energyService);

        new Thread(algoCapacity).start();
        new Thread(algoQuarter).start();

        while(true) {
            Choice choice = choiceRepository.findAll().get(0);
            try {
                System.out.println("Balance ville avant production : "+ algoQuarter.getDifference());
                int priority = 1;
                for(SiteCapacity capacity : algoCapacity.getSiteCapacities()) {

                    int choiceHydraulic = choice.getHydraulic();
                    int choiceSolar = choice.getSolar();
                    int choiceWind = choice.getWind();

                    if(choiceHydraulic == priority){
                        if (capacity.getSite().getSite_type() == "hydraulique"){
                            int difference = algoQuarter.getDifference()+capacity.getCurrent_capacity();
                           // production_of_site_repository.save(new Production_of_site(true));

                        }
                    }
                    if(choiceSolar == priority){

                    }
                    if(choiceWind == priority){

                    }
                    if (algoQuarter.getDifference() < 0) {
                        System.out.println("Site :" + capacity.getSite().getSite_type()+" has produced : " + capacity.getCurrent_capacity());
                        int newBalance = Math.abs(((algoQuarter.getDifference()) - capacity.getCurrent_capacity())*-1);
                        algoQuarter.setDifference(newBalance);
                        System.out.println("Ville balance : "+ newBalance);
                    } else break;
                }
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}