package episen.pds.checkmate.backendcheckmate.thread;

import episen.pds.checkmate.backendcheckmate.model.monitoring.ProductionSolarPanel;
import episen.pds.checkmate.backendcheckmate.model.monitoring.SolarPanel;
import episen.pds.checkmate.backendcheckmate.service.monitoring.ProductionSolarPanelService;
import episen.pds.checkmate.backendcheckmate.service.monitoring.SolarPanelService;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Slf4j
@Component
public class AlgoSolarPanel implements Runnable {

    private final SolarPanelService solarPanelService;
    private final ProductionSolarPanelService productionSolarPanelService;

    @Autowired
    public AlgoSolarPanel(SolarPanelService solarPanelEnergyService, ProductionSolarPanelService productionSolarPanelService) {
        this.solarPanelService = solarPanelEnergyService;
        this.productionSolarPanelService = productionSolarPanelService;
    }

    public static double nextDoubleBetween(double min, double max) {
        // java 8 + DoubleStream
        return new Random().doubles(min, max).limit(1).findFirst().getAsDouble();
    }

    @Override
    public void run() {
        List<SolarPanel> solarPanelList = solarPanelService.selectAllSolarPanel();
        System.out.println("+++++++++++Add Production Solar Panel++++++++");
        while (true) {
            for (SolarPanel solarPanel : solarPanelList) {
                ProductionSolarPanel productionSolarPanel = new ProductionSolarPanel();
                productionSolarPanel.setSolarPanel(solarPanel);
                productionSolarPanel.setInstantaneousPower(nextDoubleBetween(100, 120));
                productionSolarPanel.setSamplingDate(new Date());
                System.out.println("solarPanel--" + solarPanel.getIdSolarPanel()+ " Production " + productionSolarPanel.getInstantaneousPower());
                productionSolarPanelService.addProductionSolarPanel(productionSolarPanel);
            }
            try {
                Thread.sleep(60 * 1000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }

        }
    }
}
