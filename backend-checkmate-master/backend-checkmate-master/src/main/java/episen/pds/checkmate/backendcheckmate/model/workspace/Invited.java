package episen.pds.checkmate.backendcheckmate.model.workspace;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(schema = "dwp")
public class Invited {

    private Integer id_reservation;
    private Integer id_employee;
    private Integer status;


}
