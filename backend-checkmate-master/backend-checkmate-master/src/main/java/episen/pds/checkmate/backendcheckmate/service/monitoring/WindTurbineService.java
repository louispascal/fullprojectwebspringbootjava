package episen.pds.checkmate.backendcheckmate.service.monitoring;

import episen.pds.checkmate.backendcheckmate.dao.monitoring.WindTurbineRepository;
import episen.pds.checkmate.backendcheckmate.model.monitoring.WindTurbine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WindTurbineService {
    private final WindTurbineRepository windTurbineRepository;

    @Autowired
    public WindTurbineService(WindTurbineRepository windTurbineRepository) {
        this.windTurbineRepository = windTurbineRepository;
    }


    public void addWindTurbine(WindTurbine windTurbine) {
        windTurbineRepository.save(windTurbine);
    }

    public List<WindTurbine> selectAllWindTurbine() {
        return windTurbineRepository.findAll();
    }

    public List<WindTurbine> findByIdBuilding(Integer id) {
        List<WindTurbine> windTurbines = windTurbineRepository.findByIdBuilding(id);
        if(!windTurbines.isEmpty()){
            windTurbines.forEach(windTurbine -> {
                windTurbine.setProduction(windTurbineRepository.findValueMaxDateByIdWindTurbine(windTurbine.getIdWindTurbine()));
            });
        }
        return windTurbines;
    }

    public Optional<WindTurbine> getWindTurbineById(Integer id) {
        return windTurbineRepository.findById(id);
    }

    public void updateWindTurbine(WindTurbine windTurbine) {
        windTurbineRepository.save(windTurbine);
    }

    public void deleteWindTurbine(Integer id) {
        windTurbineRepository.deleteById(id);
    }


}
