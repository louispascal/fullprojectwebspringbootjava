package episen.pds.checkmate.backendcheckmate.api.analyse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.sql.Date;
import java.text.SimpleDateFormat;

@Data
@NoArgsConstructor

public class CompGraph {
    private String title;
    private Float value1;
    private Float value2;
    private Float ratio;

    public CompGraph(String title, Long value1, Long value2) {
        this.title = title;

        this.value1 = Float.valueOf(value1);
        if (value2 != null) this.value2 = Float.valueOf(value2);
    }

    public CompGraph(Date title, Long value1, Long value2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        this.title = sdf.format(title);
        this.value1 = Float.valueOf(value1);
        this.value2 = Float.valueOf(value2);
    }


    public Float getRatio() {
        return (value1 / value2) * 100;
    }
}
