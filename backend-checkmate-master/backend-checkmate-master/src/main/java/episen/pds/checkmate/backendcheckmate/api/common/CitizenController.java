package episen.pds.checkmate.backendcheckmate.api.common;

import episen.pds.checkmate.backendcheckmate.model.common.Citizen;
import episen.pds.checkmate.backendcheckmate.model.common.Role;
import episen.pds.checkmate.backendcheckmate.service.common.CitizenService;
import episen.pds.checkmate.backendcheckmate.service.common.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "citizen")
@RestController
public class CitizenController {

    private final CitizenService citizenService;
    private final RoleService roleService;

    @Autowired
    public CitizenController(CitizenService citizenService, RoleService roleService) {

        this.citizenService = citizenService;
        this.roleService = roleService;
    }

    @GetMapping("/login/{mail}")
    public Citizen loginByMail(@PathVariable String mail) {
        return citizenService.findByMail(mail);
    }

    @GetMapping("/role/{id_role}")
    public Optional<Role> getRole(@PathVariable Integer id_role) {
        return roleService.getRoleById(id_role);
    }
}
