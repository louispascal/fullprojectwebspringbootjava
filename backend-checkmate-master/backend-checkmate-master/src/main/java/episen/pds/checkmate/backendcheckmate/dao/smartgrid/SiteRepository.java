package episen.pds.checkmate.backendcheckmate.dao.smartgrid;

import episen.pds.checkmate.backendcheckmate.model.mixenergy.Site_of_Production;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SiteRepository extends JpaRepository<Site_of_Production, Integer> {

}
