package episen.pds.checkmate.backendcheckmate.api.smartgrid;

import episen.pds.checkmate.backendcheckmate.thread.AlgoProduction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class AlgoQuarterExec {

    @Autowired
    private TaskExecutor taskExecutor;
    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    public void atStartup() {

        AlgoProduction production = applicationContext.getBean(AlgoProduction.class);
        taskExecutor.execute(production);
    }

}
