package episen.pds.checkmate.backendcheckmate.service.monitoring;

import episen.pds.checkmate.backendcheckmate.dao.monitoring.HomeRepository;
import episen.pds.checkmate.backendcheckmate.model.monitoring.Home;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HomeService {
    private final HomeRepository homeRepository;

    @Autowired
    public HomeService(HomeRepository homeRepository) {
        this.homeRepository = homeRepository;
    }


    public void addHome(Home home) {
        homeRepository.save(home);
    }

    public List<Home> selectAllHome() {
        return homeRepository.findAll();
    }

    public Optional<Home> getHomeById(Integer id) {
        return homeRepository.findById(id);
    }

    public void updateHome(Home home) {
        homeRepository.save(home);
    }

    public void deleteHome(Integer id) {
        homeRepository.deleteById(id);
    }

}
