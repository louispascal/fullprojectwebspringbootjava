package episen.pds.checkmate.backendcheckmate.model.smartgrid;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(schema = "energy")
public class BuildingForEnergy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_building;
    private Integer id_wind_turbine;
    private String address;
    private Integer id_district;
    @Column(name = "type_building")
    private Integer typeBuilding;

    @Override
    public String toString() {
        return "BuildingForEnergy{"
                + "id_building=" + id_building
                + ", id_wind_turbine=" + id_wind_turbine
                + ", address='" + address + '\''
                + ", id_district=" + id_district
                + ", type_building=" + typeBuilding
                + '}';
    }
}
