package episen.pds.checkmate.backendcheckmate.api.equipment;

import episen.pds.checkmate.backendcheckmate.model.equipment.Light;
import episen.pds.checkmate.backendcheckmate.service.equipment.LightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "equipment")
@RestController
public class LightController {
    private final LightService lightService;
    @Autowired
    public LightController(LightService lightService) {
        this.lightService = lightService;

    }
    @PostMapping("/setLight")
    public void updateLight(@RequestBody Light light){
        System.out.println(light);
        System.out.println(light.getValue());
        System.out.println(light.getId_equipment());
        lightService.setLight(light);
    }

    @GetMapping("/Light/{id}")
    public Optional<Light> getLight(@PathVariable Integer id ) {
        return lightService.selectLight(id);
    }
}
