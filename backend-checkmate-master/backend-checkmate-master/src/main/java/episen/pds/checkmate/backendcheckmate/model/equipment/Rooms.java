package episen.pds.checkmate.backendcheckmate.model.equipment;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Rooms {

    private Integer id_citizen;
    private Integer id_room;
    private String type;
    private String type_equipment;
    @Id
    private Integer id_equipment;
}
