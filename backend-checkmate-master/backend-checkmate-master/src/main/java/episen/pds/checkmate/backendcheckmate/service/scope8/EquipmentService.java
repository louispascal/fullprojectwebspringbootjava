package episen.pds.checkmate.backendcheckmate.service.scope8;


import episen.pds.checkmate.backendcheckmate.dao.scope8.EquipmentRepository;
import episen.pds.checkmate.backendcheckmate.model.scope8.Equipment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class EquipmentService {
    private final EquipmentRepository EquipmentRepository;

    @Autowired
    public EquipmentService(EquipmentRepository EquipmentRepository) {
        this.EquipmentRepository = EquipmentRepository;
    }


    public void addEquipment(Equipment Equipment) {
        EquipmentRepository.save(Equipment);
    }

    public List<Equipment> selectAllEquipment() {
        return EquipmentRepository.findAll();
    }

    public Optional<Equipment> getEquipmentById(Integer id) {
        return EquipmentRepository.findById(id);
    }

    public void updateEquipment(Equipment Equipment) {
        EquipmentRepository.save(Equipment);
    }

    public void deleteEquipment(Integer id) {
        EquipmentRepository.deleteById(id);
    }
    public  List<Object[]> getEquipmentByConsumerId(Integer id) {return  EquipmentRepository.equipmentsUsedby(id);}

}

/*
public class EquipmentService {
    private final EquipmentRepository equipmentRepository;

    @Autowired
    public EquipmentService(EquipmentRepository equipmentRepository) {
        this.equipmentRepository = equipmentRepository;

    }
    public Optional<Equipment> getEquipmentById(Integer id) {
        return equipmentRepository.findById(id);
    }


    public List<Equipment> getEquipments() {
        return equipmentRepository.findAll();}


    public List<Equipment> getKWbyConID(Integer id) {
        return equipmentRepository.consKW(id);}

    public List<Timestamp> getFinishTime(Integer consumerId){
        Timestamp lt = new Timestamp(System.currentTimeMillis());
      return  equipmentRepository.finishTime(consumerId, lt);
    }

   public void insertRand(Integer consumerId){
       Timestamp currentTime = new Timestamp(System.currentTimeMillis());
       Timestamp finishTime = equipmentRepository.finishTime(consumerId,currentTime).get(0);
        equipmentRepository.voidInsertData(consumerId,currentTime,finishTime);


   }
}

*/