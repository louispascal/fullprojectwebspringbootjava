/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package episen.pds.checkmate.backendcheckmate.model.monitoring;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import episen.pds.checkmate.backendcheckmate.model.map.Building;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Ines
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "wind_turbine", schema = "energy")
public class WindTurbine implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_wind_turbine")
    private Integer idWindTurbine;

    @JoinColumn(name = "id_building", referencedColumnName = "id_building")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Building building;
    
    @Transient
    private Double production;

}
