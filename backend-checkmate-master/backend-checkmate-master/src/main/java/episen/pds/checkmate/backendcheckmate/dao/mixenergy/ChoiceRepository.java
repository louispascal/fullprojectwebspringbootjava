package episen.pds.checkmate.backendcheckmate.dao.mixenergy;
import episen.pds.checkmate.backendcheckmate.model.mixenergy.Choice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import javax.transaction.Transactional;

public interface ChoiceRepository extends JpaRepository<Choice,Integer> {
    @Transactional
    @Modifying
    @Query(value ="update energy.choice set solar =? , hydraulic =?, wind =? where id_choice =1 ", nativeQuery = true)
    void postEnergyPreference(Integer solar ,Integer hydraulic, Integer wind );

    @Transactional
    @Modifying
    @Query(value ="update energy.choice set proportion_solar =? , proportion_hydraulic =?, proportion_wind =? where id_choice =1 ", nativeQuery = true)
    void postEnergyProportion(Integer solarProportion ,Integer hydraulicProportion  , Integer windProportion);


}