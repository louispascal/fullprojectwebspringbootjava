package episen.pds.checkmate.backendcheckmate.dao.alerting;

import episen.pds.checkmate.backendcheckmate.model.alerting.RseEmployee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface RseEmployeeRepository extends JpaRepository<RseEmployee, Integer> {

    @Query(value = "SELECT * FROM rseemployee WHERE email = ?1", nativeQuery = true)
    Optional<RseEmployee> login(String email);
}
