package episen.pds.checkmate.backendcheckmate.dao.smartgrid;

import episen.pds.checkmate.backendcheckmate.model.smartgrid.Current_season;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Current_Season_Repository extends JpaRepository <Current_season, Integer>{

}
