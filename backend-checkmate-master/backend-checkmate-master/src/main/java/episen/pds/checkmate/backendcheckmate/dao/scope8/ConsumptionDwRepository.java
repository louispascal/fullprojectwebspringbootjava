package episen.pds.checkmate.backendcheckmate.dao.scope8;

import episen.pds.checkmate.backendcheckmate.model.scope8.Consumptiondw;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;


@Repository
public interface ConsumptionDwRepository extends JpaRepository<Consumptiondw, Integer> {

    @Modifying
    @Query(value = "insert into dwp.consumptiondw (idreservation_consumption,idcons_equipment, value_consumption,unity, date_prelevment)values (:idreservation_consumption,:idcons_equipment, :value_consumption ,:unity,:date_prelevment)", nativeQuery = true)
    @Transactional
    void voidInsertDataCons(@Param("idreservation_consumption") Integer idreservation_consumption, @Param("idcons_equipment") Integer idcons_equipment, @Param("value_consumption") Integer value_consumption, @Param("unity") String unity, @Param("date_prelevment") LocalDateTime date_prelevment);

    @Query(value = " select * from dwp.consumptiondw inner join dwp.equipment on consumptiondw.idcons_equipment=equipment.id_equipment where idreservation_consumption=?1", nativeQuery = true)
    List<String[]> getConsumptionDwyId(Integer id);

    @Query(value = " select MAX(value_consumption) from dwp.consumptiondw where idreservation_consumption=?1", nativeQuery = true)
    Integer getConsumptionsMax(Integer id);
}