package episen.pds.checkmate.backendcheckmate.dao.monitoring;

import episen.pds.checkmate.backendcheckmate.model.monitoring.EquipmentEnergy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author Ines
 */
@Repository
public interface EquipmentEnergyRepository extends JpaRepository<EquipmentEnergy, Integer> {
    @Query(value = "SELECT * from energy.equipment e INNER JOIN energy.room r ON r.id_room = e.id_room INNER JOIN energy.home h on r.id_home = h.id_home WHERE h.id_home = ?1 ORDER BY r.name ASC", nativeQuery = true)
    List<EquipmentEnergy> findByIdHome(int idHome);
    
    @Query(value = "SELECT instantaneous_power FROM energy.consumption WHERE id_equipment = ?1 AND sampling_date = (SELECT Max(sampling_date) FROM energy.consumption WHERE id_equipment =  ?1)", nativeQuery = true)
    Double findValueMaxDateByIdEquipment(int idEquipment);
           
    @Query(value = "SELECT * from energy.equipment e INNER JOIN room r ON r.id_room = e.id_room WHERE r.id_room = ?1 ORDER BY r.name ASC", nativeQuery = true)
    List<EquipmentEnergy> findByIdRoom(int idRoom);
}
