package episen.pds.checkmate.backendcheckmate.api.monitoring;

import episen.pds.checkmate.backendcheckmate.model.equipment.Equipments;
import episen.pds.checkmate.backendcheckmate.model.monitoring.Room;
import episen.pds.checkmate.backendcheckmate.service.monitoring.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins ="*", allowedHeaders = "*")
@RequestMapping(path = "equipment")
@RestController
public class RoomController {
    private final RoomService roomService;
    @Autowired
    public RoomController(RoomService roomService){
        this.roomService = roomService;
    }

}
