package episen.pds.checkmate.backendcheckmate.dao.prototype;

import episen.pds.checkmate.backendcheckmate.model.prototype.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {


}
