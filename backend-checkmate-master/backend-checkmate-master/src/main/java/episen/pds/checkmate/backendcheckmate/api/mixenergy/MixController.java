package episen.pds.checkmate.backendcheckmate.api.mixenergy;
import episen.pds.checkmate.backendcheckmate.model.mixenergy.MixEnergy;
import episen.pds.checkmate.backendcheckmate.service.mixenergy.MixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "power")
@RestController
public class MixController {

    private MixService mixService;
    @Autowired
    public MixController(MixService mixService) {
        this.mixService = mixService;
    }
    @GetMapping("/totalenergy")
    public MixEnergy total() {
        return mixService.getTotalEnergy();
    }

    @PostMapping("/multipleLinechart/{logic}")
    public HashMap<String, List<Float>> getGraph(@PathVariable String logic) {
            if(logic.equals("economic")) {
                return mixService.getEconomicData( );
            }else {
                return mixService.getEnvironmentalData();
            }
    }
    @PostMapping("/economicRecommendations/{value}")
    public HashMap<String, List<String>> getEconomicRecommendations(@PathVariable int value) {
        return mixService.getEconomicRecommendations(value);
    }
    @PostMapping("/environmentalRecommendations/{value}")
    public HashMap<String, List<String>> getEnvironmentalRecommendations(@PathVariable int value) {
        return mixService.getEnvironmentalRecommendations(value);
    }
    @GetMapping("/GraphBarData")
    public HashMap<String, List<Integer>> getBarGraph(){
        return mixService.getAverageProductionData();
    }

}







