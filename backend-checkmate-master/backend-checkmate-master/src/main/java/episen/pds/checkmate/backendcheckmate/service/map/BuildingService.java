package episen.pds.checkmate.backendcheckmate.service.map;

import episen.pds.checkmate.backendcheckmate.dao.map.BuildingRepository;
import episen.pds.checkmate.backendcheckmate.model.map.Building;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BuildingService {
    private final BuildingRepository buildingRepository;

    @Autowired
    public BuildingService(BuildingRepository buildingRepository) {
        this.buildingRepository = buildingRepository;
    }

    public List<Building> getBuildings(Integer id) {
        return buildingRepository.findByCompany(id);
    }
}
