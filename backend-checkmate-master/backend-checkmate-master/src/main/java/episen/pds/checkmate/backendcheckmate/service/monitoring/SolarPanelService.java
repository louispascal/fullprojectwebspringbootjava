package episen.pds.checkmate.backendcheckmate.service.monitoring;

import episen.pds.checkmate.backendcheckmate.dao.monitoring.SolarPanelRepository;
import episen.pds.checkmate.backendcheckmate.model.monitoring.SolarPanel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SolarPanelService {
    private final SolarPanelRepository solarPanelRepository;

    @Autowired
    public SolarPanelService(SolarPanelRepository solarPanelRepository) {
        this.solarPanelRepository = solarPanelRepository;
    }


    public void addSolarPanel(SolarPanel solarPanel) {
        solarPanelRepository.save(solarPanel);
    }

    public List<SolarPanel> selectAllSolarPanel() {
        return solarPanelRepository.findAll();
    }
    
    public List<SolarPanel> findByIdBuilding(Integer id) {
        List<SolarPanel> solarPanels = solarPanelRepository.findByIdBuilding(id);
        if(!solarPanels.isEmpty()){
            for (SolarPanel solarPanel : solarPanels) {
                solarPanel.setProduction(solarPanelRepository.findValueMaxDateByIdSolarPanel(solarPanel.getIdSolarPanel()));
            }
        }
        return solarPanels;
    }

    public Optional<SolarPanel> getSolarPanelById(Integer id) {
        return solarPanelRepository.findById(id);
    }

    public void updateSolarPanel(SolarPanel solarPanel) {
        solarPanelRepository.save(solarPanel);
    }

    public void deleteSolarPanel(Integer id) {
        solarPanelRepository.deleteById(id);
    }


}
