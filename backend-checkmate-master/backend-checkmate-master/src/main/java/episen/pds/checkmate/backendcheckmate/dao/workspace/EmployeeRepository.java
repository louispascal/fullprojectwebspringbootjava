package episen.pds.checkmate.backendcheckmate.dao.workspace;

import episen.pds.checkmate.backendcheckmate.model.workspace.Access;
import episen.pds.checkmate.backendcheckmate.model.workspace.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    @Query(value = "SELECT * FROM dwp.employee WHERE email = ?1", nativeQuery = true)
    Optional<Employee> login(String email);

    @Query(value = "SELECT * FROM dwp.employee WHERE id_company = ?1", nativeQuery = true)
    List<Employee> getEmployees(int company);

    @Query(value="Select * from dwp.employee where id_employee in (select id_employee from dwp.reservation where id_reservation=?1) or id_employee in (select id_employee from dwp.invited where id_reservation=?1)", nativeQuery = true)
    List<Employee> getMeetingParticipants(Integer id_reservation);

}
