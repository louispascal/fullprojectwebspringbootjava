package episen.pds.checkmate.backendcheckmate.thread;

import episen.pds.checkmate.backendcheckmate.model.monitoring.Consumption;
import episen.pds.checkmate.backendcheckmate.model.monitoring.EquipmentEnergy;
import episen.pds.checkmate.backendcheckmate.service.monitoring.ConsumptionService;
import episen.pds.checkmate.backendcheckmate.service.monitoring.EquipmentEnergyService;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Slf4j
@Component
public class AlgoEquipmentEnergy implements Runnable {

    private final EquipmentEnergyService equipmentService;
    private final ConsumptionService consumptionService;

    @Autowired
    public AlgoEquipmentEnergy(EquipmentEnergyService equipmentEnergyService, ConsumptionService consumptionService) {
        this.equipmentService = equipmentEnergyService;
        this.consumptionService = consumptionService;
    }
    
    public static double nextDoubleBetween(double min, double max) {
    // java 8 + DoubleStream
    return new Random().doubles(min, max).limit(1).findFirst().getAsDouble();
}

    @Override
    public void run() {
        List<EquipmentEnergy> equipmentList = equipmentService.selectAllEquipment();

        while (true) {
            for (EquipmentEnergy equipment : equipmentList) {
                Consumption consumption = new Consumption();
                consumption.setEquipment(equipment);
                consumption.setInstantaneousPower(nextDoubleBetween(100, 120));
                consumption.setSamplingDate(new Date());
                consumptionService.addConsumption(consumption);
            }
            try {
                Thread.sleep(60 * 60 * 1000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }

        }
    }
}
