package episen.pds.checkmate.backendcheckmate.model.alerting;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class RseEmployee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String firstname;
    private String email;
    private int age;


    public RseEmployee(Integer id, String name, String firstname, String email, int age) {
        this.id = id;
        this.name = name;
        this.firstname = firstname;
        this.email=email;
        this.age = age;
    }

    public RseEmployee(String name, String firstname, String email, int age) {
        this.name = name;
        this.firstname = firstname;
        this.email=email;
        this.age = age;
    }

    public RseEmployee() {

    }



    @Override
    public String toString() {
        return "RseEmployee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstName='" + firstname + '\'' +
                ", age=" + age +
                '}';
    }
}
