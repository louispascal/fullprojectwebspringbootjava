/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package episen.pds.checkmate.backendcheckmate.model.monitoring;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Ines
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "equipment", schema = "energy")
public class EquipmentEnergy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_equipment")
    private Integer idEquipment;
    
    private String category;
    
    @Column(name = "type_equipment")
    private String typeEquipment;
    
    @Column(name = "nominal_power")
    private Double nominalPower;
    
    @JoinColumn(name = "id_room", referencedColumnName = "id_room")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Room room;
   
    @Transient
    private Double value;
   
    @Transient
    private Date samplingDate;
//    
//     private String code;
//    private String libelle;
    //@OneToMany(mappedBy = "equipementRoom", fetch = FetchType.LAZY)
    //@JsonIgnore
    //private List<Consumption> consumptionList = new ArrayList<>();
    
    @Override
    public String toString() {
        return "episen.pds.checkmate.backendcheckmate.model.EquipementRoom[ id_equipment=" + idEquipment + " ]";
    }

}
