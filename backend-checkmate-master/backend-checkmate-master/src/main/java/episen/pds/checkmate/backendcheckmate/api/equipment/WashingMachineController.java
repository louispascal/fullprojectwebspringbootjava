package episen.pds.checkmate.backendcheckmate.api.equipment;

import episen.pds.checkmate.backendcheckmate.model.equipment.WashingMachine;
import episen.pds.checkmate.backendcheckmate.model.equipment.WashingMachine;
import episen.pds.checkmate.backendcheckmate.service.equipment.WashingMachineService;
import episen.pds.checkmate.backendcheckmate.service.equipment.WashingMachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "equipment")
@RestController
public class WashingMachineController {
    private final WashingMachineService washingmachineService;
    @Autowired
    public WashingMachineController(WashingMachineService washingmachineService) {
        this.washingmachineService = washingmachineService;

    }

    @PostMapping("/setWashingMachine/{id}")
    public void updateWashingMachine(@RequestParam("mode") String washingmachine, @PathVariable Integer id) {
        System.out.println(washingmachine);
        washingmachineService.setWashingMachine(washingmachine, id);

    }

    @GetMapping("/WashingMachine/{id}")
    public Optional<WashingMachine> getWashingMachine(@PathVariable Integer id) {
        return washingmachineService.selectWashingMachine(id);
    }
}