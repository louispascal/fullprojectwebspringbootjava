package episen.pds.checkmate.backendcheckmate.dao.workspace;

import episen.pds.checkmate.backendcheckmate.model.workspace.Access;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccessRepository extends JpaRepository<Access, Integer> {
    @Query(value = "SELECT * FROM dwp.access WHERE id_access = ?1", nativeQuery = true)
    Optional<Access> getAccess(int access);
}