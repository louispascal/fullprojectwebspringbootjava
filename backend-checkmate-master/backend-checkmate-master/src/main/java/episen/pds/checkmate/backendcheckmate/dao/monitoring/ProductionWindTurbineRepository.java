package episen.pds.checkmate.backendcheckmate.dao.monitoring;

import episen.pds.checkmate.backendcheckmate.model.monitoring.Consumption;
import episen.pds.checkmate.backendcheckmate.model.monitoring.ProductionWindTurbine;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ines
 */
@Repository
public interface ProductionWindTurbineRepository extends JpaRepository<ProductionWindTurbine, Integer> {

    @Query(value = "SELECT * from energy.production_wind_turbine c WHERE c.id_wind_turbine = ?1 ORDER BY c.sampling_date DESC", nativeQuery = true)
    List<ProductionWindTurbine> findByIdWindTurbine(int idWindTurbine);
}
