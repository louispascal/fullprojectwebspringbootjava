package episen.pds.checkmate.backendcheckmate.dao.map;

import episen.pds.checkmate.backendcheckmate.model.map.Floor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FloorRepository extends JpaRepository<Floor, Integer> {
    @Query(nativeQuery = true)
    List<Floor> getFloorByBuilding(Integer id_building);
}
