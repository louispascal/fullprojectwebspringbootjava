package episen.pds.checkmate.backendcheckmate.model.analyse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(schema = "analyze_schema")
public class ProductionAnalyze {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer value;

    private LocalDate createdOn;

    @ManyToOne(fetch = FetchType.EAGER)
    private EnergySource energySource=new EnergySource();
}
