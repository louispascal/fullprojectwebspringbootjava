package episen.pds.checkmate.backendcheckmate.model.equipment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "heating", schema = "energy")
public class Heating implements Serializable {

    @Id
    @Column(name = "id_heating")
    private Integer idHeating;
    private Integer value;
    private String mode;
    private Integer id_equipment;

    @Override
    public String toString() {
        return "Heating{"
                + "id_heating=" + idHeating
                + ", value='" + value + '\''
                + ", mode='" + mode + '\''
                + ", id_equipment='" + id_equipment + '\''
                + '}';
    }
}
