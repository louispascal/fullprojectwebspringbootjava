package episen.pds.checkmate.backendcheckmate.api.scope8;

import episen.pds.checkmate.backendcheckmate.model.map.Space;
import episen.pds.checkmate.backendcheckmate.model.scope8.Reservation;
import episen.pds.checkmate.backendcheckmate.model.workspace.Employee;
import episen.pds.checkmate.backendcheckmate.service.scope8.ConsumptionDwService;
import episen.pds.checkmate.backendcheckmate.service.scope8.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
This class represent the Reservation controller of the backend
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "reservation")
@RestController
public class ReservationController {
    private final ReservationService ReservationService;
    private final ConsumptionDwService consumptionDwService;

    @Autowired
    public ReservationController(ReservationService ReservationService, ConsumptionDwService consumptionDwService) {
        this.ReservationService = ReservationService;

        this.consumptionDwService = consumptionDwService;
    }


    @PostMapping("/createReservation")
    public void addReservation(@RequestBody Reservation Reservation) {
        ReservationService.addReservation(Reservation);
    }

    @GetMapping("/reservationById/{id}")
    public List<Reservation> getReservationByEmployee(@PathVariable Integer id) {
        return ReservationService.getReservationByEmployee(id);
    }

    @GetMapping("/meetingsById/{id}")
    public List<Reservation> getMeetingsByEmployee(@PathVariable Integer id) {
        return ReservationService.getMeetingsByEmployee(id);
    }


    @GetMapping("/allReservations")
    public List<Reservation> getReservations() {
        return ReservationService.selectAllReservation();
    }

    @GetMapping("/allReservationsjoin")
    public List<String[]> getReservationsjoin() {
        consumptionDwService.addDataCons();

        return ReservationService.selectJoin();
    }

    @GetMapping("/ownMeetings/{id}")
    public List<Reservation> getOwnMeetings(@PathVariable Integer id) {
        return ReservationService.getOwnMeetings(id);
    }

    @PostMapping("/invite/{id_reservation}/{id_employee}")
    public void invite(@PathVariable Integer id_reservation,@PathVariable Integer id_employee) {
        ReservationService.invite(id_reservation,id_employee);
    }

    @GetMapping("/reservationAuthor/{id}")
    public String getReservationAuthor(@PathVariable Integer id) {
        return ReservationService.getReservationAuthor(id);
    }

    @GetMapping("/meetingParticipants/{id}")
    public Integer getMeetingParticipants(@PathVariable Integer id) {
        return ReservationService.getMeetingParticipants(id);
    }

    @GetMapping("/getSpaceFloor/{id}")
    public Integer getSpaceFloor(@PathVariable Integer id) {
        return ReservationService.getSpaceFloor(id);
    }

    @GetMapping("/getMeetingSubject/{id}")
    public String getMeetingSubject(@PathVariable Integer id) {
        return ReservationService.getMeetingSubject(id);
    }

    @PostMapping("/subjectUpdate")
    public void subjectUpdate(@RequestBody String s) {
        ReservationService.subjectUpdate(s.split("\"")[3],s.split("\"")[7]);
    }
/*
    @GetMapping(path = "/readreservationbyid/{id}")
    public Optional<Reservation> getReservation(@PathVariable Integer id) {
        return ReservationService.getReservationById(id);
    }

    @PutMapping("/updateReservation")
    public void updateReservation(@RequestBody Reservation Reservation) {
        ReservationService.updateReservation(Reservation);
    }

    @DeleteMapping(path = "/deleteReservationbyid/{id}")
    public void deleteReservation(@PathVariable Integer id) {
        ReservationService.deleteReservation(id);

    }*/
}

