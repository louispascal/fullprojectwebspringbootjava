package episen.pds.checkmate.backendcheckmate.model.scope8;

import lombok.Data;

import javax.persistence.*;

@Data

@Table
@Entity
public class Consumer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;


    public Consumer(Integer id, String name) {
        this.id = id;
        this.name = name;

    }

    public Consumer() {

    }


    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
              
                '}';
    }
}

