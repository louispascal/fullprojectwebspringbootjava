package episen.pds.checkmate.backendcheckmate.service.monitoring;

import episen.pds.checkmate.backendcheckmate.model.monitoring.EquipmentEnergy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import episen.pds.checkmate.backendcheckmate.dao.monitoring.EquipmentEnergyRepository;
import episen.pds.checkmate.backendcheckmate.model.monitoring.Consumption;

@Service
public class EquipmentEnergyService {
    private final EquipmentEnergyRepository equipmentRepository;
    private final ConsumptionService consumptionService;

    @Autowired
    public EquipmentEnergyService(EquipmentEnergyRepository equipmentRepository, ConsumptionService consumptionService) {
        this.equipmentRepository = equipmentRepository;
        this.consumptionService = consumptionService;
    }


    public void addEquipment(EquipmentEnergy equipment) {
        equipmentRepository.save(equipment);
    }

    public List<EquipmentEnergy> selectAllEquipment() {
        List<EquipmentEnergy> equipments = equipmentRepository.findAll();
        if(!equipments.isEmpty()){
            for (EquipmentEnergy equipment : equipments) {
                Consumption consumption = consumptionService.findByMaxDateIdEquipment(equipment.getIdEquipment());
                equipment.setValue(consumption.getInstantaneousPower());
                equipment.setSamplingDate(consumption.getSamplingDate());
            }
        }
        return equipments;
    }

    public Optional<EquipmentEnergy> getEquipmentById(Integer id) {
        return equipmentRepository.findById(id);
    }
    
    public List<EquipmentEnergy> findByIdHome(Integer id) {
        List<EquipmentEnergy> equipments = equipmentRepository.findByIdHome(id);
        if(!equipments.isEmpty()){
            equipments.forEach(equipment -> {
                equipment.setValue(equipmentRepository.findValueMaxDateByIdEquipment(equipment.getIdEquipment()));
            });
        }
        return equipments;
    }
    
    public List<EquipmentEnergy> findByIdRoom(Integer id) {
        List<EquipmentEnergy> equipments = equipmentRepository.findByIdRoom(id);
        if(!equipments.isEmpty()){
            for (EquipmentEnergy equipment : equipments) {
                Consumption consumption = consumptionService.findByMaxDateIdEquipment(equipment.getIdEquipment());
                equipment.setValue(consumption.getInstantaneousPower());
                equipment.setSamplingDate(consumption.getSamplingDate());
            }
        }
        return equipments;
    }

    public void updateEquipment(EquipmentEnergy equipment) {
        equipmentRepository.save(equipment);
    }

    public void deleteEquipment(Integer id) {
        equipmentRepository.deleteById(id);
    }


}
