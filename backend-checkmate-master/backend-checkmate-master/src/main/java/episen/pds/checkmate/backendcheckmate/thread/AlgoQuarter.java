package episen.pds.checkmate.backendcheckmate.thread;

import episen.pds.checkmate.backendcheckmate.model.smartgrid.District;
import episen.pds.checkmate.backendcheckmate.service.smartgrid.EnergyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class AlgoQuarter implements Runnable{

private EnergyService energyService;
public int difference;

    @Autowired
    public AlgoQuarter(EnergyService energyService) {
        this.energyService = energyService;
    }

    @Override
    public void run() {

        while(true){

        List<Integer> districtList = energyService.getAllDistricts();
        for(Integer i : districtList){
            Optional<District> d = energyService.getDistrictInfosById(i);
            difference = d.get().getConsumption()-d.get().getProduction();

            if (difference>0){
                energyService.postEnergyToProduced(i,difference);
            }
        }
            //log.info(energyService.getSumInstantaneous_powerToProduced()+"");
            try{
                Thread.sleep(5*1000);
            }catch (InterruptedException e){
                System.out.println(e.getMessage());
            }

        }
    }
    public int getDifference() {
        return difference;
    }
    public void setDifference(int diff){
        this.difference = diff;
    }
}
