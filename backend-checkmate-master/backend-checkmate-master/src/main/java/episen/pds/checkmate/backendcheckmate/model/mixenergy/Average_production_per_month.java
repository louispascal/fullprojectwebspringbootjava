package episen.pds.checkmate.backendcheckmate.model.mixenergy;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "average_production_per_month", schema = "mock")
@Data
public class Average_production_per_month {
    @Id
    private Integer id_average;
    private Integer  production_per_month;
    private Integer month ;
    private String type_of_energy;

}
