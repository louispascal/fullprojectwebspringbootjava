package episen.pds.checkmate.backendcheckmate.model.analyse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "analyze_schema")
@Builder
public class EquipmentAnalyze {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @OneToMany(mappedBy = "equipment")
    private List<HomeEquipment> homeEquipments = new ArrayList<>();
}
