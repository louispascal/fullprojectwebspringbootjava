package episen.pds.checkmate.backendcheckmate.thread;

import episen.pds.checkmate.backendcheckmate.dao.smartgrid.*;
import episen.pds.checkmate.backendcheckmate.model.mixenergy.Site_of_Production;
import episen.pds.checkmate.backendcheckmate.model.smartgrid.Current_season;
import episen.pds.checkmate.backendcheckmate.model.smartgrid.Season;
import episen.pds.checkmate.backendcheckmate.service.smartgrid.EnergyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.*;

@Slf4j
@Component
public class AlgoCapacity implements Runnable{
    private List<SiteCapacity> siteCapacities = new ArrayList<>();
    private Current_Season_Repository current_season_repository;
    private WinterRepository winterRepository;
    private SummerRepository summerRepository;
    private AutumnRepository autumnRepository;
    private SpringRepository springRepository;
    private EnergyService energyService;

    public int sumCapaSolar = 0;
    public int sumCapaHydro = 0;
    public int SumCapaWind = 0;

    public AlgoCapacity(EnergyService energyService, Current_Season_Repository current_season_repository, WinterRepository winterRepository, SummerRepository summerRepository, AutumnRepository autumnRepository, SpringRepository springRepository) {
        this.energyService = energyService;
        this.current_season_repository = current_season_repository;
        this.winterRepository = winterRepository;
        this.summerRepository = summerRepository;
        this.autumnRepository = autumnRepository;
        this.springRepository = springRepository;
    }
    @Override
    public void run() {
        while(true){

            Current_season current_season = current_season_repository.findAll().get(0);
            Season season = null;
            String schedule = current_season.getSchedule().toString().split(":")[0];

            switch (current_season.getCurrent_season()){
                case "spring"->season=springRepository.getSpringByScheduleContains(schedule);
                case "winter"->season=winterRepository.getWinterByScheduleContains(schedule);
                case "summer"->season=summerRepository.getSummerByScheduleContains(schedule);
                case "autumn"->season=autumnRepository.getAutumnByScheduleContains(schedule);
            }

            log.info("SEASON SIMULATED BY SCOPE 3 : " + current_season.getCurrent_season() + " | TIME : " + current_season.getSchedule() + " | SOLAR VALUE : " + season.getSolar() + " LUX -- " + "WIND VALUE : " + season.getWind() + " km/h");
            List<Site_of_Production> sop = energyService.getSite_of_Production();
            sumCapaSolar = 0;
            sumCapaHydro = 0;
            SumCapaWind = 0;

            for (Site_of_Production site_of_production : sop) {
                SiteCapacity siteCapacity = new SiteCapacity();
                siteCapacity.setSite(site_of_production);
                switch (site_of_production.getSite_type()){
                    case "hydraulique"->siteCapacity.setCurrent_capacity(calculationHydraulic());
                    case "éolienne"->siteCapacity.setCurrent_capacity(calculationWindSite(season.getWind()));
                    case "solaire"->siteCapacity.setCurrent_capacity(calculationSolar(season.getSolar()));
                }
                siteCapacities.add(siteCapacity);
                sumCapaSolar += site_of_production.getSite_type().equalsIgnoreCase("solaire") ? siteCapacity.getCurrent_capacity() : 0;
                sumCapaHydro += site_of_production.getSite_type().equalsIgnoreCase("hydraulique") ? siteCapacity.getCurrent_capacity() : 0;
                SumCapaWind += site_of_production.getSite_type().equalsIgnoreCase("éolienne") ? siteCapacity.getCurrent_capacity() : 0;
                //log.info("Type of Site : " + site_of_production.getSite_type() + " -- Current Capacity : " + siteCapacity.getCurrent_capacity() + " kW");
            }

            log.info("SUM SOLAR CAPACITY : " + sumCapaSolar + " -- SUM HYDRAULIC CAPACITY : " + sumCapaHydro + " -- SUM WIND CAPACITY : " + SumCapaWind);

            try{
                Thread.sleep(5*1000);
            }catch (InterruptedException e){
                System.out.println(e.getMessage());
            }
        }
    }
    private Integer calculationWindSite(Integer wind) {
        if (wind == 0) {
            return wind;
        } else {
            return wind * 25 + new Random().nextInt(20) + 10;
        }
    }
    private Integer calculationSolar(Integer solar){
        if(solar == 0){
            return solar;
        }
        else {
            return solar / 4 + new Random().nextInt(20) + 20;
        }
    }
    private Integer calculationHydraulic(){
        return 300;
    }
    public List<SiteCapacity> getSiteCapacities() {
        return siteCapacities;
    }

    public int getSumCapaSolar() {
        return sumCapaSolar;
    }
    public int getSumCapaHydro() {
        return sumCapaHydro;
    }
    public int getSumCapaWind() {
        return SumCapaWind;
    }
}



