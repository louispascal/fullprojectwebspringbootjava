package episen.pds.checkmate.backendcheckmate.model.equipment;


import java.io.Serializable;
import javax.persistence.Column;
import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "light", schema = "energy")
public class Light implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_light")
    private Integer idLight;

    private Integer value;
    private Integer id_equipment;

    @Override
    public String toString() {
        return "light{"
                + "id_light=" + idLight
                + ", value='" + value + '\''
                + ", id_equipment='" + id_equipment + '\''
                + '}';
    }
    
}
