package episen.pds.checkmate.backendcheckmate.api.equipment;

import com.sun.xml.bind.v2.runtime.output.SAXOutput;
import episen.pds.checkmate.backendcheckmate.model.equipment.Heating;
import episen.pds.checkmate.backendcheckmate.service.equipment.HeatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "equipment")
@RestController
public class HeatingController {
    private HeatingService heatingService;
    @Autowired
    public HeatingController(HeatingService heatingService) {
        this.heatingService = heatingService;

    }

    @PostMapping("/setHeating")
    public void updateHeating(@RequestBody Heating heating) {
        System.out.println(heating);
        System.out.println(heating.getValue());
        System.out.println(heating.getMode());
        System.out.println(heating.getId_equipment());
        heatingService.setHeating(heating);

    }

    @GetMapping("/Heating/{id}")
    public Optional<Heating> getHeating(@PathVariable Integer id) {
        return heatingService.selectHeating(id); }

    @GetMapping("/AutoHeating/{id}")
    public Integer autoHeating(@PathVariable Integer id) throws InterruptedException { return heatingService.autoHeating(id);}
}