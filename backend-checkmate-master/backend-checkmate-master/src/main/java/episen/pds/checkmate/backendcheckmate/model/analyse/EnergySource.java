package episen.pds.checkmate.backendcheckmate.model.analyse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity@Builder
@Table(schema = "analyze_schema")
public class EnergySource {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private EnergySourceType energySourceType;
    @ManyToOne
    private DistrictAnalyze district;
    @OneToMany(mappedBy = "energySource")
    private List<ProductionAnalyze> productionAnalyze;
}
