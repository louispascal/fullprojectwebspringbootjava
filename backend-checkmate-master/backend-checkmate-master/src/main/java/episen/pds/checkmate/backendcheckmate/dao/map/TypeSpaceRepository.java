package episen.pds.checkmate.backendcheckmate.dao.map;

import episen.pds.checkmate.backendcheckmate.model.map.TypeSpace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TypeSpaceRepository extends JpaRepository<TypeSpace, Integer> {
    @Query(nativeQuery = true)
    TypeSpace getTypeSpaceByTypeEquals(String type);
}
