package episen.pds.checkmate.backendcheckmate.model.equipment;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import episen.pds.checkmate.backendcheckmate.model.monitoring.EquipmentEnergy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "washing_machine", schema = "energy")
public class WashingMachine implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_washing_machine")
    private Integer idWashingMachine;
    private String mode;
    private Integer id_equipment;

    @Override
    public String toString() {
        return "WashingMachine{"
                + "id_washing_machine=" + idWashingMachine
                + ", mode='" + mode + '\''
                + ", equipment='" + id_equipment + '\''
                + '}';
    }
}
