package episen.pds.checkmate.backendcheckmate.api.scope8;

import episen.pds.checkmate.backendcheckmate.model.scope8.Equipment;
import episen.pds.checkmate.backendcheckmate.service.scope8.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "/equipment")
@RestController


public class EquipmentController {
    private final EquipmentService EquipmentService;

    @Autowired
    public EquipmentController(EquipmentService EquipmentService) {
        this.EquipmentService = EquipmentService;

    }




    @GetMapping("/allEquipments")
    public List<Equipment> getEquipments() {
        return EquipmentService.selectAllEquipment();
    }
    @GetMapping("/EquipmentsAll/{id}")
    public List<Object[]> getEquipmentsByCons(@PathVariable Integer id){
        return EquipmentService.getEquipmentByConsumerId(id);
    }


}

/*public class EquipmentController {

    private EquipmentService equipmentService;

    @Autowired
    public EquipmentController(EquipmentService equipmentService) {
        this.equipmentService = equipmentService;

    }
    @GetMapping(path = "/equipmentcons/{id}")
    public List<Equipment> getEquipmentById(@PathVariable Integer id) {
        equipmentService.insertRand(id);
        return equipmentService.getKWbyConID(id);}


    @GetMapping(path  ="/finishTime/{id}")
    public List<Timestamp>getfinishTimebyID(@PathVariable Integer id){

        return  equipmentService.getFinishTime(id);
    }



}

 */