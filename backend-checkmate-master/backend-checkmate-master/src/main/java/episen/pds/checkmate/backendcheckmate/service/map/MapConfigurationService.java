package episen.pds.checkmate.backendcheckmate.service.map;

import episen.pds.checkmate.backendcheckmate.dao.map.AreaRepository;
import episen.pds.checkmate.backendcheckmate.dao.map.CorridorRepository;
import episen.pds.checkmate.backendcheckmate.dao.map.SpaceRepository;
import episen.pds.checkmate.backendcheckmate.dao.map.TypeSpaceRepository;
import episen.pds.checkmate.backendcheckmate.model.map.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
@Data
public class MapConfigurationService {
    private final TypeSpaceRepository typeSpaceRepository;
    private final SpaceService spaceService;
    private final SpaceRepository spaceRepository;
    private AreaRepository areaRepository;
    private final CorridorRepository corridorRepository;
    private Float sizeOpenSpace = 0.0f;
    private Float sizeDualOffice = 0.0f;
    private Float sizeMeetingRoom = 0.0f;
    private Float sizeKitchen = 0.0f;
    private Float sizeIndividualOffice = 0.0f;

    public MapConfigurationService(TypeSpaceRepository typeSpaceRepository, SpaceService spaceService, SpaceRepository spaceRepository, AreaRepository areaRepository, CorridorRepository corridorRepository) {
        this.typeSpaceRepository = typeSpaceRepository;
        this.spaceService = spaceService;
        this.spaceRepository = spaceRepository;
        this.areaRepository = areaRepository;
        this.corridorRepository = corridorRepository;
    }

    //This method assigns the size of each space type according to the number of corridors on the floor.
    public void getSpacesSizes(Integer corridor) {
        if (corridor == 1) {
            sizeOpenSpace = typeSpaceRepository.getTypeSpaceByTypeEquals("Open Space").getSize_percentage();
            sizeDualOffice = typeSpaceRepository.getTypeSpaceByTypeEquals("Dual Office").getSize_percentage();
            sizeMeetingRoom = typeSpaceRepository.getTypeSpaceByTypeEquals("Meeting room").getSize_percentage();
            ;
            sizeKitchen = typeSpaceRepository.getTypeSpaceByTypeEquals("Kitchen").getSize_percentage();
            sizeIndividualOffice = typeSpaceRepository.getTypeSpaceByTypeEquals("Individual Office").getSize_percentage();
        } else {
            sizeOpenSpace = typeSpaceRepository.getTypeSpaceByTypeEquals("Open Space").getSize_percentage2();
            sizeDualOffice = typeSpaceRepository.getTypeSpaceByTypeEquals("Dual Office").getSize_percentage2();
            sizeMeetingRoom = typeSpaceRepository.getTypeSpaceByTypeEquals("Meeting room").getSize_percentage2();
            sizeKitchen = typeSpaceRepository.getTypeSpaceByTypeEquals("Kitchen").getSize_percentage2();
            sizeIndividualOffice = typeSpaceRepository.getTypeSpaceByTypeEquals("Individual Office").getSize_percentage2();
        }
    }

    //This method calculates the size of the configuration submitted by the client.
    public Float getRequestedSize(MapRequest mapRequest) {
        Float sizeOpenSpaceRequested = sizeOpenSpace * mapRequest.getNb_open_space();
        Float sizeDualOfficeRequested = sizeDualOffice * mapRequest.getNb_office_double();
        Float sizeMeetingRoomRequested = sizeMeetingRoom * mapRequest.getNb_meeting_room();
        Float sizeKitchenRequested = sizeKitchen * mapRequest.getNb_kitchen();
        float sizeRequested = sizeOpenSpaceRequested + sizeDualOfficeRequested + sizeMeetingRoomRequested + sizeKitchenRequested;
        if (mapRequest.getCorridor().contains("2 couloirs") || mapRequest.getCorridor().contains("2")) {
            float sizeIndividualOfficeRequested = sizeIndividualOffice * mapRequest.getNb_office();
            sizeRequested += sizeIndividualOfficeRequested;
        }
        return sizeRequested;
    }

    /*  This method launches the configuration and verify if the configuration is valid.
        If not it returns the problem of the configuration.
        Otherwise, it launches the creation of spaces.
     */
    public String launchConfiguration(MapRequest mapRequest) {
        boolean corridors = mapRequest.getCorridor().contains("2");
        if (corridors) {
            getSpacesSizes(2);
        } else {
            getSpacesSizes(1);
        }
        Float sizeRequested = getRequestedSize(mapRequest);
        Float maxSize = 400.0f;
        String answer = checkSize(mapRequest, sizeRequested, maxSize);
        if (answer.equals("Size match with filling")) {
            mapRequest = launchFilling(mapRequest, corridors, sizeRequested, maxSize);
            configureFloor(mapRequest);
            answer = "Size match";
        } else if (answer.equals("Size match")) {
            configureFloor(mapRequest);
        }
        return answer;
    }

    //This method check if the requested size match the size of the floor.
    public String checkSize(MapRequest mapRequest, Float sizeRequested, Float maxSize) {
        log.info("Configuration requested : " + mapRequest);
        log.info("Size requested by client : " + sizeRequested);
        String answer;
        log.info("Required size : " + maxSize);
        if (sizeRequested > maxSize) {
            answer = "Too many requested";
        } else if (sizeRequested.equals(maxSize)) {
            answer = "Size match";
        } else if (sizeRequested < maxSize && mapRequest.getFill()) {
            answer = "Size match with filling";
        } else {
            answer = "Need more";
        }
        log.info(answer);
        return answer;
    }

    //This method computes the difference between floor size and requested size and launch the filling method.
    public MapRequest launchFilling(MapRequest mapRequest, boolean corridors, Float sizeRequested, Float maxSize) {
        float difference = maxSize - sizeRequested;
        log.info("The difference is : " + difference);
        return fillEmptySpace(difference, sizeRequested, maxSize, mapRequest, corridors);
    }

    //This method adds needed spaces to match the floor size.
    public MapRequest fillEmptySpace(float difference, Float sizeRequested, Float maxSize, MapRequest mapRequest, boolean corridors) {
        MapRequest newMapRequest = mapRequest.clone();
        if (difference == 100 || difference == 200 || difference == 300) {
            log.info("Filling empty space with open space");
            float add = newMapRequest.getNb_open_space() + (difference / 100.0f);
            newMapRequest.setNb_open_space((int) add);
        } else if (difference == 40) {
            int randomValue = new Random().nextInt(2);
            if (randomValue == 0) {
                log.info("Filling empty space with meeting room");
                Integer add = newMapRequest.getNb_meeting_room() + 1;
                newMapRequest.setNb_meeting_room(add);
            } else {
                log.info("Filling empty space with kitchen");
                Integer add = newMapRequest.getNb_kitchen() + 1;
                newMapRequest.setNb_kitchen(add);
            }
        } else if (difference == 20) {
            log.info("Filling empty space with dual office");
            Integer add = newMapRequest.getNb_office_double() + 1;
            newMapRequest.setNb_office_double(add);
        } else if (difference == 10 && corridors) {
            log.info("Filling empty space with individual office");
            Integer add = newMapRequest.getNb_office() + 1;
            newMapRequest.setNb_office(add);
        } else {
            log.info("Filling empty space with dual office");
            float size = 16.66666603088379f;
            if (corridors) {
                size = 20;
            }
            int add = mapRequest.getNb_office_double();
            while (difference > 7.0f) {
                sizeRequested += size;
                add += 1;
                log.info("We will add " + add);
                difference = maxSize - sizeRequested;
                log.info("Difference : " + difference);
            }
            newMapRequest.setNb_office_double(add);
        }
        return newMapRequest;
    }

    //This method empties a floor if it is full and then fills it with the new requested configuration.
    public void configureFloor(MapRequest mapRequest) {
        Integer id_floor = mapRequest.getId_floor();
        clearFloor(id_floor);
        ArrayList<String> listArea = shuffleArray();
        ArrayList<String> listAreaCopy = new ArrayList<>(listArea);
        createCorridors(mapRequest);
        if (mapRequest.getCorridor().contains("2")) {
            saveIndividualOffice(listArea, mapRequest, mapRequest.getNb_office(), 5, "Bureau simple");
        }
        if (listAreaCopy.size() != 0) {
            listAreaCopy = saveSpaces(listAreaCopy, mapRequest, mapRequest.getNb_meeting_room(), 3, "Salle de réunion");
        }
        if (listAreaCopy.size() != 0) {
            listAreaCopy = saveSpaces(listAreaCopy, mapRequest, mapRequest.getNb_office_double(), 4, "Bureau double");
        }
        if (listAreaCopy.size() != 0) {
            listAreaCopy = saveSpaces(listAreaCopy, mapRequest, mapRequest.getNb_kitchen(), 2, "Cuisine");
        }
        if (listAreaCopy.size() != 0) {
            saveSpaces(listAreaCopy, mapRequest, mapRequest.getNb_open_space(), 1, "Espace ouvert");
        }

    }

    public void clearFloor(Integer id_floor) {
        if (!spaceRepository.isEmpty(id_floor)) {
            spaceRepository.deleteSpaces(id_floor);
            spaceRepository.deleteCorridors(id_floor);
        }
    }

    //This method creates and saves 1 or 2 corridors depending on the requested configuration.
    public void createCorridors(MapRequest mapRequest) {
        Integer id_floor = mapRequest.getId_floor();
        Corridor horizontal = new Corridor(id_floor, "H");
        if (mapRequest.getCorridor().contains("2")) {
            Corridor vertical = new Corridor(id_floor, "V");
            corridorRepository.save(horizontal);
            corridorRepository.save(vertical);
        } else {
            corridorRepository.save(horizontal);
        }
    }

    /*
       This method saves as much individual office as required in a zone as long as the zone is not full.
       Then moves to the next zone when the first one is full.
    */
    public void saveIndividualOffice(ArrayList<String> listArea, MapRequest mapRequest, Integer nbSpace, Integer id_type, String typeName) {
        String area;
        int countE = 0;
        int countW = 4;
        for (int i = 0; i < nbSpace; i++) {
            area = listArea.get(0);
            log.info("Configuration area :" + area);
            Area areaToFill = areaRepository.getAreasByFloor(mapRequest.getId_building(), mapRequest.getId_floor(), area).get(0);
            Integer areaToFillID = areaToFill.getId_area();
            Integer max = spaceRepository.getMaxSpaceNumberByType(id_type).orElse(0);
            Space spaceToSave = new Space(max + 1, areaToFillID, id_type, typeName);
            if ((area.equals("NW") || area.equals("SW"))) {
                countW++;
                spaceToSave.setPosition_in_area(countW);
            } else if (area.equals("NE") || area.equals("SE")) {
                countE++;
                spaceToSave.setPosition_in_area(countE);
            }
            log.info("Saving...");
            spaceRepository.save(spaceToSave);
            if (countW == 6 || countE == 2) {
                countW = 4;
                countE = 0;
                listArea.remove(0);
            }
        }
    }

    /*
        This method saves as much space as required in a zone as long as the zone is not full.
        Then moves to the next zone when the first one is full.
     */
    public ArrayList<String> saveSpaces(ArrayList<String> listArea, MapRequest mapRequest, Integer nbSpace, Integer type, String typeName) {
        log.info("CREATING " + typeName);
        String area = "";
        int max_position_in_area_replacement = -1;
        for (int i = 0; i < nbSpace; i++) {
            area = listArea.get(0);
            log.info("Configuration area :" + area);
            Area areaToFill = areaRepository.getAreasByFloor(mapRequest.getId_building(), mapRequest.getId_floor(), area).get(0);
            Integer areaToFillID = areaToFill.getId_area();
            if (spaceRepository.getRemainingSpace2(areaToFillID).orElse(0.0f) < 99.0f || spaceRepository.getRemainingSpace(areaToFillID).orElse(0.0f) < 99.0f) {
                Integer max = spaceRepository.getMaxSpaceNumberByType(type).orElse(0);
                Space spaceToSave = new Space(max + 1, areaToFillID, type, typeName);
                int max_position_in_area = spaceRepository.getMaxPositionInArea(areaToFillID).orElse(0);
                if (max_position_in_area == 5 || max_position_in_area == 6) {
                    max_position_in_area_replacement++;
                } else {
                    max_position_in_area_replacement = max_position_in_area;
                }
                if (type == 1) {
                    spaceToSave.setPosition_in_area(1);
                } else {
                    spaceToSave.setPosition_in_area(max_position_in_area_replacement + 1);
                }
                spaceRepository.save(spaceToSave);
                if (mapRequest.getCorridor().contains("2")) {
                    if (spaceRepository.getRemainingSpace2(areaToFillID).orElse(0.0f) > 99.0f || spaceRepository.getRemainingSpace2(areaToFillID).orElse(0.0f) < 0.0f) {
                        listArea.remove(0);
                        max_position_in_area_replacement = -1;
                    }
                } else {
                    if (spaceRepository.getRemainingSpace(areaToFillID).orElse(0.0f)  > 99.0f || spaceRepository.getRemainingSpace(areaToFillID).orElse(0.0f)  < 0.0f) {
                        listArea.remove(0);
                        max_position_in_area_replacement = -1;
                    }
                }
            }
        }
        return listArea;

    }

    //This method returns a list of areas positions in a random order.
    public ArrayList<String> shuffleArray() {
        String[] areaArray = {"NW", "NE", "SW", "SE"};
        List<String> areaList = Arrays.asList(areaArray);
        Collections.shuffle(areaList);
        log.info("Random area list generated is : " + areaList);
        return new ArrayList<>(areaList);
    }
}
