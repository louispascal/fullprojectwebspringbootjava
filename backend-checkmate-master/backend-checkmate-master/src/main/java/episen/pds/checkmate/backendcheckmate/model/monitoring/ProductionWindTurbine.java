/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package episen.pds.checkmate.backendcheckmate.model.monitoring;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Ines
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "production_wind_turbine", schema = "energy")
public class ProductionWindTurbine implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_production_wind_turbine")
    private Integer idProductionWindTurbine;
    
    @Column(name = "instantaneous_power")
    private Double instantaneousPower;

    @Column(name = "sampling_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date samplingDate;

    @JoinColumn(name = "id_wind_turbine", referencedColumnName = "id_wind_turbine")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private WindTurbine windTurbine;

}
