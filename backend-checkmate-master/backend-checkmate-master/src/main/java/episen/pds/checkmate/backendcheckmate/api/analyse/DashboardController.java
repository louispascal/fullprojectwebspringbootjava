package episen.pds.checkmate.backendcheckmate.api.analyse;

import episen.pds.checkmate.backendcheckmate.service.analyse.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "analyse")
@RestController
public class DashboardController {
    @Autowired
    private DashboardService dashboardService;

    @GetMapping("/statistics")
    public List<DashboardDto> getStatistics() {
        return dashboardService.getStatistics();
    }

    @GetMapping("/getDistrictHistory/{name}")
    public List<CompGraph> getDistrictHistory(@PathVariable(value = "name") String name) {
        return dashboardService.getDistrictHistory(name);
    }
    @GetMapping("/getBuildingHistory/{name}")
    public List<CompGraph> getBuildingHistory(@PathVariable(value = "name") String name) {
        return dashboardService.getBuildingHistory(name);
    }
    @GetMapping("/getHomeHistory/{name}")
    public List<CompGraph> getHomeHistory(@PathVariable(value = "name") String name) {
        return dashboardService.getHomeHistory(name);
    }
    @GetMapping("/getEquipmentHistory/{name}")
    public List<CompGraph> getEquipmentHistory(@PathVariable(value = "name") String name) {
        return dashboardService.getEquipmentHistory(name);
    }


    @GetMapping("/getProductionAndConsumptionPerDistrict")
    public List<CompGraph> getProductionAndConsumptionPerDistrict() {
        return dashboardService.getProductionAndConsumptionPerDistrict();
    }
    @GetMapping("/getProductionAndConsumptionPerBuilding/{district}")
    public List<CompGraph> getProductionAndConsumptionPerBuilding(@PathVariable(value = "district") String district) {
        return dashboardService.getProductionAndConsumptionPerBuilding(district);
    }
    @GetMapping("/getProductionAndConsumptionPerHome/{building}")
    public List<CompGraph> getProductionAndConsumptionPerHome(@PathVariable(value = "building") String building) {
        return dashboardService.getProductionAndConsumptionPerHome(building);
    }
    @GetMapping("/getProductionAndConsumptionPerEquipment/{home}")
    public List<CompGraph> getProductionAndConsumptionPerEquipment(@PathVariable(value = "home") String home) {
        return dashboardService.getProductionAndConsumptionPerEquipment(home);
    }
}
