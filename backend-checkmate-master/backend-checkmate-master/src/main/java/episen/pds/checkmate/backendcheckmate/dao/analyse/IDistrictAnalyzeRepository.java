package episen.pds.checkmate.backendcheckmate.dao.analyse;

import episen.pds.checkmate.backendcheckmate.model.analyse.DistrictAnalyze;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDistrictAnalyzeRepository extends JpaRepository<DistrictAnalyze, Integer> {

}
