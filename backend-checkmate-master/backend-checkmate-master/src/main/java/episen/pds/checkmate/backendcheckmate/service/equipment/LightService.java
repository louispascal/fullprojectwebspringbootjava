package episen.pds.checkmate.backendcheckmate.service.equipment;

import episen.pds.checkmate.backendcheckmate.dao.equipment.LightRepository;
import episen.pds.checkmate.backendcheckmate.model.equipment.Light;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LightService {
    @Autowired
    private final LightRepository lightRepository;

    public LightService(LightRepository lightRepository){
        this.lightRepository =lightRepository;

    }
    public void setLight(Light light){
        lightRepository.setLightValue(light.getValue(), light.getId_equipment());
    }
    public Optional<Light> selectLight(Integer id){
        return lightRepository.getLightValue(id);
    }
}
