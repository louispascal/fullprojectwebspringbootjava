package episen.pds.checkmate.backendcheckmate.model.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MapRequest implements Cloneable {
    private Integer id_building;
    private Integer id_floor;
    private String corridor;
    private Integer nb_open_space;
    private Integer nb_office;
    private Integer nb_kitchen;
    private Integer nb_meeting_room;
    private Integer nb_office_double;
    private Boolean fill;

    @Override
    public MapRequest clone() {
        try {
            return (MapRequest) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
