package episen.pds.checkmate.backendcheckmate.dao.scope8;

import episen.pds.checkmate.backendcheckmate.model.scope8.Consumer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsumerRepository extends JpaRepository<Consumer, Integer> {


}