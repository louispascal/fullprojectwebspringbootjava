package episen.pds.checkmate.backendcheckmate.model.analyse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity@Builder
@Table(schema = "analyze_schema")
public class DistrictAnalyze {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @OneToMany(mappedBy = "district")
    private List<BuildingAnalyze> buildings = new ArrayList<>();
}
