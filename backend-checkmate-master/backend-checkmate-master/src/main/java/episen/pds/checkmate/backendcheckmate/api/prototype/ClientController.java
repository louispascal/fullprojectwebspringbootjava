package episen.pds.checkmate.backendcheckmate.api.prototype;

import episen.pds.checkmate.backendcheckmate.model.prototype.Client;
import episen.pds.checkmate.backendcheckmate.service.prototype.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/*
This class represent the client controller of the backend
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "prototype/client")
@RestController
public class ClientController {
    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;

    }

    @PostMapping("/create")
    public void addClient(@RequestBody Client client) {
        clientService.addClient(client);
    }

    @GetMapping("/read")
    public List<Client> getClients() {
        return clientService.selectAllClient();
    }

    @GetMapping(path = "/readbyid/{id}")
    public Optional<Client> getClient(@PathVariable Integer id) {
        return clientService.getClientById(id);
    }

    @PutMapping("/update")
    public void updateClient(@RequestBody Client client) {
        clientService.updateClient(client);
    }

    @DeleteMapping(path = "/deletebyid/{id}")
    public void deleteClient(@PathVariable Integer id) {
        clientService.deleteClient(id);

    }
}
