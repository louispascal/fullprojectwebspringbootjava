package episen.pds.checkmate.backendcheckmate.model.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "role", schema = "energy")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_role")
    private int idRole;
    
    @Column(name = "configure_equipment")
    private boolean configureEquipment;
    
    @Column(name = "monitoring_equipment")
    private boolean monitoringEquipment;
    
    @Column(name = "smart_grid")
    private boolean smartGrid;
    
    @Column(name = "alerting_citizen")
    private boolean alertingCitizen;
    
    @Column(name = "alerting_rse")
    private boolean alertingRse;
    
    @Column(name = "mix_energy")
    private boolean mixEnergy;
    
    @Column(name = "smart_city")
    private boolean smartCity;
}
