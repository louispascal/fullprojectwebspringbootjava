package episen.pds.checkmate.backendcheckmate.service.scope8;

import episen.pds.checkmate.backendcheckmate.dao.scope8.ConsumptionDwRepository;
import episen.pds.checkmate.backendcheckmate.model.scope8.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class ConsumptionDwService {
    private final ConsumptionDwRepository ConsumptionDwRepository ;
    private final ReservationService res;

    @Autowired
    public ConsumptionDwService(ConsumptionDwRepository ConsumptionDwRepository, ReservationService res) {
        this.ConsumptionDwRepository = ConsumptionDwRepository;
        this.res = res;
    }




    public void addDataCons(){
       List<Reservation> reservation= res.selectAlltime();

       Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        LocalDateTime now = LocalDateTime.now();
        for(Reservation reserv : reservation){
            if (ConsumptionDwRepository.getConsumptionsMax(reserv.getId_reservation())==null){
                ConsumptionDwRepository.voidInsertDataCons(reserv.getId_reservation(),1,1,"Watt-seconde",now);
                ConsumptionDwRepository.voidInsertDataCons(reserv.getId_reservation(),3,1,"Watt-seconde",now);
                ConsumptionDwRepository.voidInsertDataCons(reserv.getId_reservation(),2,1,"Watt-seconde",now);
             /*   ConsumptionDwRepository.voidInsertDataCons(reserv.getId_reservation(),1,ConsumptionDwRepository.getConsumptionsMax(reserv.getId_reservation()).intValue()+10,"W",now);
                ConsumptionDwRepository.voidInsertDataCons(reserv.getId_reservation(),3,ConsumptionDwRepository.getConsumptionsMax(reserv.getId_reservation()).intValue()+10,"W",now);
                ConsumptionDwRepository.voidInsertDataCons(reserv.getId_reservation(),2,ConsumptionDwRepository.getConsumptionsMax(reserv.getId_reservation()).intValue()+10,"W",now);
   */
            }

                ConsumptionDwRepository.voidInsertDataCons(reserv.getId_reservation(),1,ConsumptionDwRepository.getConsumptionsMax(reserv.getId_reservation()).intValue()+2,"Watt-seconde",now);
                ConsumptionDwRepository.voidInsertDataCons(reserv.getId_reservation(),3,ConsumptionDwRepository.getConsumptionsMax(reserv.getId_reservation()).intValue()+2,"Watt-seconde",now);
                ConsumptionDwRepository.voidInsertDataCons(reserv.getId_reservation(),2,ConsumptionDwRepository.getConsumptionsMax(reserv.getId_reservation()).intValue()+2,"Watt-seconde",now);
            }

        }

    public List<String[]> consumptionByID(int id){

        return ConsumptionDwRepository.getConsumptionDwyId(id) ;

    }

}
