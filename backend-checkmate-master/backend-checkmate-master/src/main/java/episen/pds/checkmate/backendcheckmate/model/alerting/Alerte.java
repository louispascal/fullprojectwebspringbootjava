package episen.pds.checkmate.backendcheckmate.model.alerting;

import javax.persistence.*;

@Entity
@Table
public class Alerte {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer alerteid;
    private String  message;

    public Alerte(Integer alerteid, String message) {
        this.alerteid = alerteid;
        this.message = message;
    }

    public Alerte() {
    }

    public Integer getAlerteid() {
        return alerteid;
    }

    public void setAlerteid(Integer alerteid) {
        this.alerteid = alerteid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}