package episen.pds.checkmate.backendcheckmate.model.analyse;


import episen.pds.checkmate.backendcheckmate.model.scope8.Equipment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(schema = "analyze_schema")
public class HomeEquipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany(mappedBy = "homeEquipment")
    private List<ConsumptionAnalyze> consumption;
    @ManyToOne(cascade = CascadeType.ALL)
    private HomeAnalyze home;
    @ManyToOne(cascade = CascadeType.ALL)
    private EquipmentAnalyze equipment;
}



