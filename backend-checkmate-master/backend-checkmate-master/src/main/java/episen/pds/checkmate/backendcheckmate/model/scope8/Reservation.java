package episen.pds.checkmate.backendcheckmate.model.scope8;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "dwp")
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_reservation;
    private Integer id_employee;
    private Integer id_spacepk;
    private String date;
    private String end_date;

}
