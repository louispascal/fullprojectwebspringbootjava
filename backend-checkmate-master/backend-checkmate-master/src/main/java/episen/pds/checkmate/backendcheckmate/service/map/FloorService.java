package episen.pds.checkmate.backendcheckmate.service.map;

import episen.pds.checkmate.backendcheckmate.dao.map.FloorRepository;
import episen.pds.checkmate.backendcheckmate.model.map.Floor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FloorService {

    private final FloorRepository floorRepository;

    @Autowired
    public FloorService(FloorRepository floorRepository) {
        this.floorRepository = floorRepository;
    }

    public List<Floor> getFloorsByBuilding(Integer id_building) {
        return floorRepository.getFloorByBuilding(id_building);
    }

    public Optional<Floor> getFloorById(Integer id) {
        return floorRepository.findById(id);
    }

}
