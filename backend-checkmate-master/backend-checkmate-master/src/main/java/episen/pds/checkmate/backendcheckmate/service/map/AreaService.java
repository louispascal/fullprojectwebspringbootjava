package episen.pds.checkmate.backendcheckmate.service.map;

import episen.pds.checkmate.backendcheckmate.dao.map.AreaRepository;
import episen.pds.checkmate.backendcheckmate.model.map.Area;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AreaService {
    private final AreaRepository areaRepository;

    @Autowired
    public AreaService(AreaRepository areaRepository) {
        this.areaRepository = areaRepository;
    }

    public List<Area> getAreasFromFloor(Integer id_building, Integer id_floor, String area) {
        return areaRepository.getAreasByFloor(id_building, id_floor, area);
    }
}
