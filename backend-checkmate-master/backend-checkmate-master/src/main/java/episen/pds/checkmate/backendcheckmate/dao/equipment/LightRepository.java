package episen.pds.checkmate.backendcheckmate.dao.equipment;

import episen.pds.checkmate.backendcheckmate.model.equipment.Heating;
import episen.pds.checkmate.backendcheckmate.model.equipment.Light;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface LightRepository extends JpaRepository<Light, Integer> {
    @Transactional
    @Modifying
    @Query(value = "UPDATE energy.light SET value=?1 WHERE id_equipment=?2 ", nativeQuery = true)
    void setLightValue(Integer lightValue, Integer id);
    @Query(value = "Select *  from energy.light where id_equipment=?1 ",nativeQuery = true)
    Optional<Light> getLightValue(Integer id);
    @Query(value = "Select * from energy.light where id_equipment= ?1",nativeQuery = true)
    Optional<Heating> findLight(Integer id);
}
