package episen.pds.checkmate.backendcheckmate.model.prototype;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String firstName;
    private int age;
    private String message;


    public Client(Integer id, String name, String firstName, int age, String message) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.age = age;
        this.message = message;
    }

    public Client() {

    }


    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", age=" + age +
                ", message='" + message + '\'' +
                '}';
    }
}
