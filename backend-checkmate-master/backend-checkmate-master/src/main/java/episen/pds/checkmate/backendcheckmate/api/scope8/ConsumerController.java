package episen.pds.checkmate.backendcheckmate.api.scope8;

import episen.pds.checkmate.backendcheckmate.model.scope8.Consumer;
import episen.pds.checkmate.backendcheckmate.service.scope8.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "/cons")
@RestController
public class ConsumerController {
    private ConsumerService consumerService;

    @Autowired
    public ConsumerController(ConsumerService consumerService) {
        this.consumerService = consumerService;

    }
        @GetMapping(path = "/consumer/{id}")
        public Optional<Consumer> getConsumerById(@PathVariable Integer id) {
            return consumerService.getConsumerById(id);}



}