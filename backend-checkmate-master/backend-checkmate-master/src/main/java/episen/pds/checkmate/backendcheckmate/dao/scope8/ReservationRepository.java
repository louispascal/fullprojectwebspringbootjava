package episen.pds.checkmate.backendcheckmate.dao.scope8;

import episen.pds.checkmate.backendcheckmate.model.map.Space;
import episen.pds.checkmate.backendcheckmate.model.scope8.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

  @Query(value = "select * from dwp.reservation inner join dwp.space on reservation.id_spacepk=space.id_space inner join employee on reservation.id_employee=employee.id_employee", nativeQuery = true)
  List<String[]> allReservationsJoint();

  @Query(value = "select * from dwp.reservation where end>?1 and date<?1", nativeQuery = true)
  List<Reservation> selectAllcurents(Timestamp lt, Timestamp lt2);

  // @Query(value="select end_date from reservation where end_date > ?1")
  @Modifying
  @Transactional
  @Query(value = "insert into dwp.reservation (id_employee,id_spacepk,date,end_date) values (?1,?2,?3,?4)", nativeQuery = true)
  void addReservation(Integer id_employee, Integer id_spacepk, Timestamp date, Timestamp end_date);

  @Query(value = "select * from dwp.reservation where id_employee=?1 order by id_spacepk", nativeQuery = true)
  List<Reservation> getReservationByEmployee(Integer id_employee);

  @Query(value = "select * from dwp.reservation where (id_reservation in (select id_reservation from dwp.invited where id_employee=?1)) or (id_employee=?1 and id_spacepk in (select id_space from dwp.space where id_type_space=3)) order by id_spacepk", nativeQuery = true)
  List<Reservation> getMeetingsByEmployee(Integer id_employee);

  @Query(value = "select firstname,name from dwp.employee where id_employee=?1", nativeQuery = true)
  List<String> getReservationAuthor(Integer id_employee);

  @Query(value = "Select count(id_employee) from dwp.invited where id_reservation=?1", nativeQuery = true)
  Integer getMeetingParticipants(Integer id_reservation);

  @Query(value = "select * from dwp.reservation where id_employee=?1 and id_spacepk in (select id_space from dwp.space where id_type_space=3) order by id_spacepk", nativeQuery = true)
  List<Reservation> getOwnMeetings(Integer id);

  @Modifying
  @Transactional
  @Query(value = "insert into dwp.invited (id_reservation, id_employee) values (?1,?2)", nativeQuery = true)
  void invite(Integer id_reservation, Integer id_employee);

  @Query(value = "select floor_number from dwp.floor where id_floor=(select id_floor from dwp.area where id_area=(select id_area from dwp.space where id_space=?1))", nativeQuery = true)
  Integer getSpaceFloor(Integer id);

  @Query(value = "select subject from dwp.meetingsubject where id_reservation=?1", nativeQuery = true)
  String getMeetingSubject(Integer id);

  @Modifying
  @Transactional
  @Query(value = "insert into dwp.meetingsubject (id_reservation, subject) values (?2,?1)", nativeQuery = true)
  void subjectUpdate(String s, Integer r);

  @Modifying
  @Transactional
  @Query(value = "delete from dwp.meetingsubject where id_reservation=?1", nativeQuery = true)
  void deleteSubject(Integer r);
}