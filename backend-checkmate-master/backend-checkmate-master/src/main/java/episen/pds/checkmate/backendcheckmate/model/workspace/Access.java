package episen.pds.checkmate.backendcheckmate.model.workspace;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "dwp")
public class Access {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_access;
    private boolean has_equipments;
    private boolean has_monitoring_meeting_room;
    private boolean has_smart_city_activity;
    private boolean has_message;
    private boolean has_meeting;
    private boolean has_reservation;
    private boolean has_configuration_plan;
    private boolean has_plan;
    private boolean has_consumption;


}
