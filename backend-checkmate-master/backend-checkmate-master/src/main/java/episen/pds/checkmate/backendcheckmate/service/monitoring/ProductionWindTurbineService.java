package episen.pds.checkmate.backendcheckmate.service.monitoring;

import episen.pds.checkmate.backendcheckmate.dao.monitoring.ProductionWindTurbineRepository;
import episen.pds.checkmate.backendcheckmate.model.monitoring.ProductionWindTurbine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductionWindTurbineService {
    private final ProductionWindTurbineRepository productionWindTurbineRepository;

    @Autowired
    public ProductionWindTurbineService(ProductionWindTurbineRepository productionWindTurbineRepository) {
        this.productionWindTurbineRepository = productionWindTurbineRepository;
    }


    public void addProductionWindTurbine(ProductionWindTurbine productionWindTurbine) {
        productionWindTurbineRepository.save(productionWindTurbine);
    }

    public List<ProductionWindTurbine> selectAllProductionWindTurbine() {
        return productionWindTurbineRepository.findAll();
    }

    public List<ProductionWindTurbine> findByIdWindTurbine(int idBuilding) {
        return productionWindTurbineRepository.findByIdWindTurbine(idBuilding);
    }

    public Optional<ProductionWindTurbine> getProductionWindTurbineById(Integer id) {
        return productionWindTurbineRepository.findById(id);
    }

    public void updateProductionWindTurbine(ProductionWindTurbine productionWindTurbine) {
        productionWindTurbineRepository.save(productionWindTurbine);
    }

    public void deleteProductionWindTurbine(Integer id) {
        productionWindTurbineRepository.deleteById(id);
    }


}
