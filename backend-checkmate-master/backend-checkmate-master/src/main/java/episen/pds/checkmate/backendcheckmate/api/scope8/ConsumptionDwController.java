package episen.pds.checkmate.backendcheckmate.api.scope8;

import episen.pds.checkmate.backendcheckmate.service.scope8.ConsumptionDwService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
This class represent the ConsumptionDw controller of the backend
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "prototype/ConsumptionDw")
@RestController
public class ConsumptionDwController {
    private final ConsumptionDwService ConsumptionDwService;

    @Autowired
    public ConsumptionDwController(ConsumptionDwService ConsumptionDwService) {
        this.ConsumptionDwService = ConsumptionDwService;

    }


    @GetMapping("/insertDataCons")
    public void addConsumptionDw() {
        ConsumptionDwService.addDataCons();
    }

    @GetMapping("/consumption/{id}")
    public List<String[]> listConsumptionDw(@PathVariable Integer id) {
        ConsumptionDwService.addDataCons();

        return ConsumptionDwService.consumptionByID(id);
    }

}
