package episen.pds.checkmate.backendcheckmate.dao.common;

import episen.pds.checkmate.backendcheckmate.model.common.Citizen;

import java.util.List;

import episen.pds.checkmate.backendcheckmate.model.common.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CitizenRepository extends JpaRepository<Citizen, Integer> {

    @Query(value = "select * from energy.citizen r INNER JOIN energy.role h on r.id_role = h.id_role WHERE h.id_role = ?1 ORDER BY r.name ASC", nativeQuery = true)
    List<Citizen> findByIdRole(int idRole);

    @Query(value = "select * from energy.citizen c WHERE c.mail = ?1 LIMIT 1", nativeQuery = true)
    Citizen findByMail(String mail);

}
