package episen.pds.checkmate.backendcheckmate.dao.smartgrid;

import episen.pds.checkmate.backendcheckmate.model.smartgrid.Autumn;
import episen.pds.checkmate.backendcheckmate.model.smartgrid.Spring;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

@Repository
public interface SpringRepository extends JpaRepository<Spring, Integer> {

    Spring getSpringByScheduleContains(String schedule);

}

