package episen.pds.checkmate.backendcheckmate.model.scope8;


import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(schema = "dwp")
public class Consumptiondw {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer  id_consumption;
    private Integer idreservation_consumption;
    private Integer  idcons_equipment;
    private String unity;
    private Integer value_consumption;
    private String date_prelevment;
    @OneToOne
    private Equipment equipment;
    @OneToOne
    private Consumptiondw consumptiondw;
    public Consumptiondw(Integer id_consumption , int idreservation_consumption,int idcons_equipment,String unity,   Integer value_consumption,String date_prelevment) {
        this.id_consumption = id_consumption;
        this.idreservation_consumption = idreservation_consumption;
        this.idcons_equipment= idcons_equipment;
        this.unity=unity;
        this.value_consumption=value_consumption;
        this.date_prelevment=date_prelevment;


    }
    public Consumptiondw() {

    }
    @Override
    public String toString() {
        return "Equipement{" +
                "idequip=" + id_consumption+
                ", name='" + idreservation_consumption+ '\'' +
                ", idconsumer='" + idcons_equipment + '\'' +
                ", kw=" + unity + '\'' +
                ", id=" + value_consumption + '\'' +
                ", time=" + date_prelevment + '\'' +




                '}';
    }




}

