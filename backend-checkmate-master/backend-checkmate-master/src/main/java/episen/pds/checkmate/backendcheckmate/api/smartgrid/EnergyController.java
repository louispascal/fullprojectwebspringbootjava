package episen.pds.checkmate.backendcheckmate.api.smartgrid;

import episen.pds.checkmate.backendcheckmate.model.smartgrid.District;
import episen.pds.checkmate.backendcheckmate.service.smartgrid.EnergyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "/district")
@RestController

public class EnergyController {
    private final EnergyService energyService;

    @Autowired
    public EnergyController(EnergyService energyService) {
        this.energyService = energyService;

    }

    @GetMapping("/read")
    public List<Integer> getDistricts() {
        return energyService.getAllDistricts();
    } //HomePage

    @GetMapping(path = "/readbydistrict/{id}") //Enter in the district --> GET CONSUMPTION AND PRODUCTION OF A DISTRICT
    public Optional<District> getDistrictInfos(@PathVariable Integer id) {
        return energyService.getDistrictInfosById(id);}

    //POST - INSERT THE VALUE TO PRODUCED FOR THE DISTRICT
    @PostMapping(path = "/insert/{id}/{value}")
    public void postDistrictEnergyToProduced(@PathVariable Integer id, @PathVariable Integer value)
    {energyService.postEnergyToProduced(id,value);}

}

