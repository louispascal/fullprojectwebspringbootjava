package episen.pds.checkmate.backendcheckmate.service.monitoring;

import episen.pds.checkmate.backendcheckmate.dao.monitoring.RoomRepository;
import episen.pds.checkmate.backendcheckmate.model.equipment.Equipments;
import episen.pds.checkmate.backendcheckmate.model.monitoring.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoomService {

    private final RoomRepository roomRepository;

    @Autowired
    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public void addRoom(Room room) {
        roomRepository.save(room);
    }

    public List<Room> selectAllRoom() {
        return roomRepository.findAll();
    }

    public List<Room> findByIdHome(int idHome) {
        List<Room> rooms = roomRepository.findByIdHome(idHome);
        return rooms;
    }

    public Optional<Room> getRoomById(Integer id) {
        return roomRepository.findById(id);
    }

    public void updateRoom(Room room) {
        roomRepository.save(room);
    }

    public void deleteRoom(Integer id) {
        roomRepository.deleteById(id);
    }

}
