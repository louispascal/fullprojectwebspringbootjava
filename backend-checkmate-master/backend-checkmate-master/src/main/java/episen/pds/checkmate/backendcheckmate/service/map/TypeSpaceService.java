package episen.pds.checkmate.backendcheckmate.service.map;

import episen.pds.checkmate.backendcheckmate.dao.map.TypeSpaceRepository;
import episen.pds.checkmate.backendcheckmate.model.map.TypeSpace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TypeSpaceService {
    private final TypeSpaceRepository typeSpaceRepository;

    @Autowired
    public TypeSpaceService(TypeSpaceRepository typeSpaceRepository) {
        this.typeSpaceRepository = typeSpaceRepository;
    }

    public TypeSpace getTypeSpaceByTypeEquals(String type) {
        return typeSpaceRepository.getTypeSpaceByTypeEquals(type);
    }
}
