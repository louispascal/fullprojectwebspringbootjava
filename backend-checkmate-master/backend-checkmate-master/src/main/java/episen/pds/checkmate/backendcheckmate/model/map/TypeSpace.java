package episen.pds.checkmate.backendcheckmate.model.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "typespace" , schema = "dwp")
public class TypeSpace {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_type_space")
    private Integer id_type_space;
    @Column(name = "type")
    private String type;
    @Column(name = "size_percentage")
    private Float size_percentage;
    @Column(name = "size_percentage_2")
    private Float size_percentage2;

}
