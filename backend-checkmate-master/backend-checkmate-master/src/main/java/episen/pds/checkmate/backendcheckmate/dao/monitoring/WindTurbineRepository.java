package episen.pds.checkmate.backendcheckmate.dao.monitoring;

import episen.pds.checkmate.backendcheckmate.model.monitoring.WindTurbine;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ines
 */
@Repository
public interface WindTurbineRepository extends JpaRepository<WindTurbine, Integer> {

    @Query(value = "SELECT * from energy.wind_turbine w INNER JOIN energy.building b ON w.id_building = b.id_building WHERE b.id_building = ?1", nativeQuery = true)
    List<WindTurbine> findByIdBuilding(int idBuilding);
    
    @Query(value = "SELECT instantaneous_power FROM energy.production_wind_turbine WHERE id_wind_turbine = ?1 AND sampling_date = (SELECT Max(sampling_date) FROM energy.production_wind_turbine WHERE id_wind_turbine =  ?1)", nativeQuery = true)
    Double findValueMaxDateByIdWindTurbine(int idWindTurbine);
}
