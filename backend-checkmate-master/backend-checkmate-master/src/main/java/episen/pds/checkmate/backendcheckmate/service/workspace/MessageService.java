package episen.pds.checkmate.backendcheckmate.service.workspace;

import episen.pds.checkmate.backendcheckmate.dao.workspace.MessageRepository;
import episen.pds.checkmate.backendcheckmate.model.workspace.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;


@Service
public class MessageService {
    private final MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public void sendMessage(Message message) {
        messageRepository.sendMessage(message.getSender(),message.getReceiver(),message.getContent(), Timestamp.valueOf(message.getTime()));
    }

    public List<Message> getMessages(Integer sender, Integer receiver){
        return messageRepository.getMessages(sender,receiver);
    }
}
