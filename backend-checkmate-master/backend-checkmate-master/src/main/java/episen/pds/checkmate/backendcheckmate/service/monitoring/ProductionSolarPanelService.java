package episen.pds.checkmate.backendcheckmate.service.monitoring;

import episen.pds.checkmate.backendcheckmate.dao.monitoring.ProductionSolarPanelRepository;
import episen.pds.checkmate.backendcheckmate.model.monitoring.ProductionSolarPanel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductionSolarPanelService {
    private final ProductionSolarPanelRepository productionSolarPanelRepository;

    @Autowired
    public ProductionSolarPanelService(ProductionSolarPanelRepository productionSolarPanelRepository) {
        this.productionSolarPanelRepository = productionSolarPanelRepository;
    }


    public void addProductionSolarPanel(ProductionSolarPanel productionSolarPanel) {
        productionSolarPanelRepository.save(productionSolarPanel);
    }

    public List<ProductionSolarPanel> selectAllProductionSolarPanel() {
        return productionSolarPanelRepository.findAll();
    }

    public List<ProductionSolarPanel> findByIdSolarPanel(int idBuilding) {
        return productionSolarPanelRepository.findByIdSolarPanel(idBuilding);
    }

    public Optional<ProductionSolarPanel> getProductionSolarPanelById(Integer id) {
        return productionSolarPanelRepository.findById(id);
    }

    public void updateProductionSolarPanel(ProductionSolarPanel productionSolarPanel) {
        productionSolarPanelRepository.save(productionSolarPanel);
    }

    public void deleteProductionSolarPanel(Integer id) {
        productionSolarPanelRepository.deleteById(id);
    }


}
