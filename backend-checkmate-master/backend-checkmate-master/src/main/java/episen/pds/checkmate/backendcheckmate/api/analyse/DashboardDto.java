package episen.pds.checkmate.backendcheckmate.api.analyse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DashboardDto {
    private Integer id;
    private String title;
    private Float value;
    private Boolean autoDisplay = true;

    public DashboardDto(String title, Long value) {
        this.title = title;
        this.value = Float.valueOf(value);
    }
}

