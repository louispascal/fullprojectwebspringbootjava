package episen.pds.checkmate.backendcheckmate.service.analyse;

import episen.pds.checkmate.backendcheckmate.api.analyse.CompGraph;
import episen.pds.checkmate.backendcheckmate.api.analyse.DashboardDto;
import episen.pds.checkmate.backendcheckmate.dao.analyse.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DashboardService {

    @Autowired
    private IBuildingAnalyzeRepository buildingAnalyzeRepository;
    @Autowired
    private IConsumptionAnalyzeRepository consumptionAnalyzeRepository;
    @Autowired
    private IDistrictAnalyzeRepository districtAnalyzeRepository;
    @Autowired
    private IEnergySourceRepository energySourceRepository;


    @Autowired
    private IEquipmentAnalyzeRepository equipmentRepository;
    @Autowired
    private IHomeAnalyzeRepository homeRepository;
    @Autowired
    private IHomeEquipmentRepository homeEquipmentRepository;
    @Autowired
    private IProductionAnalyzeRepository productionRepository;


    public DashboardService(EntityManager entityManager, IBuildingAnalyzeRepository buildingAnalyzeRepository, IConsumptionAnalyzeRepository consumptionAnalyzeRepository, IDistrictAnalyzeRepository districtAnalyzeRepository, IEnergySourceRepository energySourceRepository, IEquipmentAnalyzeRepository equipmentRepository, IHomeAnalyzeRepository homeRepository, IHomeEquipmentRepository homeEquipmentRepository, IProductionAnalyzeRepository productionRepository) {
        this.entityManager=entityManager;
        this.buildingAnalyzeRepository = buildingAnalyzeRepository;
        this.consumptionAnalyzeRepository = consumptionAnalyzeRepository;
        this.districtAnalyzeRepository = districtAnalyzeRepository;
        this.energySourceRepository = energySourceRepository;
        this.equipmentRepository = equipmentRepository;
        this.homeRepository = homeRepository;
        this.homeEquipmentRepository = homeEquipmentRepository;
        this.productionRepository = productionRepository;
    }
    public List<DashboardDto> getStatistics() {
        List<DashboardDto> list = new ArrayList<>();

        list.add(new DashboardDto(0, "Nombre de quartiers", (float) districtAnalyzeRepository.count(), true));
        list.add(new DashboardDto(1, "Nombre de bâtiments", (float) buildingAnalyzeRepository.count(), true));
        list.add(new DashboardDto(3, "Nombre d'appartements", (float) homeRepository.count(), true));
        list.add(new DashboardDto(4, "Nombre d'équipements", (float) homeEquipmentRepository.count(), true));
        list.add(new DashboardDto(5, "Nombre de sources d'énergie", (float) energySourceRepository.count(), true));
        list.add(new DashboardDto(6, "Nombre de panneaux solaires", (float) energySourceRepository.countByEnergySourceTypeName("solar"), true));
        list.add(new DashboardDto(7, "Nombre d'éoliennes", (float) energySourceRepository.countByEnergySourceTypeName("wind"), true));

        list.add(new DashboardDto(8, "Consommation moyenne par quartier", consumptionAnalyzeRepository.sumOfConsumption() / (float) districtAnalyzeRepository.count(), true));
        list.add(new DashboardDto(9, "Consommation moyenne par bâtiment", consumptionAnalyzeRepository.sumOfConsumption() / (float) buildingAnalyzeRepository.count(), true));
        list.add(new DashboardDto(10, "Consommation moyenne par appartement", consumptionAnalyzeRepository.sumOfConsumption() / (float) homeRepository.count(), true));
        list.add(new DashboardDto(11, "Consommation moyenne par équipement", consumptionAnalyzeRepository.sumOfConsumption() / (float) homeEquipmentRepository.count(), true));

        list.add(new DashboardDto(12, "Total de la consommation", (float) consumptionAnalyzeRepository.sumOfConsumption(), false));
        list.add(new DashboardDto(13, "Total de la production", (float) productionRepository.sumOfProdcution(), false));
        list.add(new DashboardDto(14, "Ratio de l'efficacité", ((float) productionRepository.sumOfProdcution() / consumptionAnalyzeRepository.sumOfConsumption()), false));

        return list;
    }


    public List<CompGraph> getProductionAndConsumptionPerDistrict() {
        List<Object[]> cons = entityManager.createNativeQuery("select d.name, sum(c.value) " +
                "from analyze_schema.homeequipment he " +
                "         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "         inner join analyze_schema.districtanalyze d on d.id = b.district_id " +
                "group by d.name").getResultList();
        var resultCons = new ArrayList<CompGraph>();
        cons.forEach(item -> resultCons.add(new CompGraph((String) item[0], ((BigInteger) item[1]).longValue(), null)));

        List<Object[]> list = entityManager.createNativeQuery("select  da.name as title,sum(distinct pa.value)  as value1 " +
                "from  analyze_schema.homeequipment he " +
                "inner join analyze_schema.homeanalyze ha on ha.id = he.home_id " +
                "inner join analyze_schema.buildinganalyze ba on ba.id = ha.building_id " +
                "inner join analyze_schema.districtanalyze da on da.id = ba.district_id " +
                "inner join analyze_schema.energysource es on da.id = es.district_id " +
                "inner join (select  p.energysource_id as energysource_id, sum(p.value) as value from analyze_schema.productionanalyze p group by p.energysource_id) pa on pa.energysource_id = es.id " +
                "group by da.name").getResultList();
        var result = new ArrayList<CompGraph>();
        list.forEach(item -> result.add(new CompGraph((String) item[0], ((BigDecimal) item[1]).longValue(), resultCons.stream().filter(p -> p.getTitle().equals((String) item[0])).toList().get(0).getValue1().longValue())));
        return result;
    }

    @PersistenceContext
    EntityManager entityManager;

    public List<CompGraph> getProductionAndConsumptionPerBuilding(String district) {

        List<Object[]> cons = entityManager.createNativeQuery("select b.name, sum(c.value) " +
                "from analyze_schema.homeequipment he " +
                "         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "         inner join analyze_schema.districtanalyze d on d.id = b.district_id where d.name = :district " +
                "group by b.name").setParameter("district", district).getResultList();
        var resultCons = new ArrayList<CompGraph>();
        cons.forEach(item -> resultCons.add(new CompGraph((String) item[0], ((BigInteger) item[1]).longValue(), null)));

        int buildingCount = ((BigInteger) entityManager.createNativeQuery("select  count(distinct b.id) " +
                "from analyze_schema.homeequipment he " +
                "         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "         inner join analyze_schema.districtanalyze d on d.id = b.district_id where d.name = :district").setParameter("district", district).getSingleResult()).intValue();


        List<Object[]> list = entityManager.createNativeQuery("select  ba.name as title, sum(distinct pa.value) / :count as value1   " +
                "from  analyze_schema.homeequipment he " +
                "inner join analyze_schema.homeanalyze ha on ha.id = he.home_id " +
                "inner join analyze_schema.buildinganalyze ba on ba.id = ha.building_id " +
                "inner join analyze_schema.districtanalyze da on da.id = ba.district_id " +
                "inner join analyze_schema.energysource es on da.id = es.district_id " +
                "inner join (select  p.energysource_id as energysource_id, sum(p.value) as value from analyze_schema.productionanalyze p group by p.energysource_id) pa on pa.energysource_id = es.id " +
                "where da.name=:district " +
                "group by ba.name").setParameter("district", district).setParameter("count", buildingCount).getResultList();
        var result = new ArrayList<CompGraph>();
        list.forEach(item -> result.add(new CompGraph((String) item[0], ((BigDecimal) item[1]).longValue(), resultCons.stream().filter(p -> p.getTitle().equals((String) item[0])).toList().get(0).getValue1().longValue())));
        return result;
    }

    public List<CompGraph> getProductionAndConsumptionPerHome(String building) {
        List<Object[]> cons = entityManager.createNativeQuery("select h.name, sum(c.value) " +
                "from analyze_schema.homeequipment he " +
                "         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "         inner join analyze_schema.districtanalyze d on d.id = b.district_id where  b.name = :building " +
                "group by h.name").setParameter("building", building).getResultList();
        var resultCons = new ArrayList<CompGraph>();
        cons.forEach(item -> resultCons.add(new CompGraph((String) item[0], ((BigInteger) item[1]).longValue(), null)));

        int districtId = ((Integer) entityManager.createNativeQuery("select  d.id " +
                "                from analyze_schema.homeequipment he " +
                "                         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "                         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "                         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "                         inner join analyze_schema.districtanalyze d on d.id = b.district_id where b.name = :building").setParameter("building", building).getSingleResult()).intValue();

        int buildingCount = ((BigInteger) entityManager.createNativeQuery("select  count(distinct b.id) " +
                "                from analyze_schema.homeequipment he " +
                "                         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "                         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "                         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "                         inner join analyze_schema.districtanalyze d on d.id = b.district_id where  d.id = :districtId").setParameter("districtId", districtId).getSingleResult()).intValue();

        int homeCount = ((BigInteger) entityManager.createNativeQuery("select  count(distinct h.id) " +
                "from analyze_schema.homeequipment he " +
                "         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "         inner join analyze_schema.districtanalyze d on d.id = b.district_id where b.name = :building").setParameter("building", building).getSingleResult()).intValue();

        List<Object[]> list = entityManager.createNativeQuery("select  ha.name as title, sum(distinct pa.value) / :count as value1   " +
                "from  analyze_schema.homeequipment he " +
                "inner join analyze_schema.homeanalyze ha on ha.id = he.home_id " +
                "inner join analyze_schema.buildinganalyze ba on ba.id = ha.building_id " +
                "inner join analyze_schema.districtanalyze da on da.id = ba.district_id " +
                "inner join analyze_schema.energysource es on da.id = es.district_id " +
                "inner join (select  p.energysource_id, sum(p.value) as value from analyze_schema.productionanalyze p group by p.energysource_id) pa on pa.energysource_id = es.id " +
                "where ba.name=:building " +
                "group by ha.name").setParameter("building", building).setParameter("count", homeCount * buildingCount).getResultList();
        var result = new ArrayList<CompGraph>();
        list.forEach(item -> result.add(new CompGraph((String) item[0], ((BigDecimal) item[1]).longValue(), resultCons.stream().filter(p -> p.getTitle().equals((String) item[0])).toList().get(0).getValue1().longValue())));
        return result;
    }

    public List<CompGraph> getProductionAndConsumptionPerEquipment(String home) {
        List<Object[]> cons = entityManager.createNativeQuery("select ea.name, sum(c.value) " +
                "from analyze_schema.homeequipment he " +
                "         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "         inner join analyze_schema.districtanalyze d on d.id = b.district_id " +
                "inner join analyze_schema.equipmentanalyze ea on ea.id = he.equipment_id where  h.name = :home " +
                "group by ea.name").setParameter("home", home).getResultList();
        var resultCons = new ArrayList<CompGraph>();
        cons.forEach(item -> resultCons.add(new CompGraph((String) item[0], ((BigInteger) item[1]).longValue(), null)));

        int districtId = ((Integer) entityManager.createNativeQuery("select  d.id " +
                "                from analyze_schema.homeequipment he " +
                "                         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "                         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "                         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "                         inner join analyze_schema.districtanalyze d on d.id = b.district_id where h.name = :home").setParameter("home", home).getSingleResult()).intValue();
        int buildingId = ((Integer) entityManager.createNativeQuery("select  b.id " +
                "                from analyze_schema.homeequipment he " +
                "                         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "                         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "                         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "                         inner join analyze_schema.districtanalyze d on d.id = b.district_id where h.name = :home").setParameter("home", home).getSingleResult()).intValue();

        int buildingCount = ((BigInteger) entityManager.createNativeQuery("select  count(distinct b.id) " +
                "                from analyze_schema.homeequipment he " +
                "                         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "                         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "                         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "                         inner join analyze_schema.districtanalyze d on d.id = b.district_id where  d.id=:districtId").setParameter("districtId", districtId).getSingleResult()).intValue();

        int homeCount = ((BigInteger) entityManager.createNativeQuery("select  count(distinct h.id) " +
                "from analyze_schema.homeequipment he " +
                "         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "         inner join analyze_schema.districtanalyze d on d.id = b.district_id where  b.id=:buildingId").setParameter("buildingId", buildingId).getSingleResult()).intValue();


        int equipCount = ((BigInteger) entityManager.createNativeQuery("select  count(distinct he.id) " +
                "from analyze_schema.homeequipment he " +
                "         inner join analyze_schema.consumptionanalyze c on he.id = c.homeequipment_id " +
                "         inner join analyze_schema.homeanalyze h on h.id = he.home_id " +
                "         inner join analyze_schema.buildinganalyze b on h.building_id = b.id " +
                "         inner join analyze_schema.districtanalyze d on d.id = b.district_id where h.name=:home").setParameter("home", home).getSingleResult()).intValue();

        List<Object[]> list = entityManager.createNativeQuery("select  ea.name as title, sum(distinct pa.value) / :count as value1  " +
                "from  analyze_schema.homeequipment he  " +
                "inner join analyze_schema.homeanalyze ha on ha.id = he.home_id " +
                "inner join analyze_schema.buildinganalyze ba on ba.id = ha.building_id " +
                "inner join analyze_schema.districtanalyze da on da.id = ba.district_id " +
                "inner join analyze_schema.energysource es on da.id = es.district_id " +
                "inner join (select  p.energysource_id as energysource_id, sum(p.value) as value from analyze_schema.productionanalyze p group by p.energysource_id) pa on pa.energysource_id = es.id " +
                "inner join analyze_schema.equipmentanalyze ea on ea.id = he.equipment_id " +
                "where ha.name=:home " +
                "group by ea.name").setParameter("home", home).setParameter("count", equipCount * buildingCount * homeCount).getResultList();
        var result = new ArrayList<CompGraph>();
        list.forEach(item -> result.add(new CompGraph((String) item[0], ((BigDecimal) item[1]).longValue(),  resultCons.stream().filter(p -> p.getTitle().equals((String) item[0])).toList().get(0).getValue1().longValue())));
        return result;
    }

    public List<CompGraph> getDistrictHistory(String name) {
        List<Object[]> list = entityManager.createNativeQuery("select make_date(CAST(date_part('year', paa.createdon) as int), CAST(date_part('month', paa.createdon) as int), 01) as date, sum(distinct pa.value)  as value1 ,sum(distinct ca.value) as value2  " +
                "from (select c.homeequipment_id as homeequipment_id,sum(c.value) as value from analyze_schema.consumptionanalyze c group by c.homeequipment_id) ca " +
                "inner join analyze_schema.homeequipment he on ca.homeequipment_id = he.id " +
                "inner join analyze_schema.homeanalyze ha on ha.id = he.home_id " +
                "inner join analyze_schema.buildinganalyze ba on ba.id = ha.building_id " +
                "inner join analyze_schema.districtanalyze da on da.id = ba.district_id " +
                "inner join analyze_schema.energysource es on da.id = es.district_id " +
                "inner join (select  p.energysource_id as energysource_id,sum(p.value) as value from analyze_schema.productionanalyze p group by p.energysource_id) pa on pa.energysource_id = es.id " +
                "inner join analyze_schema.productionanalyze paa on paa.energysource_id = pa.energysource_id " +
                "inner join analyze_schema.equipmentanalyze ea on ea.id = he.equipment_id " +
                "where da.name=:district " +
                "group by date order by date").setParameter("district", name).getResultList();
        var result = new ArrayList<CompGraph>();
        list.forEach(item -> result.add(new CompGraph((Date) item[0], ((BigDecimal) item[1]).longValue(), ((BigDecimal) item[2]).longValue())));
        return result;
    }

    public List<CompGraph> getBuildingHistory(String name) {
        List<Object[]> list = entityManager.createNativeQuery("select  make_date(CAST(date_part('year', paa.createdon) as int), CAST(date_part('month', paa.createdon) as int), 01) as date, sum(distinct pa.value)  as value1 ,sum(distinct ca.value) as value2  " +
                "from (select c.homeequipment_id as homeequipment_id,sum(c.value) as value from analyze_schema.consumptionanalyze c group by c.homeequipment_id) ca " +
                "inner join analyze_schema.homeequipment he on ca.homeequipment_id = he.id " +
                "inner join analyze_schema.homeanalyze ha on ha.id = he.home_id " +
                "inner join analyze_schema.buildinganalyze ba on ba.id = ha.building_id " +
                "inner join analyze_schema.districtanalyze da on da.id = ba.district_id " +
                "inner join analyze_schema.energysource es on da.id = es.district_id " +
                "inner join (select  p.energysource_id as energysource_id, sum(p.value) as value from analyze_schema.productionanalyze p group by p.energysource_id) pa on pa.energysource_id = es.id " +
                "inner join analyze_schema.productionanalyze paa on paa.energysource_id = pa.energysource_id " +
                "inner join analyze_schema.equipmentanalyze ea on ea.id = he.equipment_id " +
                "where ba.name=:building " +
                "group by date order by date").setParameter("building", name).getResultList();
        var result = new ArrayList<CompGraph>();
        list.forEach(item -> result.add(new CompGraph((Date) item[0], ((BigDecimal) item[1]).longValue(), ((BigDecimal) item[2]).longValue())));
        return result;
    }

    public List<CompGraph> getHomeHistory(String name) {
        List<Object[]> list = entityManager.createNativeQuery("select  make_date(CAST(date_part('year', pa.createdon) as int), CAST(date_part('month', pa.createdon) as int), 01) as date, sum(distinct pa.value)  as value1 ,sum(distinct ca.value) as value2  " +
                "from analyze_schema.consumptionanalyze ca " +
                "inner join analyze_schema.homeequipment he on ca.homeequipment_id = he.id " +
                "inner join analyze_schema.homeanalyze ha on ha.id = he.home_id " +
                "inner join analyze_schema.buildinganalyze ba on ba.id = ha.building_id " +
                "inner join analyze_schema.districtanalyze da on da.id = ba.district_id " +
                "inner join analyze_schema.energysource es on da.id = es.district_id " +
                "inner join analyze_schema.productionanalyze pa on pa.energysource_id = es.id " +
                "inner join analyze_schema.equipmentanalyze ea on ea.id = he.equipment_id where ha.name=:home " +
                "group by date order by date").setParameter("home", name).getResultList();
        var result = new ArrayList<CompGraph>();
        list.forEach(item -> result.add(new CompGraph((Date) item[0], ((BigDecimal) item[1]).longValue(), ((BigDecimal) item[2]).longValue())));
        return result;
    }

    public List<CompGraph> getEquipmentHistory(String name) {
        List<Object[]> list = entityManager.createNativeQuery("select  make_date(CAST(date_part('year', pa.createdon) as int), CAST(date_part('month', pa.createdon) as int), 01) as date, sum(distinct pa.value)  as value1 ,sum(distinct ca.value) as value2 " +
                "from analyze_schema.consumptionanalyze ca " +
                "inner join analyze_schema.homeequipment he on ca.homeequipment_id = he.id " +
                "inner join analyze_schema.homeanalyze ha on ha.id = he.home_id " +
                "inner join analyze_schema.buildinganalyze ba on ba.id = ha.building_id " +
                "inner join analyze_schema.districtanalyze da on da.id = ba.district_id " +
                "inner join analyze_schema.energysource es on da.id = es.district_id " +
                "inner join analyze_schema.productionanalyze pa on pa.energysource_id = es.id " +
                "inner join analyze_schema.equipmentanalyze ea on ea.id = he.equipment_id where ea.name=:equipment " +
                "group by date order by date").setParameter("equipment", name).getResultList();
        var result = new ArrayList<CompGraph>();
        list.forEach(item -> result.add(new CompGraph((Date) item[0], ((BigDecimal) item[1]).longValue(), ((BigDecimal) item[2]).longValue())));
        return result;
    }


}
