package episen.pds.checkmate.backendcheckmate.model.mixenergy;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "production_of_site", schema = "energy")
@Data
public class Production_of_site {
    @Id
    private Integer id_production_of_site;
    private Integer instantaneous_power;
    private String sampling_date ;
    private Integer id_site;
    private Integer current_capacity;

}
