package episen.pds.checkmate.backendcheckmate.api.alerting;


import episen.pds.checkmate.backendcheckmate.model.alerting.Alerte;
import episen.pds.checkmate.backendcheckmate.service.alerting.AlerteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "alerte")
public class AlerteController {

    private final AlerteService alerteService;

    @Autowired
    public AlerteController(AlerteService alerteService) {
        this.alerteService = alerteService;
    }

    @PostMapping("/create")
    public void addAlerte(@RequestBody Alerte alerte) {
        alerteService.addAlerte(alerte);
    }

    @GetMapping("/read")
    public List<Alerte> getAlertes() {return alerteService.getAlertes();}

    @GetMapping(path = "/readbyid/{id}")
    public Optional<Alerte> getClient(@PathVariable Integer id) {return alerteService.getAlerteById(id);}

}