package episen.pds.checkmate.backendcheckmate.model.mixenergy;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "site_of_production", schema = "energy")
@Data
public class Site_of_Production {
    @Id
    private Integer id_site;
    private Integer max_capacity;
    private String site_type;

}