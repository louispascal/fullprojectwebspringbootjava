package episen.pds.checkmate.backendcheckmate.model.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "dwp")
public class Space {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_space;
    private Integer position_in_area;
    private Integer space_number;
    private Integer id_area;
    private Integer id_type_space;
    @Column(name = "type")
    private String type;

    public Space(Integer space_number, Integer id_area, Integer id_type_space, String type) {
        this.space_number = space_number;
        this.id_area = id_area;
        this.id_type_space = id_type_space;
        this.type = type;
    }
}
