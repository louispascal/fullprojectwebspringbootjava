package episen.pds.checkmate.backendcheckmate.dao.scope8;


import episen.pds.checkmate.backendcheckmate.model.scope8.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EquipmentRepository extends JpaRepository<Equipment, Integer> {

     @Query(value = "select name from dwp.equipment inner join dwp.consumptiondw on equipment.id_equipment=consumptiondw.idcons_equipment inner join dwp.reservation on reservation.id_reservation= ?1", nativeQuery =true)
        List<Object[]> equipmentsUsedby(Integer id);




}
/*public interface EquipmentRepository extends JpaRepository<Equipment, Integer> {
    //Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    @Query(value = "select * from Equipment where idconsumer= ?1 "  ,nativeQuery = true)

    List<Equipment> consKW(Integer idconsumer) ;


    @Query(value="SELECT finishtime FROM Equipment  WHERE idconsumer =?1 and finishtime > ?2 ",nativeQuery = true)
    List<Timestamp> finishTime(Integer idconusmer,Timestamp currentTime);
    @Modifying
    @Query(value="insert into equipment values (2,:idconsumer,generate_series(1,10),'chauffage',:time,:finishtime)",nativeQuery = true)
    @Transactional
    void voidInsertData(@Param("idconsumer")Integer idconusmer,@Param("time") Timestamp time,@Param("finishtime") Timestamp finishtime);


}

 */
