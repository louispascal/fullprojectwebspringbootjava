package episen.pds.checkmate.backendcheckmate.thread;

import com.fasterxml.jackson.annotation.JsonProperty;
import episen.pds.checkmate.backendcheckmate.model.mixenergy.Site_of_Production;
import org.springframework.boot.jackson.JsonComponent;

import java.util.ArrayList;
import java.util.List;

@JsonComponent
public class SiteCapacity {

    private Integer current_capacity;
    private Site_of_Production site;

    public Integer getCurrent_capacity() {
        return current_capacity;
    }

    public void setCurrent_capacity(Integer current_capacity) {
        this.current_capacity = current_capacity;
    }

    public Site_of_Production getSite() {
        return site;
    }

    public void setSite(Site_of_Production site) {
        this.site = site;
    }
}

