package episen.pds.checkmate.backendcheckmate.model.workspace;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "dwp")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer sender;
    private Integer receiver;
    private String time;
    private String content;

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                "sender=" + sender + '\'' +
                ", receiver='" + receiver + '\'' +
                ", time='" + time + '\'' +
                ", content=" + content +
                '}';
    }
}
