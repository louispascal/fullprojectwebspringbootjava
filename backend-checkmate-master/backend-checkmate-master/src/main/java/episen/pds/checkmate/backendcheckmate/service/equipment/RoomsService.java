package episen.pds.checkmate.backendcheckmate.service.equipment;

import episen.pds.checkmate.backendcheckmate.model.equipment.Rooms;
import episen.pds.checkmate.backendcheckmate.dao.equipment.RoomsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class RoomsService {
    private Integer id_user;
    private final RoomsRepository roomsRepository;

    public RoomsService(RoomsRepository roomsRepository) {
        this.roomsRepository = roomsRepository;
    }

    public Iterable<Rooms> selectRoomByCitizen(Integer id_citizen){
        Iterable<Rooms> rooms = roomsRepository.getRoomByUserId(id_citizen);
        return rooms;
    }
}
