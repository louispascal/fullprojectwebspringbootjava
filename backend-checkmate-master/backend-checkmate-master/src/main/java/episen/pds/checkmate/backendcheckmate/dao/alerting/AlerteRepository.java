package episen.pds.checkmate.backendcheckmate.dao.alerting;

import episen.pds.checkmate.backendcheckmate.model.alerting.Alerte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlerteRepository extends JpaRepository<Alerte,Integer> {

}
