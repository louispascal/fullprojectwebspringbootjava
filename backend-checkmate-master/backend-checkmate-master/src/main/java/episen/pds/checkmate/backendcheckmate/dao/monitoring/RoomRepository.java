package episen.pds.checkmate.backendcheckmate.dao.monitoring;

import episen.pds.checkmate.backendcheckmate.model.equipment.Equipments;
import episen.pds.checkmate.backendcheckmate.model.monitoring.Room;
import java.math.BigDecimal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author Ines
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {

    @Query(value = "select * from energy.room r INNER JOIN energy.home h on r.id_home = h.id_home WHERE h.id_home = ?1 ORDER BY r.name ASC", nativeQuery = true)
    List<Room> findByIdHome(int idHome);
}
