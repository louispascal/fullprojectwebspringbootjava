package episen.pds.checkmate.backendcheckmate.dao.monitoring;

import episen.pds.checkmate.backendcheckmate.model.monitoring.Consumption;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ines
 */
@Repository
public interface ConsumptionRepository extends JpaRepository<Consumption, Integer> {
    @Query(value = "SELECT * from energy.consumption c WHERE c.id_equipment = ?1 ORDER BY c.sampling_date DESC", nativeQuery = true)
    List<Consumption> findByIdEquipment(int idEquipment);
    
    @Query(value = "SELECT * from energy.consumption c WHERE c.id_equipment = ?1 AND c.sampling_date BETWEEN ?2 AND ?3 ORDER BY c.sampling_date DESC", nativeQuery = true)
    List<Consumption> findByIdEquipmentDate(int idEquipment, Date dateDegin, Date dateEnd);

    @Query(value = "SELECT * FROM energy.consumption WHERE id_equipment = ?1 AND sampling_date = (SELECT Max(sampling_date) FROM energy.consumption WHERE id_equipment =  ?1)", nativeQuery = true)
    Consumption findByMaxDateIdEquipment(int idEquipment);
}
