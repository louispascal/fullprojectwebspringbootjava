package episen.pds.checkmate.backendcheckmate.model.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Coordinate {
    Integer row;
    Integer column;
}
