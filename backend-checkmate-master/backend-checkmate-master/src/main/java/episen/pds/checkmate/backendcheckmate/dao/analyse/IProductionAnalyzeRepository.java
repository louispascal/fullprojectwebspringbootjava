package episen.pds.checkmate.backendcheckmate.dao.analyse;

import episen.pds.checkmate.backendcheckmate.model.analyse.ProductionAnalyze;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IProductionAnalyzeRepository extends JpaRepository<ProductionAnalyze, Integer> {
    @Query("select sum(pa.value)  from ProductionAnalyze pa")
    Integer sumOfProdcution();


}
