package episen.pds.checkmate.backendcheckmate.service.common;

import episen.pds.checkmate.backendcheckmate.dao.common.CitizenRepository;
import episen.pds.checkmate.backendcheckmate.dao.monitoring.HomeRepository;
import episen.pds.checkmate.backendcheckmate.model.common.Citizen;
import episen.pds.checkmate.backendcheckmate.model.common.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CitizenService {

    private final CitizenRepository citizenRepository;
    private final HomeRepository homeRepository;

    @Autowired
    public CitizenService(CitizenRepository citizenRepository, HomeRepository homeRepository) {
        this.citizenRepository = citizenRepository;
        this.homeRepository = homeRepository;
    }

    public void addCitizen(Citizen citizen) {
        citizenRepository.save(citizen);
    }

    public List<Citizen> selectAllCitizen() {
        return citizenRepository.findAll();
    }

    public List<Citizen> findByIdRole(int idRole) {
        List<Citizen> citizens = citizenRepository.findByIdRole(idRole);
        return citizens;
    }

    public Optional<Citizen> getCitizenById(Integer id) {
        return citizenRepository.findById(id);
    }

    public void updateCitizen(Citizen citizen) {
        citizenRepository.save(citizen);
    }

    public void deleteCitizen(Integer id) {
        citizenRepository.deleteById(id);
    }

    public Citizen findByMail(String mail) {
        Citizen citizen = citizenRepository.findByMail(mail);
        citizen.setIdHome(homeRepository.findIdHomeByIdCitizen(citizen.getIdCitizen()));
        return citizen;
    }


}
