package episen.pds.checkmate.backendcheckmate.dao.common;

import episen.pds.checkmate.backendcheckmate.model.common.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
}
