package episen.pds.checkmate.backendcheckmate.service.scope8;

import episen.pds.checkmate.backendcheckmate.dao.scope8.ReservationRepository;
import episen.pds.checkmate.backendcheckmate.model.map.Space;
import episen.pds.checkmate.backendcheckmate.model.scope8.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {
    private final ReservationRepository ReservationRepository;

    @Autowired
    public ReservationService(ReservationRepository ReservationRepository) {
        this.ReservationRepository = ReservationRepository;
    }


    public void addReservation(Reservation reservation) {
        ReservationRepository.addReservation(reservation.getId_employee(),reservation.getId_spacepk(),Timestamp.valueOf(reservation.getDate()),Timestamp.valueOf(reservation.getEnd_date()));
    }

    public List<Reservation> getReservationByEmployee(Integer id){
        return ReservationRepository.getReservationByEmployee(id);
    }

    public List<Reservation> getMeetingsByEmployee(Integer id){
        return ReservationRepository.getMeetingsByEmployee(id);
    }

    public String getReservationAuthor(Integer id){
        List<String> l = ReservationRepository.getReservationAuthor(id);
        return l.get(0).split(",")[0] + " " + l.get(0).split(",")[1];
    }

    public Integer getMeetingParticipants(Integer id){
        return ReservationRepository.getMeetingParticipants(id)+1;
    }

    public List<Reservation> getOwnMeetings(Integer id){
        return ReservationRepository.getOwnMeetings(id);
    }

    public void invite(Integer id_reservation, Integer id_employee){
        ReservationRepository.invite(id_reservation,id_employee);
    }

    public Integer getSpaceFloor(Integer id){
        return ReservationRepository.getSpaceFloor(id);
    }

    public List<Reservation> selectAllReservation() {
        return ReservationRepository.findAll();
    }
    public List<String[]> selectJoin(){

        return ReservationRepository.allReservationsJoint();}

    public List<Reservation> selectAlltime() {

        Timestamp lt = new Timestamp(System.currentTimeMillis());
        return ReservationRepository.selectAllcurents(lt,lt);
    }


    public Optional<Reservation> getReservationById(Integer id) {
        return ReservationRepository.findById(id);
    }

    public void updateReservation(Reservation Reservation) {
        ReservationRepository.save(Reservation);
    }

    public void deleteReservation(Integer id) {
        ReservationRepository.deleteById(id);
    }

    public String getMeetingSubject(Integer id) {
        return ReservationRepository.getMeetingSubject(id);
    }

    public void subjectUpdate(String s, String r) {
        ReservationRepository.deleteSubject(Integer.valueOf(r));
        ReservationRepository.subjectUpdate(s,Integer.valueOf(r));
    }

}
