package episen.pds.checkmate.backendcheckmate.model.monitoring;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import episen.pds.checkmate.backendcheckmate.model.common.Citizen;
import episen.pds.checkmate.backendcheckmate.model.map.Building;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "home", schema = "energy")
public class Home {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_home")
    private Integer idHome;

    private String type;

    @JoinColumn(name = "id_citizen", referencedColumnName = "id_citizen")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Citizen citizen;

    @JoinColumn(name = "id_building", referencedColumnName = "id_building")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Building building;
}