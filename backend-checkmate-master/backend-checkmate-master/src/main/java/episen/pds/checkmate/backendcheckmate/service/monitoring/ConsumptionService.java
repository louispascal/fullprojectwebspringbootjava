package episen.pds.checkmate.backendcheckmate.service.monitoring;

import episen.pds.checkmate.backendcheckmate.dao.monitoring.ConsumptionRepository;
import episen.pds.checkmate.backendcheckmate.model.monitoring.Consumption;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ConsumptionService {
    private final ConsumptionRepository consumptionRepository;

    @Autowired
    public ConsumptionService(ConsumptionRepository consumptionRepository) {
        this.consumptionRepository = consumptionRepository;
    }


    public void addConsumption(Consumption consumption) {
        consumptionRepository.save(consumption);
    }

    public List<Consumption> selectAllConsumption() {
        return consumptionRepository.findAll();
    }

    public List<Consumption> findByIdEquipment(int idEquipment) {
        return consumptionRepository.findByIdEquipment(idEquipment);
    }

    public List<Consumption> findByIdEquipmentDate(int idEquipment, Date dateDegin, Date dateEnd) {
        return consumptionRepository.findByIdEquipmentDate(idEquipment, dateDegin, dateEnd);
    }

    public Optional<Consumption> getConsumptionById(Integer id) {
        return consumptionRepository.findById(id);
    }
    
    public Consumption findByMaxDateIdEquipment(Integer id) {
        return consumptionRepository.findByMaxDateIdEquipment(id);
    }

    public void updateConsumption(Consumption consumption) {
        consumptionRepository.save(consumption);
    }

    public void deleteConsumption(Integer id) {
        consumptionRepository.deleteById(id);
    }


}
