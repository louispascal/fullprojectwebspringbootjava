package episen.pds.checkmate.backendcheckmate.dao.analyse;

import episen.pds.checkmate.backendcheckmate.model.analyse.BuildingAnalyze;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IBuildingAnalyzeRepository extends JpaRepository<BuildingAnalyze, Integer> {

}
