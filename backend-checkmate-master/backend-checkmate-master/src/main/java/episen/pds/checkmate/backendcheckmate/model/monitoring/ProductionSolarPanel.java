/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package episen.pds.checkmate.backendcheckmate.model.monitoring;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Ines
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "production_solar_panel", schema = "energy")
public class ProductionSolarPanel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_production_solar_panel")
    private Integer idProductionSolarPanel;
    
    @Column(name = "instantaneous_power")
    private Double instantaneousPower;

    @Column(name = "sampling_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date samplingDate;

    @JoinColumn(name = "id_solar_panel", referencedColumnName = "id_solar_panel")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SolarPanel solarPanel;

}
