package episen.pds.checkmate.backendcheckmate.model.equipment;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import episen.pds.checkmate.backendcheckmate.model.monitoring.EquipmentEnergy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "fridge", schema = "energy")
public class Fridge implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_fridge")
    private Integer idFridge;
    
    private Double temperature;
    
    @JoinColumn(name = "id_equipment", referencedColumnName = "id_equipment")
    @ManyToOne(fetch = FetchType.LAZY)
    private EquipmentEnergy equipment;

    @Override
    public String toString() {
        return "Fridge{"
                + "id_fridge=" + idFridge
                + ", temperature='" + temperature + '\''
                + ", equipment='" + equipment + '\''
                + '}';
    }
}
