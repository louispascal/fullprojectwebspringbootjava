package episen.pds.checkmate.backendcheckmate.service.mixenergy;

import episen.pds.checkmate.backendcheckmate.dao.mixenergy.AverageRepository;
import episen.pds.checkmate.backendcheckmate.dao.mixenergy.MixRepository;
import episen.pds.checkmate.backendcheckmate.model.mixenergy.MixEnergy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Slf4j
@Service
public class MixService {
    private final MixRepository mixRepository;
    private final AverageRepository averageRepository;

    @Autowired
    public MixService(MixRepository mixRepository, AverageRepository averageRepository) {
        this.mixRepository = mixRepository;
        this.averageRepository = averageRepository;
    }
    public Integer getSolarProduction() {
        return mixRepository.findSolarEnergy();
    }
    public Integer getWindProduction() {
        return mixRepository.findWindEnergy();
    }
    public Integer getHydraulicProduction() {
        return mixRepository.findHydraulicEnergy();
    }
    public String getTimeOfProduction() {
        return mixRepository.findTimeProd();
    }
    public MixEnergy getTotalEnergy() {
        Integer solarprod = getSolarProduction();
        Integer hydrauprod = getHydraulicProduction();
        Integer windprod = getWindProduction();
        String timeOfProd = getTimeOfProduction();
        MixEnergy mix = new MixEnergy();
        if (hydrauprod == null) {
            mix.setHydraulic_power(0);
        } else {
            mix.setHydraulic_power(hydrauprod);
        }
        if (solarprod == null) {
            mix.setSolar_power(0);
        } else {
            mix.setSolar_power(solarprod);
        }
        if (windprod == null) {
            mix.setWind_power(0);
        } else {
            mix.setWind_power(windprod);
        }
        if (timeOfProd == null) {
            mix.setHydraulic_power(0);
        } else {
            mix.setSampling_date(timeOfProd);
        }
        return mix;
    }
    public HashMap<String, List<Float>> getEconomicData() {
        List<Float> solarCostList = Arrays.asList(1.5f , 1.35f , 1.3f , 1.2f , 1f , 2.4f , 2.2f , 1.9f , 1.7f , 1.7f , 1.6f , 1.5f );
        List<Float> windCostList = Arrays.asList(1.8f , 1.55f , 1.45f , 1.95f , 2.2f , 2f , 1.68f , 1.45f , 1.05f , 2.2f , 2.1f , 1.9f  );
        List<Float> hydraulicCostList = Arrays.asList(1.95f , 1.65f , 1.45f , 1.1f , 1.05f , 1.07f , 1.52f , 1.62f , 1.55f , 0.77f , 0.67f , 0.57f  );
        HashMap<String, List<Float>> graphData = new HashMap<>();

        graphData.put("solar", solarCostList);
        graphData.put("wind", windCostList);
        graphData.put("hydraulic", hydraulicCostList);

        return graphData;
    }
    public HashMap<String, List<String>> getEconomicRecommendations(int EnergieToProduce) {

        HashMap<String, List<String>> economicRecommandationData = new HashMap<>();
        List<String> recommandationList = new ArrayList<>();
        List<String> totalCostList = new ArrayList<>();
        List<String> currentCapacityList = new ArrayList<>();

        List<Float> solarMarginalCostList = Arrays.asList(1.5f , -0.15f , -0.05f , -0.1f , -0.2f , 1.4f , -0.2f , -0.3f , -0.2f , 0f , -0.1f , -0.1f );
        List<Float> windMarginalCostList = Arrays.asList(1.8f , -0.25f , -0.1f , 0.5f , 0.25f , -0.2f , -0.32f , -0.23f , -0.4f , 1.15f , -0.1f , -0.2f  );
        List<Float> hydraulicMarginalCostList = Arrays.asList(1.95f , -0.3f , -0.2f , -0.35f , -0.05f , 0.2f , 0.45f , 0.1f , -0.07f , -0.78f , -0.1f , -0.1f  );

        int iSolar =1;                                                   		   	    // nbr  of iteration
        int iWind =1;                                                   		   	    // nbr  of iteration
        int iHydraulic =1;                                                   		    // nbr  of iteration
        int E = EnergieToProduce;                   		 	                        //Energie quantity left to produce
        int E_Solar = 0;                                       		 	                //Energie to be produced with Solar
        int E_Wind = 0;                                      		 	                //Energie to be produced with Wind
        int E_Hydraulic = 0; 	                       		  	                        //Energie to be produced with Hydraulic
        int E_OtherSources = 0;                                                          //Energie to be produced with other sources
        float solarCost = solarMarginalCostList.get(0);  		                        //init Solar unit Cost
        float windCost = windMarginalCostList.get(0);  		                            //init Wind unit Cost
        float hydraulicCost = hydraulicMarginalCostList.get(0); 		                //init hydraulic unit Cost
        float totalCost =0; 				                                            //init total cost
        float infinitCost = 100000000.0f;
        int iterationE =1000; 				                                            //Energie slice
        // Capacity of all solar sites
        int solarCapacity = getCurrentCapacity("solaire") - getInstantaneousPower("solaire");
        // Capacity of all wind sites
        int windCapacity = getCurrentCapacity("éolienne") - getInstantaneousPower("éolienne");
        // Capacity of all hydraulic sites
        int hydraulicCapacity = getCurrentCapacity("éolienne") - getInstantaneousPower("hydraulique");    // used capacity of all hydraulic sites




        currentCapacityList.add("Solar");
        currentCapacityList.add(String.valueOf(solarCapacity));
        currentCapacityList.add("Wind");
        currentCapacityList.add(String.valueOf(windCapacity));
        currentCapacityList.add("Hydraulic");
        currentCapacityList.add(String.valueOf(hydraulicCapacity));
        economicRecommandationData.put("currentCapacity", currentCapacityList);

        log.info("**** total energy to produce : " +String.valueOf(E)) ;
        log.info("**** solar unit cost  : " +String.valueOf(solarCost)) ;
        log.info("**** wind unit cost  : " +String.valueOf(windCost)) ;
        log.info("**** hydraulic unit cost  : " +String.valueOf(hydraulicCost)) ;


        while (E>0){
            log.info("**** total energy left to produce : " +String.valueOf(E)) ;
            log.info("**** current solar capacity : "+String.valueOf(solarCapacity));
            log.info("**** current wind capacity : "+String.valueOf(windCapacity));
            log.info("**** current hydraulic capacity : "+String.valueOf(hydraulicCapacity));
            log.info("**** solar sites are chosen to produce : "+String.valueOf(E_Solar)+" KW");
            log.info("**** wind sites are chosen to produce : "+String.valueOf(E_Wind)+" KW");
            log.info("**** hydraulic sites are chosen to produce : "+String.valueOf(E_Hydraulic)+" KW");
            log.info("**** other sites are chosen to produce : "+String.valueOf(E_Hydraulic)+" KW");
            log.info("**** total cost for renewal energy sources : " +String.valueOf(totalCost)) ;


            if (solarCapacity == 0 && windCapacity == 0 && hydraulicCapacity == 0)
            {
                E_OtherSources +=E;
                log.info("**** no production capacity left for renewal energy sources. Additional quantity of "+String.valueOf(E)+" KW need to be produced from other sources");
                E=0;

            }
            else {
                if (solarCost <= windCost && solarCost <= hydraulicCost ){
                    if(E<1000){
                        if (E <= solarCapacity) {
                            E_Solar += E;
                            totalCost += E * solarCost ;
                            solarCapacity -=E;
                            E=0;
                        }
                        else if (solarCapacity > 0){
                            E_Solar += solarCapacity;
                            totalCost += solarCapacity * solarCost ;
                            E -= solarCapacity;
                            solarCapacity =0;
                            if(windCost <= hydraulicCost && windCapacity > 0){
                                if(E <= windCapacity){
                                    E_Wind += E;
                                    totalCost += E * windCost ;
                                    windCapacity -=E;
                                    E=0;
                                }
                                else {
                                    E_Wind += windCapacity;
                                    totalCost += windCost * windCapacity;
                                    E -= windCapacity;
                                    windCapacity =0;
                                    if(hydraulicCapacity - E >= 0){
                                        E_Hydraulic +=E;
                                        totalCost += E * hydraulicCost;
                                        hydraulicCapacity -= E;
                                        E=0;
                                    }
                                    else {
                                        E_Hydraulic += hydraulicCapacity;
                                        totalCost += hydraulicCapacity * hydraulicCost ;
                                        E -= hydraulicCapacity;
                                        hydraulicCapacity =0;
                                        E_OtherSources = E;
                                        E =0;
                                    }
                                }

                            }
                            else {
                                if(E <= hydraulicCapacity){
                                    E_Hydraulic += E;
                                    totalCost += E * hydraulicCost ;
                                    hydraulicCapacity -=E;
                                    E=0;
                                }
                                else {
                                    E_Hydraulic += hydraulicCapacity;
                                    totalCost += hydraulicCost * hydraulicCapacity;
                                    E -= hydraulicCapacity;
                                    hydraulicCapacity =0;
                                    if(windCapacity - E >= 0){
                                        E_Wind +=E;
                                        totalCost += E * windCost;
                                        windCapacity -= E;
                                        E=0;
                                    }
                                    else {
                                        E_Wind+= windCapacity;
                                        totalCost += windCost * windCapacity ;
                                        E -= windCapacity;
                                        windCapacity =0;
                                        E_OtherSources = E;
                                        E =0;
                                    }
                                }

                            }
                        }
                        else if (windCost <= hydraulicCost && windCapacity >0){
                            if (E <= windCapacity){
                                E_Wind += E;
                                totalCost += E * windCost;
                                windCapacity -= E;
                                E =0;
                            }
                            else {
                                E_Wind += windCapacity;
                                totalCost += windCost * windCapacity;
                                E -= windCapacity;
                                windCapacity =0;
                                if (hydraulicCapacity - E >=0){
                                    E_Hydraulic +=E;
                                    totalCost += hydraulicCost * E;
                                    hydraulicCapacity -=E;
                                    E=0;
                                }
                                else {
                                    E_Hydraulic= hydraulicCapacity;
                                    totalCost += hydraulicCost * hydraulicCapacity;
                                    E -= hydraulicCapacity;
                                    hydraulicCapacity = 0;
                                    E_OtherSources += E;
                                    E=0;
                                }
                            }

                        }
                        else if (hydraulicCapacity > 0){
                            if (E <= hydraulicCapacity){
                                E_Hydraulic +=E;
                                totalCost +=hydraulicCost * E;
                                hydraulicCapacity -= E;
                                E=0;
                            }
                            else {
                                E_Hydraulic +=hydraulicCapacity;
                                totalCost += hydraulicCost * hydraulicCapacity;
                                E -= hydraulicCapacity;
                                hydraulicCapacity =0;
                                E_OtherSources = E;
                                E=0;
                            }
                        }
                        else {
                            E_OtherSources +=E;
                            E=0;
                        }
                    }
                    else {
                        if(iterationE <= solarCapacity){
                            E_Solar +=iterationE;
                            totalCost += solarCost * iterationE;
                            solarCapacity -= iterationE;
                            solarCost += solarMarginalCostList.get(iSolar);
                            iSolar++;
                            E -= iterationE;
                            log.info("**** updated solar unit cost  : " +String.valueOf(solarCost)) ;

                        }
                        else if (solarCapacity >0){
                            E_Solar += solarCapacity;
                            totalCost += solarCost * solarCapacity;
                            E -= solarCapacity;
                            solarCapacity =0;
                            solarCost = infinitCost;

                        }
                        else if (windCost <= hydraulicCost && iterationE <= windCapacity){
                            E_Wind += iterationE;
                            totalCost += windCost * iterationE;
                            windCapacity -= iterationE;
                            windCost +=windMarginalCostList.get(iWind);
                            iWind ++;
                            E -= iterationE;
                            log.info("**** updated wind unit cost  : " +String.valueOf(windCost)) ;

                        }
                        else if (windCost <= hydraulicCost && windCapacity >0){
                            E_Wind+= windCapacity;
                            totalCost += windCost * windCapacity;
                            E -= windCapacity;
                            windCapacity =0;
                            windCost = infinitCost;

                        }
                        else if (iterationE <= hydraulicCapacity){
                            E_Hydraulic +=iterationE;
                            totalCost += hydraulicCost * iterationE;
                            hydraulicCapacity -= iterationE;
                            hydraulicCost +=hydraulicMarginalCostList.get(iHydraulic);
                            iHydraulic ++;
                            E -= iterationE;
                            log.info("**** updated hydraulic unit cost  : " +String.valueOf(hydraulicCost)) ;
                        }
                        else if (hydraulicCapacity >0){
                            E_Hydraulic += hydraulicCapacity;
                            totalCost += hydraulicCost * hydraulicCapacity;
                            E -= hydraulicCapacity;
                            hydraulicCapacity =0;
                            hydraulicCost = infinitCost;

                        }
                        else {
                            E_OtherSources += iterationE;
                            E -= iterationE;
                        }
                    }
                }
                if (windCost <= solarCost && windCost <= hydraulicCost ){
                    if(E<1000){
                        if (E <= windCapacity) {
                            E_Wind += E;
                            totalCost += E * windCost ;
                            windCapacity -=E;
                            E=0;
                        }
                        else if (windCapacity > 0){
                            E_Wind += windCapacity;
                            totalCost += windCapacity * windCost ;
                            E -= windCapacity;
                            windCapacity =0;
                            if(solarCost <= hydraulicCost && solarCapacity > 0){
                                if(E <= solarCapacity){
                                    E_Solar += E;
                                    totalCost += E * solarCost ;
                                    solarCapacity -=E;
                                    E=0;
                                }
                                else {
                                    E_Solar += solarCapacity;
                                    totalCost += solarCost * solarCapacity;
                                    E -= solarCapacity;
                                    solarCapacity =0;
                                    if(hydraulicCapacity - E >= 0){
                                        E_Hydraulic +=E;
                                        totalCost += E * hydraulicCost;
                                        hydraulicCapacity -= E;
                                        E=0;
                                    }
                                    else {
                                        E_Hydraulic += hydraulicCapacity;
                                        totalCost += hydraulicCapacity * hydraulicCost ;
                                        E -= hydraulicCapacity;
                                        hydraulicCapacity =0;
                                        E_OtherSources = E;
                                        E =0;
                                    }
                                }

                            }
                            else {
                                if(E <= hydraulicCapacity){
                                    E_Hydraulic += E;
                                    totalCost += E * hydraulicCost ;
                                    hydraulicCapacity -=E;
                                    E=0;
                                }
                                else {
                                    E_Hydraulic += hydraulicCapacity;
                                    totalCost += hydraulicCost * hydraulicCapacity;
                                    E -= hydraulicCapacity;
                                    hydraulicCapacity =0;
                                    if(solarCapacity - E >= 0){
                                        E_Solar +=E;
                                        totalCost += E * solarCost;
                                        solarCapacity -= E;
                                        E=0;
                                    }
                                    else {
                                        E_Solar+= solarCapacity;
                                        totalCost += solarCost * solarCapacity ;
                                        E -= solarCapacity;
                                        solarCapacity =0;
                                        E_OtherSources = E;
                                        E =0;
                                    }
                                }

                            }
                        }
                        else if (solarCost <= hydraulicCost && solarCapacity >0){
                            if (E <= solarCapacity){
                                E_Solar += E;
                                totalCost += E * solarCost;
                                solarCapacity -= E;
                                E =0;
                            }
                            else {
                                E_Solar += solarCapacity;
                                totalCost += solarCost * solarCapacity;
                                E -= solarCapacity;
                                solarCapacity =0;
                                if (hydraulicCapacity - E >=0){
                                    E_Hydraulic +=E;
                                    totalCost += hydraulicCost * E;
                                    hydraulicCapacity -=E;
                                    E=0;
                                }
                                else {
                                    E_Hydraulic= hydraulicCapacity;
                                    totalCost += hydraulicCost * hydraulicCapacity;
                                    E -= hydraulicCapacity;
                                    hydraulicCapacity = 0;
                                    E_OtherSources += E;
                                    E=0;
                                }
                            }

                        }
                        else if (hydraulicCapacity > 0){
                            if (E <= hydraulicCapacity){
                                E_Hydraulic +=E;
                                totalCost +=hydraulicCost * E;
                                hydraulicCapacity -= E;
                                E=0;
                            }
                            else {
                                E_Hydraulic +=hydraulicCapacity;
                                totalCost += hydraulicCost * hydraulicCapacity;
                                E -= hydraulicCapacity;
                                hydraulicCapacity =0;
                                E_OtherSources = E;
                                E=0;
                            }
                        }
                        else {
                            E_OtherSources +=E;
                            E=0;
                        }
                    }
                    else {
                        if(iterationE <= windCapacity){
                            E_Wind +=iterationE;
                            totalCost += windCost * iterationE;
                            windCapacity -= iterationE;
                            windCost += windMarginalCostList.get(iWind);
                            iWind++;
                            E -= iterationE;
                            log.info("**** updated wind unit cost  : " +String.valueOf(windCost)) ;
                        }
                        else if (windCapacity >0){
                            E_Wind+= windCapacity;
                            totalCost += windCost * windCapacity;
                            E -= windCapacity;
                            windCapacity =0;
                            windCost = infinitCost;

                        }
                        else if (solarCost <= hydraulicCost && iterationE <= solarCapacity){
                            E_Solar += iterationE;
                            totalCost += solarCost * iterationE;
                            solarCapacity -= iterationE;
                            solarCost +=solarMarginalCostList.get(iSolar);
                            iSolar ++;
                            E -= iterationE;
                            log.info("**** updated solar unit cost  : " +String.valueOf(solarCost)) ;

                        }
                        else if (solarCost <= hydraulicCost && solarCapacity >0){
                            E_Solar += solarCapacity;
                            totalCost += solarCost * solarCapacity;
                            E -= solarCapacity;
                            solarCapacity =0;
                            solarCost = infinitCost;

                        }
                        else if (iterationE <= hydraulicCapacity){
                            E_Hydraulic +=iterationE;
                            totalCost += hydraulicCost * iterationE;
                            hydraulicCapacity -= iterationE;
                            hydraulicCost +=hydraulicMarginalCostList.get(iHydraulic);
                            iHydraulic ++;
                            E -= iterationE;
                            log.info("**** updated hydraulic unit cost  : " +String.valueOf(hydraulicCost)) ;
                        }
                        else if (hydraulicCapacity >0){
                            E_Hydraulic += hydraulicCapacity;
                            totalCost += hydraulicCost * hydraulicCapacity;
                            E -= hydraulicCapacity;
                            hydraulicCapacity =0;
                            hydraulicCost = infinitCost;

                        }
                        else {
                            E_OtherSources += iterationE;
                            E -= iterationE;
                        }
                    }
                }
                if (hydraulicCost <= solarCost && hydraulicCost <= windCost ){
                    if(E<1000){
                        if (E <= hydraulicCapacity) {
                            E_Hydraulic += E;
                            totalCost += E * hydraulicCost ;
                            hydraulicCapacity -=E;
                            E=0;
                        }
                        else if (hydraulicCapacity > 0){
                            E_Hydraulic += hydraulicCapacity;
                            totalCost += hydraulicCapacity * hydraulicCost ;
                            E -= hydraulicCapacity;
                            hydraulicCapacity =0;
                            if(solarCost <= windCost && solarCapacity > 0){
                                if(E <= solarCapacity){
                                    E_Solar += E;
                                    totalCost += E * solarCost ;
                                    solarCapacity -=E;
                                    E=0;
                                }
                                else {
                                    E_Solar += solarCapacity;
                                    totalCost += solarCost * solarCapacity;
                                    E -= solarCapacity;
                                    solarCapacity =0;
                                    if(windCapacity - E >= 0){
                                        E_Wind +=E;
                                        totalCost += E * windCost;
                                        windCapacity -= E;
                                        E=0;
                                    }
                                    else {
                                        E_Wind += windCapacity;
                                        totalCost += windCapacity * windCost ;
                                        E -= windCapacity;
                                        windCapacity =0;
                                        E_OtherSources = E;
                                        E =0;
                                    }
                                }

                            }
                            else {
                                if(E <= windCapacity){
                                    E_Wind += E;
                                    totalCost += E * windCost ;
                                    windCapacity -=E;
                                    E=0;
                                }
                                else {
                                    E_Wind += windCapacity;
                                    totalCost += windCost * windCapacity;
                                    E -= windCapacity;
                                    windCapacity =0;
                                    if(solarCapacity - E >= 0){
                                        E_Solar +=E;
                                        totalCost += E * solarCost;
                                        solarCapacity -= E;
                                        E=0;
                                    }
                                    else {
                                        E_Solar+= solarCapacity;
                                        totalCost += solarCost * solarCapacity ;
                                        E -= solarCapacity;
                                        solarCapacity =0;
                                        E_OtherSources = E;
                                        E =0;
                                    }
                                }

                            }
                        }
                        else if (solarCost <= windCost && solarCapacity >0){
                            if (E <= solarCapacity){
                                E_Solar += E;
                                totalCost += E * solarCost;
                                solarCapacity -= E;
                                E =0;
                            }
                            else {
                                E_Solar += solarCapacity;
                                totalCost += solarCost * solarCapacity;
                                E -= solarCapacity;
                                solarCapacity =0;
                                if (windCapacity - E >=0){
                                    E_Wind +=E;
                                    totalCost += windCost * E;
                                    windCapacity -=E;
                                    E=0;
                                }
                                else {
                                    E_Wind= windCapacity;
                                    totalCost += windCost * windCapacity;
                                    E -= windCapacity;
                                    windCapacity = 0;
                                    E_OtherSources += E;
                                    E=0;
                                }
                            }

                        }
                        else if (windCapacity > 0){
                            if (E <= windCapacity){
                                E_Wind +=E;
                                totalCost +=windCost * E;
                                windCapacity -= E;
                                E=0;
                            }
                            else {
                                E_Wind +=windCapacity;
                                totalCost += windCost * windCapacity;
                                E -= windCapacity;
                                windCapacity =0;
                                E_OtherSources = E;
                                E=0;
                            }
                        }
                        else {
                            E_OtherSources +=E;
                            E=0;
                        }
                    }
                    else {
                        if(iterationE <= hydraulicCapacity){
                            E_Hydraulic +=iterationE;
                            totalCost += hydraulicCost * iterationE;
                            hydraulicCapacity -= iterationE;
                            hydraulicCost += hydraulicMarginalCostList.get(iHydraulic);
                            iHydraulic++;
                            E -= iterationE;
                            log.info("**** updated hydraulic unit cost  : " +String.valueOf(hydraulicCost)) ;
                        }
                        else if (hydraulicCapacity >0){
                            E_Hydraulic += hydraulicCapacity;
                            totalCost += hydraulicCost * hydraulicCapacity;
                            E -= hydraulicCapacity;
                            hydraulicCapacity =0;
                            hydraulicCost = infinitCost;

                        }
                        else if (solarCost <= windCost && iterationE <= solarCapacity){
                            E_Solar += iterationE;
                            totalCost += solarCost * iterationE;
                            solarCapacity -= iterationE;
                            solarCost +=solarMarginalCostList.get(iSolar);
                            iSolar ++;
                            E -= iterationE;
                            log.info("**** updated solar unit cost  : " +String.valueOf(solarCost)) ;

                        }
                        else if (solarCost <= windCost  && solarCapacity >0){
                            E_Solar += solarCapacity;
                            totalCost += solarCost * solarCapacity;
                            E -= solarCapacity;
                            solarCapacity =0;
                            solarCost = infinitCost;

                        }
                        else if (iterationE <= windCapacity){
                            E_Wind +=iterationE;
                            totalCost += windCost * iterationE;
                            windCapacity -= iterationE;
                            windCost +=windMarginalCostList.get(iWind);
                            iWind ++;
                            E -= iterationE;
                            log.info("**** updated wind unit cost  : " +String.valueOf(windCost)) ;
                        }
                        else if ( windCapacity >0){
                            E_Wind+= windCapacity;
                            totalCost += windCost * windCapacity;
                            E -= windCapacity;
                            windCapacity =0;
                            windCost = infinitCost;

                        }
                        else {
                            E_OtherSources += iterationE;
                            E -= iterationE;
                        }
                    }
                }
            }

        }

        totalCostList.add(String.valueOf(totalCost));
        economicRecommandationData.put("totalCost", totalCostList);

        recommandationList.add("Solar");
        recommandationList.add(String.valueOf(E_Solar));
        recommandationList.add("Wind");
        recommandationList.add(String.valueOf(E_Wind));
        recommandationList.add("Hydraulic");
        recommandationList.add(String.valueOf(E_Hydraulic));
        recommandationList.add("autres");
        recommandationList.add(String.valueOf(E_OtherSources));

        economicRecommandationData.put("recommendation", recommandationList);
        log.info("the economic recommendations are : " +economicRecommandationData) ;
        return economicRecommandationData;
    }
    public Float getSolarCF(float quantity) {
        // carbon footprint for 1 produced kW in CO2 per KW
        Float carbonFootprintForKWSolar =  43.9f;
        return carbonFootprintForKWSolar * quantity;
    }

    public Float getWindCF(float quantity) {
        // carbon footprint for 1 produced kW in CO2 per KW
        Float carbonFootprintForKWWind =  9f;
        return carbonFootprintForKWWind * quantity;
    }

    public Float getHydraulicCF(float quantity) {
        // carbon footprint for 1 produced kW in CO2 per KW
        Float carbonFootprintForKWHydraulic = 10f;
        return carbonFootprintForKWHydraulic * quantity;
    }
    public HashMap<String, List<Float>> getEnvironmentalData() {
        List<Float> solarCFList = new ArrayList<>();
        List<Float> windCFList = new ArrayList<>();
        List<Float> hydraulicCFList = new ArrayList<>();
        HashMap<String, List<Float>> graphData = new HashMap<>();
        for (int i = 0; i <= 10000; i += 1000) {
            solarCFList.add(getSolarCF(i));
            windCFList.add(getWindCF(i));
            hydraulicCFList.add(getHydraulicCF(i));
        }
        graphData.put("solar", solarCFList);
        graphData.put("wind", windCFList);
        graphData.put("hydraulic", hydraulicCFList);
        return graphData;
    }
    public HashMap<String, List<String>> getEnvironmentalRecommendations(int simValue){

        Float simValueQuantitySolarCF = getSolarCF(simValue);
        Float simValueQuantityWindCF = getWindCF(simValue);
        Float simValueQuantityHydraulicCF = getHydraulicCF(simValue);
        HashMap<String, List<String>> environmentalRecommendationData = new HashMap<>();
        List<String> recommendationList = new ArrayList<>();
        if (simValueQuantitySolarCF <= simValueQuantityWindCF && simValueQuantitySolarCF <= simValueQuantityWindCF) {
            recommendationList.add("solaire");
            if (simValueQuantityWindCF <= simValueQuantityHydraulicCF) {
                recommendationList.add("eolienne");
                recommendationList.add("hydraulique");
            } else {
                recommendationList.add("hydraulique");
                recommendationList.add("eolienne");
            }
        } else if (simValueQuantityWindCF < simValueQuantitySolarCF && simValueQuantityWindCF < simValueQuantityHydraulicCF) {
            recommendationList.add("eolienne");
            if (simValueQuantitySolarCF < simValueQuantityHydraulicCF) {
                recommendationList.add("solaire");
                recommendationList.add("hydraulique");
            } else {
                recommendationList.add("hydraulique");
                recommendationList.add("solaire");
            }
        } else if (simValueQuantityHydraulicCF < simValueQuantitySolarCF && simValueQuantityHydraulicCF < simValueQuantityWindCF) {
            recommendationList.add("hydraulique");
            if (simValueQuantitySolarCF < simValueQuantityWindCF) {
                recommendationList.add("solaire");
                recommendationList.add("eolienne");
            } else {
                recommendationList.add("eolienne");
                recommendationList.add("solaire");
            }
        }
        environmentalRecommendationData.put("recommendation", recommendationList);
        log.info("the environmental recommendation of energy sources" +environmentalRecommendationData) ;
        return environmentalRecommendationData;



    }
    public Integer getCurrentCapacity(String energyType){
       return mixRepository.findCurrentCapacity(energyType);

    }
    public Integer getInstantaneousPower(String energyType){
        return mixRepository.findInstantaneousPower(energyType);

    }

    public HashMap<String, List<Integer>> getAverageProductionData() {
        List<Integer> averageProductionSolarList = new ArrayList<>();
        List<Integer> averageProductionWindList = new ArrayList<>();
        List<Integer> averageProductionHydraulicList = new ArrayList<>();
        HashMap<String, List<Integer>> graphBarData = new HashMap<>();
        averageProductionSolarList = averageRepository.getAverage_prod("solaire");
        averageProductionWindList = averageRepository.getAverage_prod("éolienne");
        averageProductionHydraulicList = averageRepository.getAverage_prod("hydraulique");
        graphBarData.put("solar", averageProductionSolarList);
        graphBarData.put("wind", averageProductionWindList);
        graphBarData.put("hydraulic", averageProductionHydraulicList);

        return graphBarData;


    }



}