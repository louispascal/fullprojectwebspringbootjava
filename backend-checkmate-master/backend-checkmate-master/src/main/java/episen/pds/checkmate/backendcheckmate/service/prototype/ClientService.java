package episen.pds.checkmate.backendcheckmate.service.prototype;

import episen.pds.checkmate.backendcheckmate.dao.prototype.ClientRepository;
import episen.pds.checkmate.backendcheckmate.model.prototype.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }


    public void addClient(Client client) {
        clientRepository.save(client);
    }

    public List<Client> selectAllClient() {
        return clientRepository.findAll();
    }

    public Optional<Client> getClientById(Integer id) {
        return clientRepository.findById(id);
    }

    public void updateClient(Client client) {
        clientRepository.save(client);
    }

    public void deleteClient(Integer id) {
        clientRepository.deleteById(id);
    }


}
