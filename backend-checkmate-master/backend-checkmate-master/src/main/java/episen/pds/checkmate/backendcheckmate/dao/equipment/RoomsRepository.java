package episen.pds.checkmate.backendcheckmate.dao.equipment;

import episen.pds.checkmate.backendcheckmate.model.equipment.Rooms;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomsRepository extends JpaRepository<Rooms, Integer> {

    @Query(value = "select distinct r.type, id_citizen,type_equipment,id_equipment,r.id_room \n" +
            "from energy.room r\n" +
            "INNER JOIN energy.home h ON r.id_home = h.id_home \n" +
            "INNER JOIN energy.equipment e ON r.id_room = e.id_room \n" +
            "where h.id_citizen = ?1", nativeQuery = true)
    Iterable<Rooms> getRoomByUserId(Integer id);
}
