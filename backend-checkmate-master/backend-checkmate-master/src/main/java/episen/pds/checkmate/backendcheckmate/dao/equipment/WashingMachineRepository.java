package episen.pds.checkmate.backendcheckmate.dao.equipment;

import episen.pds.checkmate.backendcheckmate.model.equipment.WashingMachine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface WashingMachineRepository extends JpaRepository<WashingMachine, Integer> {
    @Transactional
    @Modifying
    @Query(value = "UPDATE energy.washing_machine SET mode=?1 WHERE id_equipment=?2 ", nativeQuery = true)
    void setWashingMachine( String WashingMachineMode, Integer id);
    @Query(value = "Select mode from energy.washing_machine where id_equipment= ?1",nativeQuery = true)
    String getWashingMachineMode(Integer id_equipment);
    @Query(value = "Select * from energy.washing_machine where id_equipment= ?1",nativeQuery = true)
    Optional<WashingMachine> findWashingMachine(Integer id);

}