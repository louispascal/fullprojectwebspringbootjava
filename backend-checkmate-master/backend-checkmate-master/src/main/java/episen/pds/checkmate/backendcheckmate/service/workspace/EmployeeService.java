package episen.pds.checkmate.backendcheckmate.service.workspace;

import episen.pds.checkmate.backendcheckmate.dao.workspace.AccessRepository;
import episen.pds.checkmate.backendcheckmate.dao.workspace.EmployeeRepository;
import episen.pds.checkmate.backendcheckmate.model.workspace.Access;
import episen.pds.checkmate.backendcheckmate.model.workspace.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final AccessRepository accessRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, AccessRepository accessRepository) {
        this.employeeRepository = employeeRepository;
        this.accessRepository = accessRepository;
    }

    public List<Employee> getMeetingParticipants(Integer id) {
        return employeeRepository.getMeetingParticipants(id);
    }


    public void addEmployee(Employee employee) {
        employeeRepository.save(employee);
    }

    public List<Employee> selectAllEmployee(int id) {
        return employeeRepository.getEmployees(id);
    }

    public Optional<Employee> getEmployeeById(Integer id) {
        return employeeRepository.findById(id);
    }

    public void updateEmployee(Employee employee) {
        employeeRepository.save(employee);
    }

    public void deleteEmployee(Integer id) {
        employeeRepository.deleteById(id);
    }

    public Optional<Employee> loginEmployee(String email) {
        return employeeRepository.login(email);
    }

    public Optional<Access> getAccess(Integer id_access) {return accessRepository.getAccess(id_access);
    }
}
