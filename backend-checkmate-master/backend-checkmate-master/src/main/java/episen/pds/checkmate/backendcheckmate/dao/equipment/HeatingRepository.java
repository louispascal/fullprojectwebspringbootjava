package episen.pds.checkmate.backendcheckmate.dao.equipment;

import episen.pds.checkmate.backendcheckmate.model.equipment.Heating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface HeatingRepository extends JpaRepository<Heating, Integer> {
    @Transactional
    @Modifying
    @Query(value = "UPDATE energy.heating SET value=?1,mode=?2 WHERE id_equipment=?3 ", nativeQuery = true)
    void setHeating(Integer heatingValue, String HeatingMode, Integer id_equipment);
    @Query(value = "Select mode from energy.heating where id_equipment= ?1",nativeQuery = true)
    String getHeatingMode(Integer id_equipment);
    @Query(value = "Select value from energy.heating where id_equipment= ?1",nativeQuery = true)
    Integer getHeatingValue(Integer id_equipment);
    @Query(value = "Select * from energy.heating where id_equipment= ?1", nativeQuery = true)
    Optional<Heating> findHeating(Integer id);
    @Query(value = "Select heat from mock.sensor where id_sensor = (SELECT MAX(id_sensor) FROM mock.sensor)",nativeQuery = true)
    Integer getCaptorValues();
    @Query(value = "UPDATE energy.heating SET value=?1 WHERE id_equipment=?2 ", nativeQuery = true)
    void setWithAutoMode(Integer autoValue,Integer id);


}
