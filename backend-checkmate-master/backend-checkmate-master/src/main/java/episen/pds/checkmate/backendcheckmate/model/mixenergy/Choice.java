package episen.pds.checkmate.backendcheckmate.model.mixenergy;
import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "choice", schema = "energy")
@Data
    public class Choice{
        @Id
        private Integer id_choice;
        private Integer  solar;
        private Integer hydraulic ;
        private Integer wind;
        private Integer  proportion_solar;
        private Integer proportion_hydraulic ;
        private Integer proportion_wind;
}
