package episen.pds.checkmate.backendcheckmate.dao.monitoring;

import episen.pds.checkmate.backendcheckmate.model.monitoring.Home;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface HomeRepository extends JpaRepository<Home, Integer> {
    @Query(value = "select r.id_home from energy.home r INNER JOIN energy.citizen h on r.id_citizen = h.id_citizen WHERE h.id_citizen = ?1", nativeQuery = true)
    Integer findIdHomeByIdCitizen(int idCitizen);
}
