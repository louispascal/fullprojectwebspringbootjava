package episen.pds.checkmate.backendcheckmate.model.scope8;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(schema = "dwp")
public class Equipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_equipment;
    private String name;


    @OneToOne
    private Reservation reservation;

    public Equipment(Integer id_equipment, String name) {
        this.id_equipment = id_equipment;
        this.name = name;

    }

    public Equipment() {

    }


}


/*public class Equipment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer idequip;

    private String nom;
    private Integer idconsumer;
    private int kw;
    private LocalDateTime time;
    private LocalDateTime finishtime;


    public Equipment(Integer idequip, int idconsumer,int kw,String nom,   LocalDateTime time,LocalDateTime finishtime , int id) {
        this.idequip = idequip;
        this.nom = nom;
        this.kw= kw;
        this.idconsumer=idconsumer;
        this.id=id;
        this.time=time;
        this.finishtime=finishtime;


    }

    public Equipment() {

    }


    @Override
    public String toString() {
        return "Equipement{" +
                "idequip=" + idequip +
                ", name='" + nom + '\'' +
                ", idconsumer='" + idconsumer + '\'' +
                ", kw=" + kw + '\'' +
                ", id=" + id + '\'' +
                ", time=" + time + '\'' +
                ", finishtime=" + finishtime +



                '}';
    }
}

*/
