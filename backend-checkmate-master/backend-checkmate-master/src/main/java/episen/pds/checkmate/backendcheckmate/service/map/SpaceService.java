package episen.pds.checkmate.backendcheckmate.service.map;

import episen.pds.checkmate.backendcheckmate.dao.map.SpaceRepository;
import episen.pds.checkmate.backendcheckmate.model.map.Space;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class SpaceService {

    private final SpaceRepository spaceRepository;

    @Autowired
    public SpaceService(SpaceRepository spaceRepository) {
        this.spaceRepository = spaceRepository;
    }


    public void saveSpace(Space space) {
        log.info("Saving space " + space);
        spaceRepository.saveSpace(space.getPosition_in_area(), space.getSpace_number(), space.getId_area(), space.getId_type_space(), space.getType());
    }

    public List<Space> getSpaceByArea(Integer id_building, Integer id_floor, Integer id_area) {
        List<Space> spaceList = spaceRepository.getSpaceByArea(id_building, id_floor, id_area);
        log.info("Getting spaces from specified id_area (" + id_area + ") : " + spaceList.toString());
        return spaceList;
    }

    public Integer getMaxSpaceNumberByType(Integer id_type_space) {
        Integer max = spaceRepository.getMaxSpaceNumberByType(id_type_space).orElse(0);
        log.info("Getting the maximum number by space type (" + id_type_space + ") : " + max);
        return max;
    }

    public Integer getMaxPositionInArea(Integer id_area) {
        return spaceRepository.getMaxPositionInArea(id_area).orElse(0);
    }

    public float getRemainingSpace(Integer id_area) {
        float remainingSpace = spaceRepository.getRemainingSpace(id_area).orElse(0.0f);
        log.info("Remaining space in area (" + id_area + ") : " + (100.0f - remainingSpace));
        return remainingSpace;
    }

    public float getRemainingSpace2(Integer id_area) {
        float remainingSpace = spaceRepository.getRemainingSpace2(id_area).orElse(0.0f);
        log.info("Remaining space in area (" + id_area + ") : " + (100.0f - remainingSpace));
        return remainingSpace;
    }

    public boolean isEmpty(Integer id_floor) {
        Boolean empty = spaceRepository.isEmpty(id_floor);
        String emptiness;
        boolean isEmpty;
        log.info(empty.toString());
        if (empty) {
            emptiness = "empty";
            isEmpty = true;
        } else {
            emptiness = "not empty";
            isEmpty = false;
        }
        log.info("The floor (id_floor : " + id_floor + ") is " + emptiness + ".");
        return isEmpty;
    }

    public void deleteSpaces(Integer id_floor) {
        log.info("Deleting spaces where id_floor = " + id_floor);
        spaceRepository.deleteSpaces(id_floor);
    }

    public void deleteCorridors(Integer id_floor) {
        log.info("Deleting corridors where id_floor = " + id_floor);
        spaceRepository.deleteCorridors(id_floor);
    }

    public List<Space> getAvailableRooms(Integer id_company) {
        return spaceRepository.getAvailableRooms(id_company);
    }

    public List<Space> getAvailableRoomsByBuilding(Integer id_building) {
        return spaceRepository.getAvailableRoomsByBuilding(id_building);
    }

    public List<Space> getAvailableRoomsByFloor(Integer id_floor) {
        return spaceRepository.getAvailableRoomsByFloor(id_floor);
    }


    public List<Space> getOpenSpaces(Integer id_company) {
        return spaceRepository.getOpenSpaces(id_company);
    }

    public List<Space> getReservedSpaces(Integer id) {
        return spaceRepository.getReservedSpaces(id);

    }
}
