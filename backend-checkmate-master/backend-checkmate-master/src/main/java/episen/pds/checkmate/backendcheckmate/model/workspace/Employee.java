package episen.pds.checkmate.backendcheckmate.model.workspace;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(schema = "dwp")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_employee;
    private String name;
    private String firstName;
    private String job;
    private String email;
    private Integer id_company;
    private Integer id_access;
    private Integer id_status;


}
