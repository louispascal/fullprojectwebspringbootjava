package episen.pds.checkmate.backendcheckmate.dao.map;

import episen.pds.checkmate.backendcheckmate.model.map.Building;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BuildingRepository extends JpaRepository<Building, Integer> {

    List<Building> findByCompany(Integer id);
}

