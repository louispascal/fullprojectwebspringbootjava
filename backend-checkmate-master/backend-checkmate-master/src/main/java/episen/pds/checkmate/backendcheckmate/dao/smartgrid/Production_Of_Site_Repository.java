package episen.pds.checkmate.backendcheckmate.dao.smartgrid;

import episen.pds.checkmate.backendcheckmate.model.mixenergy.Production_of_site;
import episen.pds.checkmate.backendcheckmate.model.smartgrid.District;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Production_Of_Site_Repository extends JpaRepository<Production_of_site, Integer> {

}
