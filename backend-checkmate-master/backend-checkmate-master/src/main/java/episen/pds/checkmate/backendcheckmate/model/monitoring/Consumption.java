package episen.pds.checkmate.backendcheckmate.model.monitoring;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Ines
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "consumption", schema = "energy")
public class Consumption implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_consumption")
    private Integer idConsumption;

    @Column(name = "instantaneous_power")
    private Double instantaneousPower;

    @Column(name = "sampling_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date samplingDate;

    @JoinColumn(name = "id_equipment", referencedColumnName = "id_equipment")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private EquipmentEnergy equipment;

//    @Column(name = "value_consumption")
//    private BigDecimal valueConsumption;
//    private String unity;
//    @Column(name = "date_prelevement")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date datePrelevement;
//    @JoinColumn(name = "id_equipement_room", referencedColumnName = "id", nullable = false)
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
//    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//    private Equipment equipementRoom;
    @Override
    public String toString() {
        return "episen.pds.checkmate.backendcheckmate.model.Consumption[ id_consumption=" + idConsumption + " instantaneous_power= " + instantaneousPower + " sampling_date= " + samplingDate + " id_equipment= " + equipment.getIdEquipment() +" ]";
    }

}
