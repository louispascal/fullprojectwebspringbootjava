package episen.pds.checkmate.backendcheckmate.api.monitoring;

import episen.pds.checkmate.backendcheckmate.model.monitoring.Consumption;
import episen.pds.checkmate.backendcheckmate.model.monitoring.EquipmentEnergy;
import episen.pds.checkmate.backendcheckmate.model.monitoring.Home;
import episen.pds.checkmate.backendcheckmate.model.monitoring.Room;
import episen.pds.checkmate.backendcheckmate.model.monitoring.SolarPanel;
import episen.pds.checkmate.backendcheckmate.model.monitoring.WindTurbine;
import episen.pds.checkmate.backendcheckmate.service.monitoring.ConsumptionService;
import episen.pds.checkmate.backendcheckmate.service.monitoring.EquipmentEnergyService;
import episen.pds.checkmate.backendcheckmate.service.monitoring.HomeService;
import episen.pds.checkmate.backendcheckmate.service.monitoring.RoomService;
import episen.pds.checkmate.backendcheckmate.service.monitoring.SolarPanelService;
import episen.pds.checkmate.backendcheckmate.service.monitoring.WindTurbineService;
import java.util.Calendar;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/*
This class represent the consumption controller of the backend
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "monitoring")
@RestController
public class MonitoringController {
    private final ConsumptionService consumptionService;
    private final RoomService roomService;
    private final EquipmentEnergyService equipmentService;
    private final HomeService homeService;
    private final SolarPanelService solarPanelService;
    private final WindTurbineService windTurbineService;
    

    @Autowired
    public MonitoringController(ConsumptionService consumptionService, RoomService roomService, 
            EquipmentEnergyService equipmentService, HomeService homeService,
            SolarPanelService solarPanelService, WindTurbineService windTurbineService) {
       
        this.consumptionService = consumptionService;
        this.roomService = roomService;
        this.equipmentService = equipmentService;
        this.homeService = homeService;
        this.solarPanelService = solarPanelService;
        this.windTurbineService = windTurbineService;
    }


    @GetMapping("/homes")
    public List<Home> getHomes() {
        return homeService.selectAllHome();
    }

    @GetMapping("/home/room/{id}")
    public List<Room> getRoomByIdHome(@PathVariable Integer id) {
        return roomService.findByIdHome(id);
    }

    @GetMapping("/home/{id}")
    public Optional<Home> getHomeById(@PathVariable Integer id) {
        return homeService.getHomeById(id);
    }

    @GetMapping("/room/{id}")
    public Optional<Room> getRoomById(@PathVariable Integer id) {
        return roomService.getRoomById(id);
    }

    @GetMapping("/equipment/{id}")
    public Optional<EquipmentEnergy> getEquipmentById(@PathVariable Integer id) {
        return equipmentService.getEquipmentById(id);
    }

    @GetMapping("/home/equipment/{id}")
    public List<EquipmentEnergy> getEquipmentByIdHome(@PathVariable Integer id) {
        return equipmentService.findByIdHome(id);
    }
    
    @GetMapping("/building/windTurbine/{id}")
    public List<WindTurbine> getWindTurbineByIdBuilding(@PathVariable Integer id) {
        return windTurbineService.findByIdBuilding(id);
    }
    
    @GetMapping("/building/solarPanel/{id}")
    public List<SolarPanel> getSolarPanelByIdBuilding(@PathVariable Integer id) {
        return solarPanelService.findByIdBuilding(id);
    }

    @GetMapping("/room/equipment/{id}")
    public List<EquipmentEnergy> getEquipmentByIdRoom(@PathVariable Integer id) {
        return equipmentService.findByIdRoom(id);
    }

    @GetMapping("/equipments")
    public List<EquipmentEnergy> equipements() {
        return equipmentService.selectAllEquipment();
    }

    @GetMapping("/equipment/consumption/{id}")
    public List<Consumption> consumptions(@PathVariable Integer id) {
        return consumptionService.findByIdEquipmentDate(id, addDayToDate(new Date(), -3), addDayToDate(new Date(), 1));
    }
        
    private Date addDayToDate(Date date, int nb){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, nb);
        return calendar.getTime();
    }
}
