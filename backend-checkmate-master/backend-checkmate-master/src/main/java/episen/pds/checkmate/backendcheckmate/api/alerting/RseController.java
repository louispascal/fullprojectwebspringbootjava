package episen.pds.checkmate.backendcheckmate.api.alerting;


import episen.pds.checkmate.backendcheckmate.model.alerting.RseEmployee;
import episen.pds.checkmate.backendcheckmate.service.alerting.RseEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path = "rseuser")
public class RseController {

    private final RseEmployeeService rseEmployeeService;

    @Autowired
    public RseController(RseEmployeeService rseEmployeeService) {
        this.rseEmployeeService = rseEmployeeService;
    }

    @PostMapping("/create")
    public void addRseEmployee(@RequestBody RseEmployee rseEmployee) {
        rseEmployeeService.addRseEmployee(rseEmployee);
    }

    @GetMapping("/read")
    public List<RseEmployee> selectAllRseEmployees() {
        return rseEmployeeService.selectAllRseEmployees();
    }

    @GetMapping(path = "/readbyid/{id}")
    public Optional<RseEmployee> getRseEmployeeById(@PathVariable Integer id) {
        return rseEmployeeService.getRseEmployeeById(id);
    }

    @PutMapping("/update")
    public void updateRseEmployee(@RequestBody RseEmployee rseEmployee) {
        rseEmployeeService.updateRseEmployee(rseEmployee);
    }

    @DeleteMapping(path = "/deletebyid/{id}")
    public void deleteRseEmployee(@PathVariable Integer id) {
        rseEmployeeService.deleteRseEmployee(id);

    }

    @GetMapping(path="/loginRseEmployee/{email}")
    public Optional<RseEmployee> loginRseEmployee(@PathVariable String email)
    {return rseEmployeeService.loginRseEmployee(email);}





}
