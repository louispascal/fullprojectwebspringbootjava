package episen.pds.checkmate.backendcheckmate.dao.monitoring;

import episen.pds.checkmate.backendcheckmate.model.monitoring.ProductionSolarPanel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ines
 */
@Repository
public interface ProductionSolarPanelRepository extends JpaRepository<ProductionSolarPanel, Integer> {
    @Query(value = "SELECT * from energy.production_solar_panel c WHERE c.id_solar_panel = ?1 ORDER BY c.sampling_date DESC", nativeQuery = true)
    List<ProductionSolarPanel> findByIdSolarPanel(int idSolarPanel);
}
