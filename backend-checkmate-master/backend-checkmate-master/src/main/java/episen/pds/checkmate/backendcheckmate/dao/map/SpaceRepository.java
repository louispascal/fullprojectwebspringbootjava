package episen.pds.checkmate.backendcheckmate.dao.map;

import episen.pds.checkmate.backendcheckmate.model.map.Space;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.lang.management.OperatingSystemMXBean;
import java.util.List;
import java.util.Optional;

public interface SpaceRepository extends JpaRepository<Space, Integer> {

    @Query(nativeQuery = true)
    Integer countNbOfIndividualOffice(Integer floor_id);

    @Query(nativeQuery = true)
    Integer countNbOfDoubleOffice(Integer floor_id);

    @Query(nativeQuery = true)
    List<Space> getSpaceByArea(Integer id_building, Integer floor_id, Integer id_area);

    @Query(nativeQuery = true)
    Optional<Integer> getMaxSpaceNumberByType(Integer id_type_space);

    @Query(nativeQuery = true)
    Optional<Integer> getMaxPositionInArea(Integer id_area);

    @Modifying
    @Transactional
    @Query(nativeQuery = true)
    void saveSpace(Integer position_in_area, Integer space_number, Integer id_area, Integer id_type_space, String type);

    @Query(nativeQuery = true)
    Optional<Float> getRemainingSpace(Integer id_area);

    @Query(nativeQuery = true)
    Optional<Float> getRemainingSpace2(Integer id_area);

    @Query(nativeQuery = true)
    Boolean isEmpty(Integer id_floor);

    @Modifying
    @Transactional
    @Query(nativeQuery = true)
    void deleteSpaces(Integer id_floor);

    @Modifying
    @Transactional
    @Query(nativeQuery = true)
    void deleteCorridors(Integer id_floor);

    @Query(nativeQuery = true)
    Integer getNextNeighbor(Integer Position, Integer id_area);

    @Query(nativeQuery = true)
    Integer getPreviousNeighbor(Integer Position, Integer id_area);

    @Query(value = "select id_space,space_number,id_area,id_type_space,position_in_area,type as type from dwp.space where id_type_space!=2 and id_area in (select id_area from dwp.area where id_floor in(select id_floor from dwp.floor where id_building in(select id_building from dwp.building where id_company=?1)))", nativeQuery = true)
    List<Space> getAvailableRooms(Integer id_company);

    @Query(value = "select id_space,space_number,id_area,id_type_space,position_in_area,type as type from dwp.space  where id_type_space!=2 and id_area in (select id_area from dwp.area where id_floor in(select id_floor from dwp.floor where id_building=?1))", nativeQuery = true)
    List<Space> getAvailableRoomsByBuilding(Integer id_building);

    @Query(value = "select id_space,space_number,id_area,id_type_space,position_in_area,type as type from dwp.space where id_type_space!=2 and id_area in (select id_area from dwp.area where id_floor=?1)", nativeQuery = true)
    List<Space> getAvailableRoomsByFloor(Integer id_floor);

    @Query(value = "select id_space,space_number,id_area,id_type_space,position_in_area,type as type from dwp.space where id_type_space=1 and id_area in (select id_area from dwp.area where id_floor in(select id_floor from dwp.floor where id_building in(select id_building from dwp.building where id_company=?1)))", nativeQuery = true)
    List<Space> getOpenSpaces(Integer id_company);

    @Query(value="select id_space,space_number,id_area,id_type_space,position_in_area,type from dwp.space inner join dwp.reservation on(dwp.space.id_space=dwp.reservation.id_spacepk) where id_employee=?1 order by id_space", nativeQuery = true)
    List<Space> getReservedSpaces(Integer id);


}
