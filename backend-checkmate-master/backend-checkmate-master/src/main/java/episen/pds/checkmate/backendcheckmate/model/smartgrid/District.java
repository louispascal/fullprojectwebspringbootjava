package episen.pds.checkmate.backendcheckmate.model.smartgrid;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table (schema = "energy")
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_district;
    private Integer consumption;
    private Integer production;
    private String sampling_date;

    @Override
    public String toString() {
        return "District{" +
                "id_district=" + id_district +
                ", consumption=" + consumption +
                ", production=" + production +
                ", sampling_date='" + sampling_date + '\'' +
                '}';
    }
}