package episen.pds.checkmate.backendcheckmate.dao.analyse;


import episen.pds.checkmate.backendcheckmate.model.analyse.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IHomeAnalyzeRepository extends JpaRepository<HomeAnalyze, Integer> {
    HomeAnalyze getByName(String name);
}

