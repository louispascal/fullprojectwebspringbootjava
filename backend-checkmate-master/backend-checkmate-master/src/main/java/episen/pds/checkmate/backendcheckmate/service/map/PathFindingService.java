package episen.pds.checkmate.backendcheckmate.service.map;

import episen.pds.checkmate.backendcheckmate.dao.map.AreaRepository;
import episen.pds.checkmate.backendcheckmate.dao.map.CorridorRepository;
import episen.pds.checkmate.backendcheckmate.dao.map.SpaceRepository;
import episen.pds.checkmate.backendcheckmate.model.map.Coordinate;
import episen.pds.checkmate.backendcheckmate.model.map.MapInformation;
import episen.pds.checkmate.backendcheckmate.model.map.Node;
import episen.pds.checkmate.backendcheckmate.model.map.Space;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class PathFindingService {

    private final SpaceRepository spaceRepository;
    private final AreaRepository areaRepository;
    private final CorridorRepository corridorRepository;

    @Autowired
    public PathFindingService(SpaceRepository spaceRepository, AreaRepository areaRepository, CorridorService corridorService, CorridorRepository corridorRepository) {
        this.spaceRepository = spaceRepository;
        this.areaRepository = areaRepository;
        this.corridorRepository = corridorRepository;
    }

    public ArrayList<Coordinate> retrievePath(Integer goId, Integer toId, MapInformation mapInformation) {
        String[][] mapRepresentation = createMap(mapInformation);
        return searchTarget(goId, toId, mapRepresentation);
    }

    private ArrayList<Coordinate> searchTarget(Integer goId, Integer toId, String[][] mapRepresentation) {
        Space goSpace = spaceRepository.getById(goId);
        String goLocalisation = areaRepository.getById(goSpace.getId_area()).getArea_localisation();
        int startI = 0;
        int startJ = 0;
        loop:
        for (int i = 0; i < mapRepresentation.length; i++) {
            for (int j = 0; j < mapRepresentation[i].length; j++) {
                if (mapRepresentation[i][j].contains("P" + goId)) {
                    startI = i;
                    startJ = j;
                    break loop;
                }
            }
        }
        Coordinate fromSpace = new Coordinate(startI, startJ);
        log.info("FROM : " + fromSpace);
        ArrayList<Coordinate> solution = findPath(mapRepresentation, startI, startJ, toId);
        for (Coordinate nodeC : solution) {
            System.out.println(nodeC);
        }

        return solution;
    }

    private ArrayList<Coordinate> findPath(String[][] mapRepresentation, int startI, int startJ, Integer toId) {
        Node source = new Node(startI, startJ, 0, new ArrayList<>());
        Queue<Node> queue = new LinkedList<>();
        queue.add(new Node(source.getRow(), source.getColumn(), 0, new ArrayList<>()));
        boolean[][] visited = new boolean[mapRepresentation.length][mapRepresentation[0].length];
        visited[source.getRow()][source.getColumn()] = true;

        ArrayList<Node> nodes = new ArrayList<>();
        ArrayList<Coordinate> solution = new ArrayList<>();
        while (!queue.isEmpty()) {
            Node currentNode = queue.remove();
            nodes.add(currentNode);
            // Destination found;
            if (Objects.equals(mapRepresentation[currentNode.getRow()][currentNode.getColumn()], "P" + toId.toString())) {
                log.info("TO targeted node" + currentNode);
                for (Node node : nodes) {
                    String nodeString = node.toString();
                    if (nodeString.contains("row=" + currentNode.getRow() + ", column=" + currentNode.getColumn())) {
                        System.out.println(node);
                        solution.add(new Coordinate(node.getRow(), node.getColumn()));
                    }

                }
                return solution;
            }
            // moving up
            if (isValid(currentNode.getRow() - 1, currentNode.getColumn(), mapRepresentation, visited, toId)) {
                Node newNode = new Node(currentNode.getRow() - 1, currentNode.getColumn(), currentNode.getDistance() + 1, new ArrayList<Node>());
                queue.add(newNode);
                visited[currentNode.getRow() - 1][currentNode.getColumn()] = true;
                currentNode.getNeighbours().add(newNode);
            }

            // moving down
            if (isValid(currentNode.getRow() + 1, currentNode.getColumn(), mapRepresentation, visited, toId)) {
                Node newNode = new Node(currentNode.getRow() + 1, currentNode.getColumn(), currentNode.getDistance() + 1, new ArrayList<Node>());
                queue.add(newNode);
                visited[currentNode.getRow() + 1][currentNode.getColumn()] = true;
                currentNode.getNeighbours().add(newNode);
            }

            // moving left
            if (isValid(currentNode.getRow(), currentNode.getColumn() - 1, mapRepresentation, visited, toId)) {
                Node newNode = new Node(currentNode.getRow(), currentNode.getColumn() - 1, currentNode.getDistance() + 1, new ArrayList<Node>());
                queue.add(newNode);
                visited[currentNode.getRow()][currentNode.getColumn() - 1] = true;
                currentNode.getNeighbours().add(newNode);
            }

            // moving right
            if (isValid(currentNode.getRow(), currentNode.getColumn() + 1, mapRepresentation, visited, toId)) {
                Node newNode = new Node(currentNode.getRow(), currentNode.getColumn() + 1, currentNode.getDistance() + 1, new ArrayList<Node>());
                queue.add(newNode);
                visited[currentNode.getRow()][currentNode.getColumn() + 1] = true;
                currentNode.getNeighbours().add(newNode);
            }
        }
        return solution;
    }

    private static boolean isValid(int x, int y, String[][] grid, boolean[][] visited, Integer toId) {
        return x >= 0 && y >= 0 && x < grid.length && y < grid[0].length && (grid[x][y].contains("CCCC") || grid[x][y].equals("P" + toId.toString())) && !visited[x][y];
    }

    private String[][] createMap(MapInformation mapInformation) {
        Integer id_building = mapInformation.getId_building();
        Integer id_floor = mapInformation.getId_floor();
        Integer corridor = corridorRepository.getNumberOfCorridor(mapInformation.getId_floor());
        int numberOfSpacesByArea = 6;
        if (corridor.equals(2)) {
            numberOfSpacesByArea = 5;
        }
        String[][] mapRepresentation = new String[5][12];
        List<Space> northWestSpaces = spaceRepository.getSpaceByArea(id_building, id_floor, mapInformation.getId_nw());
        List<Space> northEastSpaces = spaceRepository.getSpaceByArea(id_building, id_floor, mapInformation.getId_ne());
        List<Space> southWestSpaces = spaceRepository.getSpaceByArea(id_building, id_floor, mapInformation.getId_sw());
        List<Space> southEastSpaces = spaceRepository.getSpaceByArea(id_building, id_floor, mapInformation.getId_se());
        int c = 0;
        createArea(corridor, mapRepresentation, northWestSpaces, c, "NW");
        if (corridor.equals(2)) {
            mapRepresentation[0][numberOfSpacesByArea] = "CCCC";
            mapRepresentation[1][numberOfSpacesByArea] = "CCCC";
            mapRepresentation[0][numberOfSpacesByArea + 1] = "CCCC";
            mapRepresentation[1][numberOfSpacesByArea + 1] = "CCCC";
            c = numberOfSpacesByArea + 2;
        } else {
            c = numberOfSpacesByArea;
        }

        createArea(corridor, mapRepresentation, northEastSpaces, c, "NE");
        for (int k = 0; k < 12; k++) {
            mapRepresentation[2][k] = "CCCC";
        }
        c = 0;
        createArea(corridor, mapRepresentation, southWestSpaces, c, "SW");
        if (corridor.equals(2)) {
            mapRepresentation[3][numberOfSpacesByArea] = "CCCC";
            mapRepresentation[4][numberOfSpacesByArea] = "CCCC";
            mapRepresentation[3][numberOfSpacesByArea + 1] = "CCCC";
            mapRepresentation[4][numberOfSpacesByArea + 1] = "CCCC";
            c = numberOfSpacesByArea + 2;
        } else {
            c = numberOfSpacesByArea;
        }
        createArea(corridor, mapRepresentation, southEastSpaces, c, "SE");
        displayMap(mapRepresentation);
        return mapRepresentation;
    }

    private void displayMap(String[][] mapRepresentation) {
        //Display
        log.info("Displaying map for searching target");
        log.info("-----------------------------------");
        for (String[] strings : mapRepresentation) {
            for (int j = 0; j < strings.length; j++) {
                System.out.print("|" + strings[j]);
            }
            System.out.print("|");
            System.out.println();
        }
        log.info("-----------------------------------");
    }

    private String[][] createArea(Integer corridor, String[][] mapRepresentation, List<Space> Spaces, int c, String localisation) {
        int l1;
        int l2;
        if (localisation.contains("N")) {
            l1 = 0;
            l2 = 1;
        } else {
            l1 = 3;
            l2 = 4;
        }
        for (Space space : Spaces) {
            if (space.getType().contains("Individual Office") && (space.getPosition_in_area().equals(1) || space.getPosition_in_area().equals(5))) {
                mapRepresentation[l1][c] = "P" + space.getId_space().toString();
            } else if (space.getType().contains("Individual Office") && (space.getPosition_in_area().equals(2) || space.getPosition_in_area().equals(6))) {
                mapRepresentation[l2][c - 1] = "P" + space.getId_space().toString();
                c--;
            } else if (space.getType().contains("Kitchen") || space.getType().contains("Meeting room")) {
                if (localisation.contains("N")) {
                    mapRepresentation[l1][c] = "*" + space.getId_space().toString();
                    mapRepresentation[l2][c] = "*" + space.getId_space().toString();
                    mapRepresentation[l1][c + 1] = "*" + space.getId_space().toString();
                    mapRepresentation[l2][c + 1] = "P" + space.getId_space().toString();
                    if (corridor.equals(1)) {
                        mapRepresentation[l1][c + 2] = "*" + space.getId_space().toString();
                        mapRepresentation[l2][c + 2] = "*" + space.getId_space().toString();
                    }
                } else if (localisation.contains("S")) {
                    mapRepresentation[l1][c] = "*" + space.getId_space().toString();
                    mapRepresentation[l2][c] = "*" + space.getId_space().toString();
                    mapRepresentation[l1][c + 1] = "P" + space.getId_space().toString();
                    mapRepresentation[l2][c + 1] = "*" + space.getId_space().toString();
                    if (corridor.equals(1)) {
                        mapRepresentation[l1][c + 2] = "*" + space.getId_space().toString();
                        mapRepresentation[l2][c + 2] = "*" + space.getId_space().toString();
                    }
                }
                if (corridor.equals(1)) {
                    c += 2;
                } else {
                    c++;
                }

            } else if (space.getType().contains("Open Space") && localisation.contains("W")) {
                for (int i = c; i < 6; i++) {
                    mapRepresentation[l1][i] = "*" + space.getId_space().toString();
                    mapRepresentation[l2][i] = "*" + space.getId_space().toString();
                }
            } else if (space.getType().contains("Open Space") && localisation.contains("E")) {
                for (int i = c; i < 12; i++) {
                    mapRepresentation[l1][i] = "*" + space.getId_space().toString();
                    mapRepresentation[l2][i] = "*" + space.getId_space().toString();
                }
            } else if (localisation.contains("N")) {
                mapRepresentation[l1][c] = "*" + space.getId_space().toString();
                mapRepresentation[l2][c] = "P" + space.getId_space().toString();
            } else if (localisation.contains("S")) {
                mapRepresentation[l1][c] = "P" + space.getId_space().toString();
                mapRepresentation[l2][c] = "*" + space.getId_space().toString();
            }
            if (space.getType().contains("Open Space") && localisation.contains("NW")) {
                mapRepresentation[l2][2] = "P" + space.getId_space().toString();
            } else if (space.getType().contains("Open Space") && localisation.contains("NE")) {
                mapRepresentation[l2][9] = "P" + space.getId_space().toString();
            } else if (space.getType().contains("Open Space") && localisation.contains("SW")) {
                mapRepresentation[l1][2] = "P" + space.getId_space().toString();
            } else if (space.getType().contains("Open Space") && localisation.contains("SE")) {
                mapRepresentation[l1][9] = "P" + space.getId_space().toString();
            }
            c++;
        }
        return mapRepresentation;
    }

}
