package episen.pds.checkmate.backendcheckmate.model.smartgrid;

import java.sql.Timestamp;

public class Season {
    protected Integer solar;
    protected Integer wind;
    protected String schedule;

    public Integer getSolar() {
        return solar;
    }

    public Integer getWind() {
        return wind;
    }

    public String getSchedule() {
        return schedule;
    }
}
