package episen.pds.checkmate.backendcheckmate.service.equipment;

import episen.pds.checkmate.backendcheckmate.dao.equipment.WashingMachineRepository;
import episen.pds.checkmate.backendcheckmate.model.equipment.WashingMachine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WashingMachineService {
    @Autowired
    private final WashingMachineRepository washingmachineRepository;

    public WashingMachineService(WashingMachineRepository washingmachineRepository) {
        this.washingmachineRepository = washingmachineRepository;

    }

    public void setWashingMachine(String washingmachineMode,Integer id) {
        washingmachineRepository.setWashingMachine(washingmachineMode, id);
    }

    public Optional<WashingMachine> selectWashingMachine(Integer id) {
        return washingmachineRepository.findWashingMachine(id);
    }

    public void getWashingMachine( String washingmachineMode, Integer id) {
        washingmachineRepository.getWashingMachineMode(id);
    }
}

