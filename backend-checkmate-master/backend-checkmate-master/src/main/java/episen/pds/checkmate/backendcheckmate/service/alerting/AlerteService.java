package episen.pds.checkmate.backendcheckmate.service.alerting;


import episen.pds.checkmate.backendcheckmate.dao.alerting.AlerteRepository;
import episen.pds.checkmate.backendcheckmate.model.alerting.Alerte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AlerteService {

    private final AlerteRepository alerteRepository;

    @Autowired
    public AlerteService(AlerteRepository alerteRepository) {
        this.alerteRepository = alerteRepository;
    }

    public  void addAlerte(Alerte alerte) {
       alerteRepository.save(alerte);
    }

    public  Optional<Alerte> getAlerteById(Integer id) {
        return alerteRepository.findById(id);
    }

    public  List<Alerte> getAlertes() {
        return alerteRepository.findAll();
    }
}
