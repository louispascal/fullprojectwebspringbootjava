package episen.pds.checkmate.backendcheckmate.service.equipment;

import episen.pds.checkmate.backendcheckmate.dao.equipment.HeatingRepository;
import episen.pds.checkmate.backendcheckmate.model.equipment.Heating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.lang.Thread.sleep;

@Service
public class HeatingService {
    private int id = 1;
    @Autowired
    private HeatingRepository heatingRepository;

    public HeatingService(HeatingRepository heatingRepository) {
        this.heatingRepository = heatingRepository;

    }

    public void setHeating(Heating heating) {
        heatingRepository.setHeating(heating.getValue(), heating.getMode(),heating.getId_equipment());
    }

    public Optional<Heating> selectHeating(Integer id_equipment) {
        return heatingRepository.findHeating(id_equipment);
    }

    public void getHeating(Integer heatingValue, String heatingMode, Integer id_equipment) {
        heatingRepository.getHeatingValue(id_equipment);
        heatingRepository.getHeatingMode(id_equipment);
    }

    public Integer autoHeating(Integer id_equipment) throws InterruptedException {
        Integer heatAutoValue = 20;
        System.out.println(id_equipment);
        Integer outsideHeat = heatingRepository.getCaptorValues();
        Integer heatingTemperature = heatingRepository.getHeatingValue(id_equipment);
        for (int i = 0; i < 20; i++) {
            if (outsideHeat <15){
                heatAutoValue = 19;
            }
            if(outsideHeat>27){
                heatAutoValue = 21;

            }
            if(outsideHeat<=27 && outsideHeat>= 15){
                heatAutoValue = (outsideHeat + heatingTemperature)/2;
            }
            System.out.println(heatAutoValue);
            heatingRepository.setWithAutoMode(heatAutoValue,id_equipment);
            sleep(5000);
        }
        return heatAutoValue;
    }
}


