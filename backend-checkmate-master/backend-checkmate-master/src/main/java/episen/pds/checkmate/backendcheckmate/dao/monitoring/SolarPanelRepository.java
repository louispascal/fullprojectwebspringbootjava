package episen.pds.checkmate.backendcheckmate.dao.monitoring;

import episen.pds.checkmate.backendcheckmate.model.monitoring.Consumption;
import episen.pds.checkmate.backendcheckmate.model.monitoring.SolarPanel;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ines
 */
@Repository
public interface SolarPanelRepository extends JpaRepository<SolarPanel, Integer> {
    
    @Query(value = "SELECT * from energy.solar_panel s INNER JOIN energy.building b ON s.id_building = b.id_building WHERE b.id_building = ?1", nativeQuery = true)
    List<SolarPanel> findByIdBuilding(int idBuilding);
    
    @Query(value = "SELECT instantaneous_power FROM energy.production_solar_panel WHERE id_solar_panel = ?1 AND sampling_date = (SELECT Max(sampling_date) FROM energy.production_solar_panel WHERE id_solar_panel =  ?1)", nativeQuery = true)
    Double findValueMaxDateByIdSolarPanel(int idSolarPanel);
       
}
