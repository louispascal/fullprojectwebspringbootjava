package episen.pds.checkmate.backendcheckmate.model.analyse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity@Builder
@Table(schema = "analyze_schema")
public class BuildingAnalyze {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @ManyToOne
    private DistrictAnalyze district;

    @OneToMany(mappedBy = "building")
    private List<HomeAnalyze> homes = new ArrayList<>();
}
