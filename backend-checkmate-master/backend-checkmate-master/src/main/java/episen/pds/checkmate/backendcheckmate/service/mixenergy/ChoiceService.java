package episen.pds.checkmate.backendcheckmate.service.mixenergy;
import episen.pds.checkmate.backendcheckmate.dao.mixenergy.ChoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChoiceService {
    private final ChoiceRepository choiceRepository;
    @Autowired
    public ChoiceService(ChoiceRepository choiceRepository) {
        this.choiceRepository = choiceRepository;
    }
    public void postEnergyPreference(Integer solar,Integer hydraulic, Integer wind){
       choiceRepository.postEnergyPreference(solar,hydraulic,wind);

    }
    public void postEnergyProportion(Integer solarProportion ,Integer hydraulicProportion , Integer windProportion){
        choiceRepository.postEnergyProportion(solarProportion,hydraulicProportion,windProportion);
    }
}
