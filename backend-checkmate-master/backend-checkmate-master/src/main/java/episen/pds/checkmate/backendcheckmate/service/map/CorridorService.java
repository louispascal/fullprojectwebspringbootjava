package episen.pds.checkmate.backendcheckmate.service.map;

import episen.pds.checkmate.backendcheckmate.dao.map.CorridorRepository;
import episen.pds.checkmate.backendcheckmate.model.map.Corridor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class CorridorService {
    private final CorridorRepository corridorRepository;

    @Autowired
    public CorridorService(CorridorRepository corridorRepository) {
        this.corridorRepository = corridorRepository;
    }

    public List<Corridor> getCorridorsFromFloor(Integer id_building, Integer id_floor) {
        List<Corridor> corridorList = corridorRepository.getCorridorsByFloor(id_building, id_floor);
        log.info("Getting corridors from specified id_floor (" + id_floor + ") : " + corridorList.toString());
        return corridorList;
    }

    public void saveCorridor(Corridor corridor) {
        log.info("Saving corridor :" + corridor);
        corridorRepository.save(corridor);
    }
}