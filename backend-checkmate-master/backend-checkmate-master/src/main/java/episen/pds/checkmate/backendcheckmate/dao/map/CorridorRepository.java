package episen.pds.checkmate.backendcheckmate.dao.map;

import episen.pds.checkmate.backendcheckmate.model.map.Corridor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CorridorRepository extends JpaRepository<Corridor, Integer> {
    @Query(nativeQuery = true)
    List<Corridor> getCorridorsByFloor(Integer id_building, Integer id_floor);

    @Query(nativeQuery = true)
    Integer getNumberOfCorridor(Integer id_floor);
}
