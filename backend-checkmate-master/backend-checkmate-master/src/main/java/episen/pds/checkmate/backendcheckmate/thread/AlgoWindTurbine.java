package episen.pds.checkmate.backendcheckmate.thread;

import episen.pds.checkmate.backendcheckmate.model.monitoring.ProductionWindTurbine;
import episen.pds.checkmate.backendcheckmate.model.monitoring.WindTurbine;
import episen.pds.checkmate.backendcheckmate.service.monitoring.ProductionWindTurbineService;
import episen.pds.checkmate.backendcheckmate.service.monitoring.WindTurbineService;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Slf4j
@Component
public class AlgoWindTurbine implements Runnable {

    private final WindTurbineService windTurbineService;
    private final ProductionWindTurbineService productionWindTurbineService;

    @Autowired
    public AlgoWindTurbine(WindTurbineService windTurbineEnergyService, ProductionWindTurbineService productionWindTurbineService) {
        this.windTurbineService = windTurbineEnergyService;
        this.productionWindTurbineService = productionWindTurbineService;
    }

    public static double nextDoubleBetween(double min, double max) {
        // java 8 + DoubleStream
        return new Random().doubles(min, max).limit(1).findFirst().getAsDouble();
    }

    @Override
    public void run() {
        List<WindTurbine> windTurbineList = windTurbineService.selectAllWindTurbine();
        System.out.println("+++++++++++Add Production Wind Turbine++++++++");
        while (true) {
            for (WindTurbine windTurbine : windTurbineList) {
                ProductionWindTurbine productionWindTurbine = new ProductionWindTurbine();
                productionWindTurbine.setWindTurbine(windTurbine);
                productionWindTurbine.setInstantaneousPower(nextDoubleBetween(100, 120));
                productionWindTurbine.setSamplingDate(new Date());
                System.out.println("windTurbine--" + windTurbine.getIdWindTurbine() + " Production " + productionWindTurbine.getInstantaneousPower());
                productionWindTurbineService.addProductionWindTurbine(productionWindTurbine);
            }
            try {
                Thread.sleep(60 * 1000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }

        }
    }
}
