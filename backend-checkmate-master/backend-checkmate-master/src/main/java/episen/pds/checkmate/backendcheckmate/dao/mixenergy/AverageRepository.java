package episen.pds.checkmate.backendcheckmate.dao.mixenergy;

import episen.pds.checkmate.backendcheckmate.model.mixenergy.Average_production_per_month;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AverageRepository extends JpaRepository<Average_production_per_month, Integer> {
    @Query(value ="SELECT production_per_month FROM mock.average_production_per_month where type_of_energy=?"
            ,nativeQuery = true)
    List<Integer> getAverage_prod(String energy_type);

}
