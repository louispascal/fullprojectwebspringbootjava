package episen.pds.checkmate.backendcheckmate.api;

import episen.pds.checkmate.backendcheckmate.model.scope8.Reservation;
import episen.pds.checkmate.backendcheckmate.model.workspace.Access;
import episen.pds.checkmate.backendcheckmate.model.workspace.Employee;
import episen.pds.checkmate.backendcheckmate.model.workspace.Message;
import episen.pds.checkmate.backendcheckmate.service.scope8.ReservationService;
import episen.pds.checkmate.backendcheckmate.service.workspace.EmployeeService;
import episen.pds.checkmate.backendcheckmate.service.workspace.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "user")
@RestController
public class UserController {
    private final EmployeeService employeeService;
    private final MessageService messageService;
    private final ReservationService reservationService;

    @Autowired
    public UserController(EmployeeService employeeService, MessageService messageService, ReservationService reservationService) {
        this.employeeService = employeeService;
        this.messageService = messageService;
        this.reservationService = reservationService;
    }

    @PostMapping("/createReservation")
    public void addReservation(@RequestBody Reservation reservation) {
        reservationService.addReservation(reservation);
    }

    @PostMapping("/createEmployee")
    public void addEmployee(@RequestBody Employee employee) {
        employeeService.addEmployee(employee);
    }

    @GetMapping("/readEmployee/{id}")
    public List<Employee> getEmployees(@PathVariable Integer id) {
        return employeeService.selectAllEmployee(id);
    }

    @GetMapping(path = "/readEmployeebyid/{id}")
    public Optional<Employee> getEmployee(@PathVariable Integer id) {return employeeService.getEmployeeById(id);}

    @PutMapping("/updateEmployee")
    public void updateEmployee(@RequestBody Employee employee) {
        employeeService.updateEmployee(employee);
    }

    @DeleteMapping(path = "/deleteEmployeebyid/{id}")
    public void deleteEmployee(@PathVariable Integer id) {employeeService.deleteEmployee(id);}

    @GetMapping(path="/loginEmployee/{email}")
    public Optional<Employee> loginEmployee(@PathVariable String email) {return employeeService.loginEmployee(email);}

    @GetMapping("/getMessages/{ids}")
    public List<Message> getMessages(@PathVariable String ids) {
        return messageService.getMessages(Integer.parseInt(ids.split(",")[0]),Integer.parseInt(ids.split(",")[1]));
    }

    @PostMapping("/sendMessage")
    public void sendMessage(@RequestBody Message message) {
        messageService.sendMessage(message);
    }

    @GetMapping("/getAccess/{id_access}")
    public Optional<Access> getAccess(@PathVariable Integer id_access) {
        return employeeService.getAccess(id_access);
    }

    @GetMapping("/meetingParticipants/{id}")
    public List<Employee> getMeetingParticipants(@PathVariable Integer id) {
        return employeeService.getMeetingParticipants(id);
    }
}

