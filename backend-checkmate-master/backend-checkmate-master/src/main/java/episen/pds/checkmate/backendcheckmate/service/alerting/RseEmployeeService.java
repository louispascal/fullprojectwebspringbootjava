package episen.pds.checkmate.backendcheckmate.service.alerting;

import episen.pds.checkmate.backendcheckmate.dao.alerting.RseEmployeeRepository;
import episen.pds.checkmate.backendcheckmate.model.alerting.RseEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RseEmployeeService {

     private RseEmployeeRepository rseEmployeeRepository;

    @Autowired
    public RseEmployeeService(RseEmployeeRepository rseEmployeeRepository) {
        this.rseEmployeeRepository = rseEmployeeRepository;
    }

    public  void addRseEmployee(RseEmployee rseEmployee) {
        rseEmployeeRepository.save(rseEmployee);
    }

    public List<RseEmployee> selectAllRseEmployees() {
        return rseEmployeeRepository.findAll();
    }

    public  Optional<RseEmployee> getRseEmployeeById(Integer id) {
        return rseEmployeeRepository.findById(id);
    }

    public  void updateRseEmployee(RseEmployee rseEmployee) {
        rseEmployeeRepository.save(rseEmployee);
    }

    public  void deleteRseEmployee(Integer id) {
    }

    public Optional<RseEmployee> loginRseEmployee(String email) {
        return rseEmployeeRepository.login(email);
    }
}
