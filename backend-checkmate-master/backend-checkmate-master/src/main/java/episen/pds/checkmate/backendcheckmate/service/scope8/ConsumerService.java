package episen.pds.checkmate.backendcheckmate.service.scope8;

import episen.pds.checkmate.backendcheckmate.dao.scope8.ConsumerRepository;
import episen.pds.checkmate.backendcheckmate.model.scope8.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ConsumerService {
   private final ConsumerRepository consumerRepository;


    @Autowired
    public ConsumerService(ConsumerRepository consumerRepository) {
        this.consumerRepository = consumerRepository;

    }




    public Optional<Consumer> getConsumerById(Integer id) {
        return consumerRepository.findById(id);
    }





}