package episen.pds.checkmate.backendcheckmate.service.smartgrid;

import episen.pds.checkmate.backendcheckmate.dao.mixenergy.AverageRepository;
import episen.pds.checkmate.backendcheckmate.dao.mixenergy.MixRepository;
import episen.pds.checkmate.backendcheckmate.dao.smartgrid.EnergyRepository;
import episen.pds.checkmate.backendcheckmate.dao.smartgrid.SiteRepository;
import episen.pds.checkmate.backendcheckmate.model.mixenergy.Site_of_Production;
import episen.pds.checkmate.backendcheckmate.model.smartgrid.District;
import episen.pds.checkmate.backendcheckmate.service.mixenergy.MixService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Slf4j
@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class EnergyServiceTest {
    @Autowired
    private EnergyRepository energyRepository;
    @Autowired
    private SiteRepository siteRepository;
    @InjectMocks
    private EnergyService underTest;

    @BeforeEach
    void setUp() {
        underTest = new EnergyService(energyRepository,siteRepository);
    }

    @Test
    void testgetAllDistricts(){
        //GIVEN
        List<Integer> allDistricts= new ArrayList<>();
        //WHEN
        allDistricts = underTest.getAllDistricts();
        log.info("allDistricts"+allDistricts);
        //THEN
        assertThat(allDistricts).isNotNull(); //INFORMATION ABOUT THE NUMBER OF DISTRICTS

    }
    @Test
    void testgetDistrictInfosById(){
        //GIVEN
        Optional<District> getDistrictInfosById;
        int infoDistrict = 1;
        //WHEN
        getDistrictInfosById = underTest.getDistrictInfosById(infoDistrict);
        log.info("districtInfosByID"+getDistrictInfosById);
        //THEN
        assertThat(getDistrictInfosById).isNotNull(); // INFORMATION ABOUT THE ID,CONSUMPTION,PRODUCTION AND SAMPLING_DATE

    }
    @Test
    void testgetSite_of_Production(){
        //GIVEN
        List<Site_of_Production> getSite_of_Production;
        //WHEN
        getSite_of_Production = underTest.getSite_of_Production();
        log.info("getSite_of_Production"+getSite_of_Production);
        //THEN
        assertThat(getSite_of_Production).isNotNull(); // INFORMATION ABOUT THE SITE OF PRODUCTION WITH ID OF SITE,MAX CAPACITY AND TYPE OF ENERGY FOR THE SITE

    }
}






