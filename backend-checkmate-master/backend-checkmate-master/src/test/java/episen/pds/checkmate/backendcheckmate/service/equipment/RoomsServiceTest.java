package episen.pds.checkmate.backendcheckmate.service.equipment;

import episen.pds.checkmate.backendcheckmate.dao.equipment.RoomsRepository;
import episen.pds.checkmate.backendcheckmate.model.equipment.Rooms;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
public class RoomsServiceTest {
    @Autowired
    private RoomsRepository roomsRepository;
    @InjectMocks
    private RoomsService roomsService;

    @BeforeEach
    public void setUp() throws Exception {
        roomsService = new RoomsService(roomsRepository);
    }

    @Test
    public void RoomsAreTheSame(){
        //
        // GIVEN
        //
        Integer id_citizen = 99;
        //
        // WHEN
        //
        Iterable<Rooms> r1 = roomsRepository.getRoomByUserId(id_citizen);
        Iterable<Rooms> r2 = roomsService.selectRoomByCitizen(id_citizen);
        //
        // THEN
        //
        Assert.assertEquals(r1,r2);
    }
}
