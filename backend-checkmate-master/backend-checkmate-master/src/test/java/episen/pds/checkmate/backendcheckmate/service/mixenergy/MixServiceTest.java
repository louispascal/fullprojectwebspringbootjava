package episen.pds.checkmate.backendcheckmate.service.mixenergy;

import episen.pds.checkmate.backendcheckmate.dao.mixenergy.AverageRepository;
import episen.pds.checkmate.backendcheckmate.dao.mixenergy.MixRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Slf4j
@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class MixServiceTest {
    @Autowired
    private MixRepository mixRepository;
    @Autowired
    private AverageRepository averageRepository;
    @InjectMocks
    private MixService underTest;

    @BeforeEach
    void setUp() {
        underTest = new MixService(
                mixRepository,averageRepository
        );
    }

    @Test
    void testgetEconomicRecommendations(){
        //given
        int energyToBeProduced=8000;
        //when
        HashMap<String, List<String>> expectedRecommandations = underTest.getEconomicRecommendations(energyToBeProduced);
        List<String> expectedRecommandationList = new ArrayList<>();
        log.info("expectedRecommandations :"+expectedRecommandations);
        expectedRecommandationList=expectedRecommandations.get("recommendation");
        int sum = Integer.valueOf(expectedRecommandationList.get(1))+Integer.valueOf(expectedRecommandationList.get(3))+Integer.valueOf(expectedRecommandationList.get(5))+Integer.valueOf(expectedRecommandationList.get(7));
        // Capacity of all solar sites
        int solarCapacity = underTest.getCurrentCapacity("solaire") - underTest.getInstantaneousPower("solaire");
        // Capacity of all wind sites
        int windCapacity = underTest.getCurrentCapacity("éolienne") - underTest.getInstantaneousPower("éolienne");
        // Capacity of all hydraulic sites
        int hydraulicCapacity = underTest.getCurrentCapacity("hydraulique") - underTest.getInstantaneousPower("hydraulique");
        int sum_renewal_energy = Integer.valueOf(expectedRecommandationList.get(1))+Integer.valueOf(expectedRecommandationList.get(3))+Integer.valueOf(expectedRecommandationList.get(5));
        //then
        assertThat(sum).isEqualTo(energyToBeProduced);
        assertThat(expectedRecommandations).isNotNull(); // assert that there is a recommendation
        assertThat(sum_renewal_energy).isLessThanOrEqualTo(solarCapacity+windCapacity+hydraulicCapacity);

    }
    @Test
    void testgetEconomicData(){
        //given
        HashMap<String, List<Float>> graphData = new HashMap<>();
        HashMap<String, List<Float>>  expectedData = new HashMap<>();
        //when
        expectedData=underTest.getEconomicData();
        log.info("expectedData"+expectedData);
        //then
        assertThat(expectedData).isNotEqualTo(graphData);
        assertThat(expectedData).isNotEqualTo(graphData);
    }

    @Test
    void testgetCurrentCapacity() {
        //given
        int currentSolarCapacity,currentHydraulicCapacity,currentWindCapacity;
        //when
        currentSolarCapacity = underTest.getCurrentCapacity("solaire");
        currentHydraulicCapacity = underTest.getCurrentCapacity("hydraulique");
        currentWindCapacity = underTest.getCurrentCapacity("éolienne");
        log.info("expectedSolarCapacity"+ currentSolarCapacity);
        log.info("expectedHydraulicCapacity"+ currentHydraulicCapacity);
        log.info("expectedDataWindCapacity"+ currentWindCapacity);
        //then
        assertThat(currentSolarCapacity).isGreaterThanOrEqualTo(0);
        assertThat(currentHydraulicCapacity).isGreaterThanOrEqualTo(0);
        assertThat(currentWindCapacity).isGreaterThanOrEqualTo(0);

    }
    @Test
    void testgetInstantaneousPower() {
        //given
        int currentSolarPower,currentWindPower,currentHydraulicPower;
        //When
        currentSolarPower = underTest.getCurrentCapacity("solaire");
        currentHydraulicPower = underTest.getCurrentCapacity("hydraulique");
        currentWindPower= underTest.getCurrentCapacity("éolienne");
        log.info("currentSolarPower"+ currentSolarPower);
        log.info("currentHydraulicPower"+ currentHydraulicPower);
        log.info("currentWindPower"+ currentWindPower);
        //then
        assertThat(currentSolarPower).isGreaterThanOrEqualTo(0);
        assertThat(currentHydraulicPower).isGreaterThanOrEqualTo(0);
        assertThat(currentWindPower).isGreaterThanOrEqualTo(0);

    }

    
}

