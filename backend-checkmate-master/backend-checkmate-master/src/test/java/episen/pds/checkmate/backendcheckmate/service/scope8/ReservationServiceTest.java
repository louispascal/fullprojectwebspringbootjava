package episen.pds.checkmate.backendcheckmate.service.scope8;

import episen.pds.checkmate.backendcheckmate.dao.scope8.ReservationRepository;
import episen.pds.checkmate.backendcheckmate.dao.workspace.AccessRepository;
import episen.pds.checkmate.backendcheckmate.dao.workspace.EmployeeRepository;
import episen.pds.checkmate.backendcheckmate.model.scope8.Reservation;
import episen.pds.checkmate.backendcheckmate.model.workspace.Employee;
import episen.pds.checkmate.backendcheckmate.service.workspace.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;


@Slf4j
@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class ReservationServiceTest {
    @Autowired
    private ReservationRepository reservationRepository;

    @InjectMocks
    private ReservationService reservationService;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AccessRepository accessRepository;

    @InjectMocks
    private EmployeeService employeeService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        reservationService = new ReservationService(reservationRepository);
        employeeService = new EmployeeService(employeeRepository,accessRepository);
    }

    @Test
    public void ownMeetingsListIsComplete(){
        //
        // GIVEN
        //
        Integer id_employee = 2469;
        //
        // WHEN
        //
        List<Reservation> r1 = reservationRepository.getOwnMeetings(id_employee);
        List<Reservation> r2 = reservationService.getOwnMeetings(id_employee);
        //
        // THEN
        //
        Assert.assertEquals(r1,r2);
    }

    @Test
    public void amountOfParticipantsIncrementAfterInvite(){
        //
        // GIVEN
        //
        Integer id_employee = 2665;
        Integer id_reservation= 1;
        //
        // WHEN
        //
        Integer amountBefore = reservationService.getMeetingParticipants(id_reservation);
        reservationService.invite(id_reservation,id_employee);
        Integer amountAfter = reservationService.getMeetingParticipants(id_reservation);
        //
        // THEN
        //
        Assert.assertTrue(amountAfter==amountBefore+1);
    }

    @Test
    public void lessMeetingReservationThanTotalReservation(){
        //
        // GIVEN
        //
        Integer id_employee = 2469;
        //
        // WHEN
        //
        List<Reservation> r1 = reservationService.getOwnMeetings(id_employee);
        List<Reservation> r2 = reservationService.getReservationByEmployee(id_employee);
        //
        // THEN
        //
        Assert.assertTrue(r1.size()<r2.size());
    }

    @Test
    public void addReservationIncrementTotalReservation(){
        //
        // GIVEN
        //
        Integer id_employee = 2469;
        Reservation res = new Reservation(3,2469,2,"2022-05-17 08:00:00.000000","2022-05-17 09:00:00.000000");
        //
        // WHEN
        //
        List<Reservation> r1 = reservationService.getReservationByEmployee(id_employee);
        reservationService.addReservation(res);
        List<Reservation> r2 = reservationService.getReservationByEmployee(id_employee);
        //
        // THEN
        //
        Assert.assertTrue(r2.size() == r1.size()+1);
    }

    @Test
    public void addReservationMakesYouReservationAuthor(){
        //
        // GIVEN
        //
        Employee e = new Employee(2469,"Baron","Shanie","Services généraux","sbaron@etu.u-pec.fr",1,2,1);
        Reservation res = new Reservation(24,e.getId_employee(),2,"2022-05-12 08:00:00.000000","2022-05-12 09:00:00.000000");
        //
        // WHEN
        //
        String author = reservationService.getReservationAuthor(2469);
        //
        // THEN
        //
        Assert.assertTrue(author.equals(e.getFirstName()+" "+e.getName()));
    }

    @Test
    public void changeMeetingSubject(){
        //
        // GIVEN
        //
        String id_reservation = "2";
        //
        // WHEN
        //
        reservationService.subjectUpdate("petite réunion",id_reservation);
        String previousSubject = reservationService.getMeetingSubject(Integer.valueOf(id_reservation));
        reservationService.subjectUpdate("réunion importante",id_reservation);
        String currentSubject = reservationService.getMeetingSubject(Integer.valueOf(id_reservation));
        //
        // THEN
        //
        Assert.assertTrue(previousSubject != currentSubject);
    }

}