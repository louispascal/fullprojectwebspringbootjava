package episen.pds.checkmate.backendcheckmate.service.map;

import episen.pds.checkmate.backendcheckmate.dao.map.AreaRepository;
import episen.pds.checkmate.backendcheckmate.dao.map.CorridorRepository;
import episen.pds.checkmate.backendcheckmate.dao.map.SpaceRepository;
import episen.pds.checkmate.backendcheckmate.dao.map.TypeSpaceRepository;
import episen.pds.checkmate.backendcheckmate.model.map.Corridor;
import episen.pds.checkmate.backendcheckmate.model.map.MapRequest;
import episen.pds.checkmate.backendcheckmate.model.map.Space;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Slf4j
@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class MapConfigurationServiceTest {
    @Autowired
    private TypeSpaceRepository typeSpaceRepository;
    @Autowired
    private SpaceRepository spaceRepository;
    @InjectMocks
    private SpaceService spaceService;
    @Autowired
    private AreaRepository areaRepository;
    @Mock
    private CorridorRepository corridorRepository;
    @InjectMocks
    private MapConfigurationService underTest;

    @BeforeEach
    void setUp() {
        underTest = new MapConfigurationService(
                typeSpaceRepository,
                spaceService,
                spaceRepository,
                areaRepository,
                corridorRepository
        );
    }


    @Test
    void testLaunchConfigurationCreateMap() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                5,
                "1",
                0,
                0,
                0,
                0,
                24,
                false
        );
        int nb_office_double = mapRequest.getNb_office_double();
        //when
        String answer = underTest.launchConfiguration(mapRequest);
        int nbSpacesCreated = spaceRepository.countNbOfDoubleOffice(5);
        //then
        assertThat(answer).isEqualTo("Size match");
        assertThat(nbSpacesCreated).isEqualTo(nb_office_double);
        //cleaning
        underTest.clearFloor(5);
    }

    @Test
    void testLaunchConfigurationDoesNotCreateMap() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                5,
                "1",
                0,
                0,
                0,
                0,
                27,
                false
        );
        //when
        String answer = underTest.launchConfiguration(mapRequest);
        int nbSpacesCreated = spaceRepository.countNbOfDoubleOffice(5);
        boolean empty = spaceRepository.isEmpty(5);
        //then
        assertThat(answer).isEqualTo("Too many requested");
        assertThat(nbSpacesCreated).isEqualTo(0);
        assertThat(empty).isEqualTo(true);
    }

    @Test
    void testCanSaveIndividualSpace() {
        //given
        underTest.clearFloor(5);
        MapRequest mapRequest = new MapRequest(
                1,
                5,
                "2",
                3,
                2,
                0,
                0,
                4,
                false
        );
        ArrayList<String> listArea = underTest.shuffleArray();
        Integer nbOffice = mapRequest.getNb_office();
        //when
        underTest.saveIndividualOffice(listArea, mapRequest, nbOffice, 5, "Bureau simple");
        Integer nbOfIndividualOffice = spaceRepository.countNbOfIndividualOffice(5);
        log.info("Result : " + nbOfIndividualOffice);
        //then
        assertThat(nbOfIndividualOffice).isEqualTo(nbOffice);
        //cleaning
        underTest.clearFloor(5);
    }

    @Test
    void testCanSaveSpace() {
        //given
        underTest.clearFloor(5);
        MapRequest mapRequest = new MapRequest(
                1,
                5,
                "2",
                3,
                2,
                0,
                0,
                4,
                false
        );
        ArrayList<String> listArea = underTest.shuffleArray();
        Integer nbDoubleOffice = mapRequest.getNb_office_double();
        //when
        underTest.saveSpaces(listArea, mapRequest, nbDoubleOffice, 4, "Bureau double");
        Integer nbOfDoubleOffice = spaceRepository.countNbOfDoubleOffice(5);
        log.info("Result : " + nbOfDoubleOffice);
        //then
        assertThat(nbOfDoubleOffice).isEqualTo(nbDoubleOffice);
        //cleaning
        underTest.clearFloor(5);
    }

    @Test
    void launchFillingReturnANewMapWithRightSize() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                2,
                "1",
                3,
                0,
                0,
                0,
                0,
                true
        );
        //when
        underTest.getSpacesSizes(1);
        float requestedSize = underTest.getRequestedSize(mapRequest);
        MapRequest newMap = underTest.launchFilling(mapRequest, false, requestedSize, 400.0f);
        float newRequestedSize = underTest.getRequestedSize(newMap);
        //then
        assertThat(newMap).isNotEqualTo(mapRequest);
        assertThat(newRequestedSize).isGreaterThan(requestedSize);
        assertThat(newRequestedSize).isEqualTo(400.0f);
    }

    @Test
    void canCreateOneCorridor() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                1,
                "1",
                4,
                0,
                0,
                0,
                0,
                true
        );
        Corridor horizontalCorridor = new Corridor(1, "H");
        //when
        underTest.createCorridors(mapRequest);
        //then
        ArgumentCaptor<Corridor> corridorArgumentCaptor =
                ArgumentCaptor.forClass(Corridor.class);

        verify(corridorRepository)
                .save(corridorArgumentCaptor.capture());

        Corridor capturedCorridor = corridorArgumentCaptor.getValue();

        assertThat(capturedCorridor).isEqualTo(horizontalCorridor);

        //cleaning
        underTest.clearFloor(1);
    }

    @Test
    void canCreateTwoCorridors() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                1,
                "2",
                4,
                0,
                0,
                0,
                0,
                true
        );
        Corridor horizontalCorridor = new Corridor(1, "H");
        Corridor verticalCorridor = new Corridor(1, "V");
        //when
        underTest.createCorridors(mapRequest);
        //then
        ArgumentCaptor<Corridor> corridorArgumentCaptor =
                ArgumentCaptor.forClass(Corridor.class);

        verify(corridorRepository, times(2))
                .save(corridorArgumentCaptor.capture());


        List<Corridor> capturedCorridors = corridorArgumentCaptor.getAllValues();
        Assertions.assertEquals(horizontalCorridor, capturedCorridors.get(0));
        Assertions.assertEquals(verticalCorridor, capturedCorridors.get(1));
        //cleaning
        underTest.clearFloor(1);
    }

    @Test
    void clearFloorWithNotEmptyFloor() {
        //given
        log.info("We create spaces in floor with id 1");
        Corridor horizontal = new Corridor(1, "H");
        corridorRepository.save(horizontal);
        for (int i = 0; i < 4; i++) {
            Space spaceToSave = new Space(i + 1, i + 1, 1, "Espace ouvert");
            spaceRepository.save(spaceToSave);
        }
        //when
        boolean empty = spaceRepository.isEmpty(1);
        log.info("Floor should not be empty!");
        log.info("empty : " + empty);
        assertThat(empty).isEqualTo(false);
        log.info("We clear the floor");
        underTest.clearFloor(1);
        empty = spaceRepository.isEmpty(1);
        log.info("Floor should be empty!");
        log.info("empty : " + empty);
        //then
        assertThat(empty).isEqualTo(true);
    }

    @Test
    void ensureFillEmptySpaceWorksWithOpenSpace() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                1,
                "1",
                2,
                0,
                0,
                0,
                0,
                true
        );
        //when
        underTest.getSpacesSizes(1);
        Float requestedSize = underTest.getRequestedSize(mapRequest);
        Float maxSize = 400.0f;
        float difference = maxSize - requestedSize;
        MapRequest resultedMapRequest = underTest.fillEmptySpace(
                difference,
                requestedSize,
                maxSize,
                mapRequest,
                false
        );
        Float newSize = underTest.getRequestedSize(resultedMapRequest);
        //then
        assertThat(newSize).isEqualTo(maxSize);

    }

    @Test
    void ensureFillEmptySpaceWorksWithMeetingRoomOrKitchen() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                1,
                "2",
                3,
                0,
                0,
                0,
                3,
                true
        );
        //when
        underTest.getSpacesSizes(2);
        Float requestedSize = underTest.getRequestedSize(mapRequest);
        Float maxSize = 400.0f;
        float difference = maxSize - requestedSize;
        MapRequest resultedMapRequest = underTest.fillEmptySpace(
                difference,
                requestedSize,
                maxSize,
                mapRequest,
                false
        );
        Float newSize = underTest.getRequestedSize(resultedMapRequest);
        //then
        assertThat(newSize).isEqualTo(maxSize);

    }

    @Test
    void ensureFillEmptySpaceWorksWitDualOffice() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                5,
                "1",
                3,
                0,
                0,
                0,
                3,
                true
        );
        //when
        underTest.getSpacesSizes(1);
        Float requestedSize = underTest.getRequestedSize(mapRequest);
        Float maxSize = 400.0f;
        float difference = maxSize - requestedSize;
        MapRequest resultedMapRequest = underTest.fillEmptySpace(
                difference,
                requestedSize,
                maxSize,
                mapRequest,
                false
        );
        Float newSize = underTest.getRequestedSize(resultedMapRequest);
        //then
        assertThat(newSize).isEqualTo(maxSize);

    }

    @Test
    void ensureFillEmptySpaceWorksWitIndividualOffice() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                5,
                "2",
                3,
                1,
                1,
                1,
                0,
                true
        );
        //when
        underTest.getSpacesSizes(2);
        Float requestedSize = underTest.getRequestedSize(mapRequest);
        Float maxSize = 400.0f;
        float difference = maxSize - requestedSize;
        MapRequest resultedMapRequest = underTest.fillEmptySpace(
                difference,
                requestedSize,
                maxSize,
                mapRequest,
                true
        );
        Float newSize = underTest.getRequestedSize(resultedMapRequest);
        //then
        assertThat(newSize).isEqualTo(maxSize);
    }

    @Test
    void canGetRequestedSizeComputesWell() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                1,
                "1",
                2,
                2,
                3,
                0,
                3,
                false
        );
        //when
        underTest.getSpacesSizes(1);
        //then
        Float result = underTest.getRequestedSize(mapRequest);
        assertThat(result).isEqualTo(400.0f);
    }

    @Test
    void checkSizeWithOverSizingAndNoAutomaticFill() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                1,
                "1",
                5,
                0,
                0,
                0,
                0,
                false
        );
        Float maxSize = 400.0f;
        //when
        underTest.getSpacesSizes(1);
        Float sizeRequested = underTest.getRequestedSize(mapRequest);
        String result = underTest.checkSize(mapRequest, sizeRequested, maxSize);
        //then
        assertThat(result).isEqualTo("Too many requested");
    }

    @Test
    void checkSizeWithUnderSizingAndNoAutomaticFill() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                1,
                "1",
                2,
                0,
                0,
                0,
                0,
                false
        );
        Float maxSize = 400.0f;
        //when
        underTest.getSpacesSizes(1);
        Float sizeRequested = underTest.getRequestedSize(mapRequest);
        String result = underTest.checkSize(mapRequest, sizeRequested, maxSize);
        //then
        assertThat(result).isEqualTo("Need more");
    }

    @Test
    void checkSizeWithMatchingSizeWithOneCorridor() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                1,
                "1",
                4,
                0,
                0,
                0,
                0,
                false
        );
        Float maxSize = 400.0f;
        //when
        underTest.getSpacesSizes(1);
        Float sizeRequested = underTest.getRequestedSize(mapRequest);
        String result = underTest.checkSize(mapRequest, sizeRequested, maxSize);
        //then
        assertThat(result).isEqualTo("Size match");
    }

    @Test
    void checkSizeWithMatchingSizeWithTwoCorridor() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                1,
                "2",
                3,
                2,
                0,
                0,
                4,
                false
        );
        Float maxSize = 400.0f;
        //when
        underTest.getSpacesSizes(2);
        Float sizeRequested = underTest.getRequestedSize(mapRequest);
        String result = underTest.checkSize(mapRequest, sizeRequested, maxSize);
        //then
        assertThat(result).isEqualTo("Size match");
    }

    @Test
    void checkSizeWithUnderSizingAndAutomaticFill() {
        //given
        MapRequest mapRequest = new MapRequest(
                1,
                1,
                "1",
                1,
                0,
                2,
                0,
                0,
                true
        );
        Float maxSize = 400.0f;
        //when
        underTest.getSpacesSizes(1);
        Float sizeRequested = underTest.getRequestedSize(mapRequest);
        String result = underTest.checkSize(mapRequest, sizeRequested, maxSize);
        //then
        assertThat(result).isEqualTo("Size match with filling");
    }

    @Test
    void checkShuffleArrayReturnArrayList() {
        //when
        ArrayList<String> arrayList = underTest.shuffleArray();
        for (String area : arrayList) {
            Assertions.assertNotNull(area);
        }

    }

}