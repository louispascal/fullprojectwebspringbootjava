package episen.pds.checkmate.backendcheckmate.service.analyse;



import episen.pds.checkmate.backendcheckmate.dao.analyse.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.jupiter.api.BeforeEach;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@Slf4j
@ExtendWith(MockitoExtension.class)
@RunWith(SpringRunner.class)
@SpringBootTest
class DashboardServiceTest {

    @InjectMocks
    DashboardService underTest;

    @Autowired
    private IBuildingAnalyzeRepository buildingAnalyzeRepository;
    @Autowired
    private IConsumptionAnalyzeRepository consumptionAnalyzeRepository;
    @Autowired
    private IDistrictAnalyzeRepository districtAnalyzeRepository;
    @Autowired
    private IEnergySourceRepository energySourceRepository;
    @Autowired
    private IEquipmentAnalyzeRepository equipmentRepository;
    @Autowired
    private IHomeAnalyzeRepository homeRepository;
    @Autowired
    private IHomeEquipmentRepository homeEquipmentRepository;
    @Autowired
    private IProductionAnalyzeRepository productionRepository;
    @PersistenceContext
    EntityManager entityManager;

    @BeforeEach
    void setUp() {
        underTest = new DashboardService(entityManager,
                buildingAnalyzeRepository,
                consumptionAnalyzeRepository,
                districtAnalyzeRepository,
                energySourceRepository,
                equipmentRepository,
                homeRepository,
                homeEquipmentRepository,
                productionRepository);
    }

    @Test
    public void canGetStatisticsWorkFine() throws Exception {
        //when
        var result = underTest.getStatistics();
        //then
        assertThat(result.size()).isEqualTo(14);

    }

    @Test
    public void canGetStatisticsFirstValueIs3f() throws Exception {
        //when
        var result = underTest.getStatistics();
        //then
        //{"id":0,"title":"Nombre de quartiers","value":3.0
        assertThat(result.get(0).getValue()).isEqualTo(3.0f);
    }

    @Test
    public void isGetProductionAndConsumptionPerDistrictWorkFine() throws Exception {
        //when
        var result = underTest.getProductionAndConsumptionPerDistrict();
        //then
        //{"title":"district 1","value1":20822.0,"value2":32200.0,"ratio":64.6646}
        assertThat(result.get(0).getValue1()).isEqualTo(20822f);
    }

    @Test
    public void isGetProductionAndConsumptionPerBuildingWorkFine() throws Exception {
        //when
        var result = underTest.getProductionAndConsumptionPerBuilding("district 1");
        //then
        //{"title":"Les Pyramide","value1":10411.0,"value2":23966.0,"ratio":43.44071}
        assertThat(result.get(0).getValue1()).isEqualTo(10411f);
    }

    @Test
    public void isGetProductionAndConsumptionPerHomeWorkFine() throws Exception {
        //when
        var result = underTest.getProductionAndConsumptionPerHome("Les Pyramide");
        //then
        //{"title":"home 4","value1":3470.0,"value2":8294.0,"ratio":41.83747}
        assertThat(result.get(0).getValue1()).isEqualTo(3470f);
    }

    @Test
    public void isGetProductionAndConsumptionPerEquipmentWorkFine() throws Exception {
        //when
        var result = underTest.getProductionAndConsumptionPerEquipment("home 4");
        //then
        //{"title":"Chauffage","value1":867.0,"value2":1952.0,"ratio":44.415985}
        assertThat(result.get(0).getValue1()).isEqualTo(867f);
    }
}